require "spec_helper"

describe DoctorMailer do
  describe 'patient_registration_confirmation' do
    before :each do
      @doctor = create(:doctor)
      @speciality = Speciality.create(:spe_name => "mcb")
      @patient = create(:patient)
      @report = create(:report)
    end

    let(:mail) { DoctorMailer.patient_registration_confirmation(@report,@doctor,@speciality,@patient)}

    it 'renders the subject' do
      # mail.subject.should == '#{@report.patient_first_name} #{@report.patient_last_name} Presentation Detail Copy'
    end
    it 'renders the receiver email' do
      mail.to.should == [@doctor.email]
    end
    it 'renders the sender email' do
      mail.from.should == ['from@example.com']
    end
    it 'assigns report access url' do
      mail.body.encoded.should match("Location")
    end
    it 'assigns patient report code' do
      mail.body.encoded.should match("#{@report.report_code}")
    end
  end
end
