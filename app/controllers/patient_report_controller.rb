class PatientReportController < ApplicationController
  before_filter :authenticate_doctor!
  def new
    @procedures = Procedure.all
    @patient_report = current_doctor.patients.new
    @speciality_wise_procedure = current_doctor.speciality.procedures

  end


  def create
    @procedures = Procedure.all
    @procedure_type = ProcedureType.all
    if params[:existing_patient].present?
      @patient_report = params[:existing_patient]
      session[:patient_report] = @patient_report

    else
      @patient_report = current_doctor.patients.new(params[:patient])
      @speciality_wise_procedure = current_doctor.speciality.procedures
      # @demo = !@patient_report.errors.any?
      #  puts "-------------------------#{@patient_report.id}---------------------------------------------------------"
      # @patient_cnt = 0
      # current_doctor.patients.each do |patient|
      #   if patient.email == @patient_report.email

      #     @patient_cnt = 1
      #   end
      # end
      respond_to do |format|
        # if @patient_cnt == 0
          if @patient_report.save
            session[:patient_report] = @patient_report.id
            @patient_report.update_attributes(:correct_first_name => @patient_report.first_name,:correct_email => @patient_report.email,:correct_last_name => @patient_report.last_name)
            @patient_report.update_attributes(:correct_city => @patient_report.city,:correct_state => @patient_report.state,:correct_country => @patient_report.country)
            @patient_report.update_attributes(:correct_address => @patient_report.address,:correct_zip=>@patient_report.zip)
            @patient_report.update_attributes(:correct_phone_number=>@patient_report.phone_number,:correct_birthdate => @patient_report.birthdate)
            @patient_report.update_attributes(:correct_gender_number=>@patient_report.gender)

            # format.html { redirect_to root_url,notice: 'Patient was successfully created.' }
            format.js
            format.json #{render json: @patient_report, status: :created, location: @patient_report }
          else
            format.js
            # format.html { render "new" }
            format.json { render json: @patient_report.errors, status: :unprocessable_entity }
          end
        # else
        #   puts "already taken"
        #   format.html { redirect_to patients_path,notice: 'Patient was already created.' }
        #   format.js {flash[:notice]='Patient was already created.'}
        # end
      end
    end
  end

  def edit
    @patient_report = Patient.find(session[:patient_report])
  end

  def update
    @patient_report = Patient.find(session[:patient_report])
    @procedures = Procedure.all
    @procedure_type = ProcedureType.all

    respond_to do |format|
      if @patient_report.update_attributes(params[:patient])
        format.js #update.js.erb
      else
        format.js
      end
    end
  end

end
