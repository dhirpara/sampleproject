ActiveAdmin.register DoctorsTestimonial do
  menu :parent => "Testimonial", :priority => 1

  form do |f|
    f.inputs "Details" do
      f.input :doctor_id, :label => "Doctor name", :as => :select, :collection => Doctor.all.map{ |p| [p.doctor_name, p.id] }, :include_blank => "Please select..."
      f.input :description, :label => "Description"
      f.input :status, :as => :select, :collection => [["Hide", false],["Show", true]]
    end
    f.actions
  end

  index :download_links => false do
    selectable_column
      column :id
      column "Doctor Name" do |doctorstestimonial|
        doctorstestimonial.doctor_doctor_name
      end
      column :description
      column :status
    default_actions
  end


  show do |user|
    attributes_table do
      row :id
      row "Doctor Name" do |doctorstestimonial|
        doctorstestimonial.doctor_doctor_name
      end
      row :description
      row :status
    end
  end

end
