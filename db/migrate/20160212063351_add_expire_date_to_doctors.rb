class AddExpireDateToDoctors < ActiveRecord::Migration
  def up
    add_column :doctors, :expire_date, :datetime
  end
  def down
  	remove_column :doctors, :expire_date
  end
end
