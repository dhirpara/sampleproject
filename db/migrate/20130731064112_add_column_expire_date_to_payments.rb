class AddColumnExpireDateToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :expire_date, :string
  end
end
