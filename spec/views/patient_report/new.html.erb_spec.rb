require 'spec_helper'

describe "patient_report/new.html.erb" do
  before(:each) do
    @patient_report = assign(:patient, stub_model(Patient))
  end

  it "renders the form partial" do
    stub_template "patient_report/new.html.erb" => "New patient report"
    render
    expect(rendered).to match /New patient report/
  end
end


