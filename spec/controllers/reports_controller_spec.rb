require 'spec_helper'

describe ReportsController do
  before :each do
    @report = create(:report)
  end
  describe "access_report" do
    describe "patient enter valid report_code" do
      it "finds report by report_code" do
        get :access_report, report_code: @report.report_code
        report_code = "hello"
        expect(report_code).to eq @report.report_code
      end
      it "renders the access_report template" do
        get :access_report, report_code: @report.report_code
        expect(response).to render_template :access_report
      end
    end
  end

  describe "send_mail" do

    it "sends a e-mail" do
      get :send_mail, id: @report
      PatientMailer.registration_confirmation(@report,@doctor,@speciality).deliver
      ActionMailer::Base.deliveries.last.to.should == [@report.patient.email]
    end
  end
end
