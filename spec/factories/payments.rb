# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment do
    first_name {Faker::Name.first_name}
    last_name {Faker::Name.last_name}
    credit_card_number "4111111111111111"
    expire_date "30-8-2013"
  end
end
