class PatientsTestimonialsController < ApplicationController
  # GET /patients_testimonials
  # GET /patients_testimonials.json
  def index
    @patients_testimonials = PatientsTestimonial.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @patients_testimonials }
    end
  end

  # GET /patients_testimonials/1
  # GET /patients_testimonials/1.json
  def show
    @patients_testimonial = PatientsTestimonial.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @patients_testimonial }
    end
  end

  # GET /patients_testimonials/new
  # GET /patients_testimonials/new.json
  def new
    @patients_testimonial = PatientsTestimonial.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @patients_testimonial }
    end
  end

  # GET /patients_testimonials/1/edit
  def edit
    @patients_testimonial = PatientsTestimonial.find(params[:id])
  end

  # POST /patients_testimonials
  # POST /patients_testimonials.json
  def create
    @patients_testimonial = PatientsTestimonial.new(params[:patients_testimonial])

    respond_to do |format|
      if @patients_testimonial.save
        format.html { redirect_to @patients_testimonial, notice: 'Patients testimonial was successfully created.' }
        format.json { render json: @patients_testimonial, status: :created, location: @patients_testimonial }
      else
        format.html { render action: "new" }
        format.json { render json: @patients_testimonial.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /patients_testimonials/1
  # PUT /patients_testimonials/1.json
  def update
    @patients_testimonial = PatientsTestimonial.find(params[:id])

    respond_to do |format|
      if @patients_testimonial.update_attributes(params[:patients_testimonial])
        format.html { redirect_to @patients_testimonial, notice: 'Patients testimonial was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @patients_testimonial.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patients_testimonials/1
  # DELETE /patients_testimonials/1.json
  def destroy
    @patients_testimonial = PatientsTestimonial.find(params[:id])
    @patients_testimonial.destroy

    respond_to do |format|
      format.html { redirect_to patients_testimonials_url }
      format.json { head :no_content }
    end
  end
end
