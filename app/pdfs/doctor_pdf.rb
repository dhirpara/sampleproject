class DoctorPdf < Prawn::Document

  def initialize(report,doctor,speciality,patient_report,patient_report_l,patient_perticuler)
    super()
    @report = report
    @doctor =doctor
    @speciality =speciality
    @patient_report = patient_report
    @patient_report_l= patient_report_l
    @gender =patient_perticuler

    #@doctor_first_last= @doctor.doctor_name
    @doctor_full_name = @doctor.doctor_name.strip.split(/\s+/).map(&:capitalize)

  end

  def render
    doc = Prawn::Document.new

    # Draw some stuff...

      doc.image "#{Rails.root}/app/assets/images/pdf/logo.png",:width => 250, :height => 53,:position => 150

      # doc.move_down 200


     doc.move_down 40

    doc.indent 320, 0 do
      doc.text "Location",:inline_format => true, :size => 17,:color => "01667e"
    end

    doc.indent 320,10 do
      @add = @doctor.address.titleize
      @b = @add.split(" ,")
      @c= @b.join(",")
      @d = @c.split(", ")
      @e = @d.join(",")

      doc.text @e.to_s,:inline_format => true,:size => 10,:color => "01667e"
    end

    doc.indent 320,20 do
      doc.text @doctor.city.to_s.capitalize+","+@doctor.state.to_s.capitalize+","+@doctor.zip.to_s+".",:inline_format => true,:size => 10,:color => "01667e"
    end


      # doc.formatted_text_box [
      #   { :text => "Location",:size => 17,
      #   :color => "01667e"}
      #   ], :at => [320, 620], :width => 100, :height => 30

      # doc.formatted_text_box [
      #   { :text => @doctor.address.to_s+","+@doctor.city.to_s+","+@doctor.state.to_s+","+@doctor.zip.to_s+".",
      #   :color => "01667e"}
      #   ], :at => [320, 600], :width => 200, :height => 60

      doc.formatted_text_box [
        { :text => "Dr. #{@doctor_full_name[0]} #{@doctor_full_name[1]}",:size => 17,
        :color => "01667e"}
        ], :at => [50, 620], :width => 200, :height => 30

      doc.formatted_text_box [
        { :text => @doctor.phone,
        :color => "01667e"}
        ], :at => [50, 600], :width => 100, :height => 30

      # doc.formatted_text_box [
      #   { :text => @speciality,
      #   :color => "01667e"}
      #   ], :at => [50, 600], :width => 100, :height => 30


      # doc.formatted_text_box [
      #   { :text => "Contact: "+@doctor.code+"-"+ @doctor.phone,
      #   :color => "01667e"}
      #   ], :at => [50, 585], :width => 300, :height => 30

      #doc.formatted_text_box [
       # { :text => "Email: "+@doctor.email,
        #:color => "01667e"}
        #], :at => [50, 570], :width => 300, :height => 30

        doc.formatted_text_box [
        { :text => "A presentation has been created for " + @patient_report.to_s+" "+ @patient_report_l.to_s + " at eInformedConsent.com. "+ @gender.capitalize.to_s+" can gain easy access to their presentation by following the directions in an email sent directly to them.",:size => 10,
        :color => "01667e"}
        ], :at => [50, 500], :width => 450, :height => 30


      doc.formatted_text_box [
        { :text => "As an alternative, the patient can also gain access to their presentation by following the directions below.",:size => 10,
        :color => "01667e"}
        ], :at => [50, 470], :width => 450, :height => 30

      doc.formatted_text_box [
        { :text => "1. Open your Internet browser and go to  " + 'http://www.einformedconsent.com/' + @patient_report + "/" + "login_page", :size => 10,
        :color => "01667e"}
        ], :at => [50, 430], :width => 450, :height => 30

      doc.formatted_text_box [
        { :text => "2. Enter the following Patient Passcode "+ @report.report_code + " and click Go." ,:size => 10,
        :color => "01667e"}
        ], :at => [50, 410], :width => 450, :height => 30

      doc.formatted_text_box [
        { :text => "Once past the Patient Portal, the patient should follow the information presented and take appropriate action.",:size => 10,
        :color => "01667e"}
        ], :at => [50, 380], :width => 450, :height => 30

      doc.formatted_text_box [
        { :text => "Thank you for using eInformedConsent.com.",:size => 10,
        :color => "01667e"}
        ], :at => [50, 340], :width => 450, :height => 30

      doc.formatted_text_box [
        { :text => "Thanks,",
        :color => "01667e"}
        ], :at => [50, 295], :width => 80, :height => 30

      doc.formatted_text_box [
        { :text => "Dr. #{@doctor_full_name[0]} #{@doctor_full_name[1]}",
        :color => "01667e"}
        ], :at => [50, 280], :width => 150, :height => 30

      doc.formatted_text_box [
        { :text => "Via einformedConsent",
        :color => "01667e"}
        ], :at => [50, 265], :width => 150, :height => 30

      # Return the PDF, rendered to a string
      doc.render
  end
end


