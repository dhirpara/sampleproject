class Payment < ActiveRecord::Base
  attr_reader :credit_card_number1,:credit_card_number2,:credit_card_number3,:credit_card_number4
  attr_accessible :credit_card_number, :expire_date, :first_name, :last_name

  PROCESSING, FAILED, SUCCESS = 1, 2, 3

  validates :credit_card_number,:expire_date,:first_name,:last_name, presence: true
  validates :credit_card_number, :length => { :maximum => 16 }

  def process_payment(creditcard,name)
    #   ----------------- for live account for AIM transaction --------------------------
    # aim_transaction = AuthorizeNet::AIM::Transaction.new(AUTHORIZE_NET_CONFIG['api_login_id'], AUTHORIZE_NET_CONFIG['api_transaction_key'], :gateway => :production)

    @amount = 30.00
    aim_transaction = AuthorizeNet::AIM::Transaction.new(AUTHORIZE_NET_CONFIG['api_login_id'], AUTHORIZE_NET_CONFIG['api_transaction_key'], :gateway => :sandbox)
    self.status = PROCESSING
    aim_response = aim_transaction.purchase('300.00', creditcard,name)

    #   --------------------for live account for ARB transaction ---------------------------
    # @transaction = AuthorizeNet::ARB::Transaction.new(AUTHORIZE_NET_CONFIG['api_login_id'], AUTHORIZE_NET_CONFIG['api_transaction_key'], :gateway => :production)
    logger.info "<<<<<<<<<<<<<<<<<<< #{aim_response.inspect} <<<<<<<<<<<<<<<<<<<<<<<"
    @transaction = AuthorizeNet::ARB::Transaction.new(AUTHORIZE_NET_CONFIG['api_login_id'], AUTHORIZE_NET_CONFIG['api_transaction_key'], :gateway => :sandbox)
    @subscription = AuthorizeNet::ARB::Subscription.new(
      :name => "gold",
      :length => 1,
      :unit => AuthorizeNet::ARB::Subscription::IntervalUnits::MONTH,
      :start_date => Date.today,
      :total_occurrences => 24,
      :trial_occurrences => nil,
      :amount => @amount,
      :trial_amount => nil,
      :invoice_number => rand(),
      :description => "a test subscription",
      :subscription_id => nil,
      :credit_card => creditcard,
      :billing_address => name
    )

    response = @transaction.create(@subscription)
      logger.info "<<<<<<<<<<<<<<<<<<< #{response.inspect} <<<<<<<<<<<<<<<<<<<<<<<"
    if aim_response.success? && response.success?
        #puts "Successfully made a purchase (authorization code: #{aim_response.authorization_code})"
        #puts "Successfully created a subscription (subscription id: #{response.subscription_id})"
        self.status = SUCCESS
      else
        self.status = FAILED
    end
    return self
  end

  def success?
    self.status == SUCCESS
  end

end
