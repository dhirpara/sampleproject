require 'spec_helper'

describe Payment do
  before do 
     @payment = build(:payment)
  end

  subject { @payment }

  it { should respond_to(:first_name) }
  it { should respond_to(:last_name) }
  it { should respond_to(:credit_card_number) }
  it { should respond_to(:expire_date) }
  it { should be_valid }

  describe "when credit card number is not present" do
    before { @payment.credit_card_number = " " }
    it { should_not be_valid }
  end

  describe "when first name is not present" do
    before { @payment.first_name = " " }
    it { should_not be_valid }
  end

  describe "when last name is not present" do
    before { @payment.last_name = " " }
    it { should_not be_valid }
  end

  describe "when expire date is not present" do
    before { @payment.expire_date = " " }
    it { should_not be_valid }
  end

  describe "when credit card number is too long" do
    before { @payment.credit_card_number = "a" * 17 }
    it { should_not be_valid }
  end

  
end
