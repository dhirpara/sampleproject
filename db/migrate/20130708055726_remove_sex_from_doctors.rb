class RemoveSexFromDoctors < ActiveRecord::Migration
  def up
    remove_column :doctors, :sex
  end

  def down
    add_column :doctors, :sex, :boolean
  end
end
