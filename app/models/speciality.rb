class Speciality < ActiveRecord::Base
  attr_accessible :spe_description, :spe_name
  validates :spe_name, :presence => true

  has_many :doctors, dependent: :destroy
  has_many :procedures, dependent: :destroy
  has_many :videos, dependent: :destroy
end