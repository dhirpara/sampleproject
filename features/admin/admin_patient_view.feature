Feature: View Patients

	In order to view patients list along with respective doctor, 
	As an admin,
	I want to view the index.

	Background: 
		Given Admin is already logged in
	
	Scenario: admin successfully views and manages patient
		When Admin clicks on Patients Link
		Then he should see firstname,lastname,city and doctor's name
		And he should be able to edit,delete and view the user
		And He should see New Patient Link
