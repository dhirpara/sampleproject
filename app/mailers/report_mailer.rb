class ReportMailer < ActionMailer::Base
  default from: "from@example.com"
  def patient_report(report,doctor,speciality,patient,rq,report_menu,proce_type)
  	@report = report
    @doctor = doctor
    @speciality=speciality
    @patient = patient
    @report_questions=rq
    @report_menu=report_menu
    @presentations = proce_type
    @doctor_full_name = @doctor.doctor_name.strip.split(/\s+/).map(&:capitalize)

    email_logo = File.read(Rails.root.join('app/assets/images/maillogo/logo.png'))
    attachments.inline['logo.png'] = email_logo

    report_pdf_view = ReportPdf.new(@report,@doctor,@speciality,@patient,@report_questions,@report_menu,@presentations)

    report_pdf_content = report_pdf_view.render()

    attachments["#{@report.patient_first_name} #{@report.patient_last_name}.pdf"] = {
      mime_type: 'application/pdf',
      content: report_pdf_content
    }

    mail(:to => @doctor.email, :subject => "#{@report.patient_first_name} #{@report.patient_last_name} eInformed Consent presentation.")
  end
end
