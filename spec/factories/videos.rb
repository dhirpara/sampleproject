# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :video do
    title "dev"
    description " "
    yt_video_id " "
    association :speciality
    association :procedure
    is_complete false
  end
end
