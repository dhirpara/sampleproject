Given(/^patient should see login page$/) do
  visit "/jkigs/login_page"   # this is menual process => make this dynamic
end

When(/^he fill valid usercode$/) do
  @report = FactoryGirl.create(:report)
  fill_in('report_code', :with => @report.report_code)
  click_button "Go"
end

When(/^he fill invalid usercode$/) do
   click_button "Go"
end

Then(/^he should see  Website Disclaimer page$/) do
  page.should have_content " Website Disclaimer "
end

Then(/^he should see invalid Usercode message$/) do
  page.should have_content "invalid Usercode"
end

Then(/^he should see login page$/) do
  visit "/jkigs/login_page"
end


Given(/^patient should see Website Disclaimer page$/) do
  visit "/jkigs/login_page"
  @report = FactoryGirl.create(:report)
  fill_in('report_code', :with => @report.report_code)
  click_button "Go"

end


When(/^he click on I Agree button$/) do
  click_link "I Agree"
end

Then(/^he should see patient detail$/) do
  page.should have_content " Patient Details"
end

When(/^he click on I Do Not Agree button$/) do
  visit "/jkigs/login_page"
  @report = FactoryGirl.create(:report)
  fill_in('report_code', :with => @report.report_code)
  click_button "Go"
  click_link "I Do Not Agree"
end

Then(/^he can click on confirm and continue button$/) do
  click_button "Confirm and Continue"
end

Then(/^he should see report question page$/) do
  page.should have_content "Have Questions?"

end

Given(/^patient should see report question page$/) do
  visit "/jkigs/login_page"
  @report = FactoryGirl.create(:report)
  fill_in('report_code', :with => @report.report_code)
  click_button "Go"
  click_link "I Agree"
  click_button "Confirm and Continue"

end

When(/^he fill question$/) do
 page.should have_content "Have Questions?"
end


When(/^he click on i understand link$/) do
  click_link "I understand this segment. Go to the next step."
end

Then(/^he should see E\-signature form$/) do
  page.should have_content "E-Signature"
end
