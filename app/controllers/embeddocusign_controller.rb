class EmbeddocusignController < ApplicationController
  require 'prawn'

  def create
    # @patient = Patient.find(params[:id])
  end
  def generate_pdf
    @patient = Patient.find(params[:id])

    pdf = PatientPdf.new(@patient)
    # @pdf.render_file File.join(Rails.application.routes.recognize_path, "/home/download", "#{@pdf}")
    pdf.render_file File.join(Rails.root, "/download-pdf/", "#{pdf}")
    redirect_to root_url
  end

  def create_template
    # @patient = Patient.find(params[:id])

    # @patient = Patient.find(params[:id])
    # @receiver_email = @patient.email
    # @receiver_name = @patient.first_name

    @sender_name = current_doctor.doctor_name
    @receiver_email = params[:receiver_email]
    @att = params[:pdf_attach]

    # logger.info ">>>>>> #{@att} <<<<<<"
    @e = File.read(@att)
    # logger.info ".......... >>>>>>  #{@e} <<<<<< .........."

    client = DocusignRest::Client.new
    @template_response = client.create_template(
      description: 'Document for E-Signature',
      name: "Document for E-Signature",
      signers: [
        {
          embedded: false,
          name: 'savan',
          email: @receiver_email,
          role_name: "Signer",
          sign_here_tabs: [
            {
              anchor_string: "Signature",
              anchor_x_offset: "-100",
              anchor_y_offset: "2"
            }
          ]
        }
        # },
        # {
        #   embedded: false,
        #   name: 'savan',
        #   email: current_doctor.email,
        #   role_name: "Issuer",
        #   sign_here_tabs: [
        #     {
        #       anchor_string: "Bob",
        #       anchor_x_offset: "40",
        #       anchor_y_offset: "2"
        #     }
        #   ]
        # }
      ],
      files: [
        # { path: "patient_#{@patient.created_at.strftime("%d/%m/%Y")}.pdf", name: "patient_#{@patient.created_at.strftime("%d/%m/%Y")}.pdf" }
        { path: @att, name: @att }
      ]
    )
    # logger.info ">>>> #{@template_response} <<<<"
    create_envelope_from_template
  end

  def create_envelope_from_template
    client = DocusignRest::Client.new
    @envelope_response = client.create_envelope_from_template(
      status: 'sent',
      email: {
        subject: "Document for E-Signature",
        body: "Document for E-Signature"
      },
      template_id: @template_response["templateId"],
      signers: [
        {
          embedded: false,
          name: 'savan',
          email: @receiver_email,
          role_name: "Signer",
        }
        # {
        #   embedded: false,
        #   name: 'savan',
        #   email: current_doctor.email,
        #   role_name: "Issuer",
        # }
      ]
    )
    render 'sign', :notice => "Document is successfully sent to patient!"

    logger.info " >>>> #{@envelope_response["envelopeId"]} <<<<< "
    logger.info " >>>> #{@envelope_response} <<<<< "
  end

  def create_env_from_doc
    client = DocusignRest::Client.new
    document_envelope_response = client.create_envelope_from_document(
      email: {
        subject: "test email subject",
        body: "this is the email body and it's large!"
      },
      signers: [
        {
          embedded: false,
          name: 'savan',
          email: 'savan_raval@yahoo.com',
          role_name: 'Issuer',
          sign_here_tabs: [
            {
              anchor_string: "Signature",
              anchor_x_offset: '1'
            }
          ]
        }
      ],
      files: [
        {path: 'esign.pdf', name: 'esign.pdf'}
      ],
      status: 'sent'
    )
    render 'sign'
  end
end