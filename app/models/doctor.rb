class Doctor < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,:is_accept
  attr_accessible :practise_name, :address, :doctor_name, :code, :phone, :sex, :city, :state, :zip, :speciality_id, :subscription_plan,:subscription_date,:recuring_date, :trial_expired, :trial_period, :expire_date

  belongs_to :speciality
  has_many :patients
  has_many :reports
  delegate :spe_name, :to => :speciality, :prefix => true

  # VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  VALID_EMAIL_REGEX = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i
  VALID_ZIP_REGEX = /\A[-a-zA-Z0-9\s]*\z/i
  VALID_PHONE_REGEX = /\A[-0-9\s]*\z/i

  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }#, uniqueness: {message: "already subscribed"}
  validates :speciality_id, presence: true, :if => lambda { |o| o.current_step == "speciality_select" }
  validates :password, :password_confirmation,:doctor_name,:practise_name, :address, :code, :phone,:city, :state, :zip, presence: true, :if => lambda { |o| o.current_step == "personal" }
  validates :doctor_name,:practise_name,:address,:city,:code,:phone, :state, :zip, presence: true
  validates :is_accept, acceptance: { accept: true }
  validates :speciality_id, presence: true
  validates :zip, format: { with: VALID_ZIP_REGEX }
  validates :phone, format: { with: VALID_PHONE_REGEX }
  # validates_uniqueness_of :email, message: "lgnsknfgjn"

  attr_writer :current_step

  def current_step
    @current_step || steps.first
  end

  def steps
    %w[confirmation speciality_select personal]
  end

  def next_step
    self.current_step = steps[steps.index(current_step)+1]
  end

  def previous_step
    self.current_step = steps[steps.index(current_step)-1]
  end

  def first_step?
    current_step == steps.first
  end

  def last_step?
    current_step == steps.last
  end

  def all_valid?
    steps.all? do |step|
      self.current_step = step
      valid?
    end
  end

  def valid_attribute?(attr)
    self.valid?
    self.errors[attr].blank?
  end

  def subcribe_to(plan)
    self.trial_expired = self.subscription_plan == "Free"
    self.trial_period = self.subscription_plan == "Free" ? 0 : nil
    self.subscription_plan = plan[:plan]
    self.subscription_date = Time.now()
    self.expire_date = Time.now() + plan[:interval].to_i.year
    self.save
  end

  def has_authenticate_plan?
    if self.subscription_plan == "Expired" || self.subscription_plan.nil?
      false
    elsif self.subscription_plan == "Unlimited"
      true
    elsif self.subscription_plan == "Free" && self.expire_date.to_datetime < Time.now
      self.trial_expired = true
      self.trial_period = 0
      self.subscription_plan = "Expired"
      self.save
      false
    elsif self.expire_date.to_datetime < Time.now
      self.subscription_plan = "Expired"
      self.save
      false
    else
      true
    end
  end

  def expire_plan
    unless self.subscription_plan.nil?
      if self.subscription_plan == "Free"
        self.trial_expired = true
        self.trial_period = 0
      end
      self.subscription_plan = "Expired"
      self.save
    end
  end

  private
    # For developre only
    def active_free_plan
      self.subscription_plan = "Free"
      self.subscription_date = Time.zone.now
      self.expire_date = Time.zone.now + 30.day
      self.save
    end

    def make_fresh_account
      self.subscription_plan = nil
      self.subscription_date = nil
      self.expire_date = nil
      self.trial_expired = false
      self.trial_period = nil
      self.save
    end

end