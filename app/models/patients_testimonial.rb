class PatientsTestimonial < ActiveRecord::Base
  belongs_to :patient
  attr_accessible :description, :status, :patient_id

  delegate :fullname, :to => :patient, :prefix => true

  def fullname
    [first_name, last_name].join "  "
  end
end
