class RemoveVideoCountFromReport < ActiveRecord::Migration
  def up
    remove_column :reports, :video_count
  end

  def down
    add_column :reports, :video_count, :integer
  end
end
