function setCookie(cname, cvalue) {
	var now = new Date();
	var time = now.getTime();
	time += 1800 * 1000;
	now.setTime(time);
  document.cookie = cname + "=" + cvalue + "; expires="+ now.toUTCString() +"; path=/";
  deleteCookie("free_plan")
}

function deleteCookie(cname) {
  document.cookie = cname + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
}