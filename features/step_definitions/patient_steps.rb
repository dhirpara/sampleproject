Given(/^doctor already signin$/) do
  @doctor = FactoryGirl.create(:doctor)
  visit root_path
  within('.subscribe-form') do
    fill_in "Email", with: @doctor.email
    fill_in "Password", with: @doctor.password  
    click_button "Log in Now!"
  end
end

When(/^he click on add patient link$/) do
  click_link "Add New Patient"
end

Then(/^he should see form of patient$/) do
  expect(page).to have_selector('form#new_patient')
end

#..........................Scenario: 2 add patient with invalid data....................................

Given(/^doctor visits add patient page$/) do
  visit new_patient_path
end

When(/^he submit patient form with invalid data$/) do
  click_button "Save"
end

Then(/^he should see error message for patient validation$/) do
  page.should have_content "can't be blank"
end

Then(/^he visits add patient page again$/) do
  visit new_patient_path
end

#.........................Scenario: 3 add patient with valid data.................................................

When(/^He fill valid information$/) do
  @patient = FactoryGirl.create(:patient)
  visit new_patient_path
  fill_in('First Name', :with => @patient.first_name)
  fill_in('Last Name', :with => @patient.last_name)
  fill_in('Email', :with => @patient.email)
  fill_in("Address", :with => "Ahmedabad")
  choose('patient_gender_female')
end

And(/^He press create button$/) do
  click_button "Save"
end

Then(/^he should see show patient page$/) do
  visit patient_path(@patient)
end