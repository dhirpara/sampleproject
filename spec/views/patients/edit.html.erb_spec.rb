require 'spec_helper'

describe "patients/edit" do
  
  before(:each) do
    @patient = assign(:patient, stub_model(Patient))
  end

  it "renders the form partial" do
    stub_template "patients/_form.html.erb" => "New Patient"
    render
    expect(rendered).to match /New Patient/
  end
end