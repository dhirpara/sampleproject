class AddStatusToPatientsTestimonials < ActiveRecord::Migration
  def change
    add_column :patients_testimonials, :status, :boolean, :default => false
  end
end
