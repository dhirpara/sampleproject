require "spec_helper"

include Devise::TestHelpers
#include Capybara::DSL

describe "doctor registration" do
  subject {page}
  it "allow new users to select registration desclaimer" do
    visit "/doctors/sign_up"
    should have_content("Registration Disclaimer")
    should have_button("I Agree")
  end

  it "allow to choose 'I agree' button and then press back button" do
    visit "/doctors/sign_up"
    click_button("I Agree")
    save_and_open_page
    should have_content("Speciality")
    select 'Please Select',:from =>"doctor[speciality_id]"

    should have_button("Next")
    should have_button("Back")

    click_button "Back"
    should have_selector('h1',:text=>"Registration Disclaimer")
  end

  it "allow to choose 'I Agree' button and then press next button" do
    visit "/doctors/sign_up"
    click_button("I Agree")
    should have_content("Speciality")
    should have_selector('h2',:text=>"Select your speciality !!!")
    FactoryGirl.create(:speciality)
    select "Please Select", :from => "doctor[speciality_id]"
    should have_button("Next")
    should have_button("Back")

    click_button "Next"
    # it "fill invalid doctor Personal Information" do
    #  visit doctor_registration_path
    #  click_button "Next"
    # should have_content "errors prohibited this doctor from being saved:"
    #end
  end


  it "allow to choose 'I Agree' button and then press next button" do
    visit "/doctors/sign_up"
    click_button("I Agree")
    @speciality = FactoryGirl.create(:speciality, :spe_name => "Ortho")
    puts @speciality.inspect

    should have_content("Speciality")
    should have_selector('h2',:text=>"Select your speciality !!!")
    @doctor = FactoryGirl.create(:doctor,:speciality_id => @speciality.id)
    puts @doctor.inspect
    select "Please Select" , :from => "doctor[speciality_id]"


    should have_button("Next")
    should have_button("Back")
    click_button "Next"
    fill_in 'Doctor name',:with => "jigar"
    fill_in 'Email', :with => "j@ya.com"
    fill_in 'Password', :with => "12345678"
    fill_in 'Password confirmation', :with => "12345678"
    choose('doctor_sex_male')
    check('doctor_is_accept')
    click_button('Next')

    #should have_button("Option 1")
    #should have_button("Option 2")

  # end
  end

  it "allow to choose 'I do not agree' button" do
    visit root_url
  end

  # it "select speciality" do
  #    # visit "/doctors/sign_up"
  #

  #   select('Please Select', :from =>'doctor_speciality_id')

  # end

  # it "allows new users to register with an email address and password" do
  #   visit "/doctors/sign_up"

  #   fill_in "Email",                 :with => "s@y.com"
  #   fill_in "Password",              :with => "password123"
  #   fill_in "Password confirmation", :with => "password123"

  #   click_button "Next"

  #   #page.should have_content("Welcome! You have signed up successfully.")
  # end
end