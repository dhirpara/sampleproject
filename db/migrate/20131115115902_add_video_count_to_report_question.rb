class AddVideoCountToReportQuestion < ActiveRecord::Migration
  def change
    add_column :report_questions, :video_count, :integer
  end
end
