require 'spec_helper'

  describe RegistrationsController do
    before do
      request.env["devise.mapping"] = Devise.mappings[:doctor] 
      session[:doctor_params] ||= {}
      @doctor = Doctor.new(session[:doctor_params])
      @doctor.current_step = session[:doctor_step]
    end
    
    describe 'GET new' do
      let!(:doctor) {FactoryGirl.build(:doctor)}
      #   Doctor.stub(:new).and_return(doctor) 
      # end
      it "assigns a new doctor to @doctor" do
        get :new
        #expect(assigns[:doctor]).to eq(doctor)
        expect(assigns(:doctor)).to be_a_new(Doctor)
      end
      
      it "renders the :new template" do
        get :new
        expect(response).to render_template :new
      end
    end


    describe "POST #create" do
      context "with valid attributes" do
        it "saves the new doctor in the database" do
          expect{
            post :create, doctor: attributes_for(:doctor)
          }.to change(Doctor, :count).by(1)
        end

        it "redirects to the select plan page" do
          post :create, doctor: attributes_for(:doctor)
          expect(response.status).to eq(200)
        end
      end

      context "with invalid attributes" do
        let!(:invalid_doctor) {FactoryGirl.build(:invalid_doctor)}

        it "does not save the new doctor in the database" do
          expect{
          post :create,
          doctor: attributes_for(:invalid_doctor)
          }.to_not change(Doctor, :count)
        end

        it "re-renders the :new template" do
          post :create,
          doctor: attributes_for(:invalid_doctor)
          expect(response).to render_template :new
        end
      end
    end    
  end
