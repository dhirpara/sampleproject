class AddProcedureIdToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :procedure_id, :integer
  end
end
