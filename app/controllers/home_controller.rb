class HomeController < ApplicationController
  # before_filter :authenticate_doctor!
  def index
    @doctors_testimonials = DoctorsTestimonial.find_by_status(true)
    @doctors_testimonials_last=DoctorsTestimonial.limit(1).order('id desc')

    @patients_testimonials = PatientsTestimonial.find_by_status(true)
    @patients_testimonials_last=PatientsTestimonial.limit(1).order('id desc')
    if session[:subcribe_doc].present? && cookies[:free_plan].present?
      session[:subcribe_doc].update_attributes(:subscription_plan => "Free", :subscription_date => Time.now, :expire_date => Time.now + 30.days, :trial_expired => false, :trial_period => "30")
      DoctorMailer.delay(queue: "doctor-#{session[:subcribe_doc].id}", run_at: session[:subcribe_doc].expire_date.to_datetime - 7.days).trial_expired_notification(current_doctor)
      cookies.delete :free_plan
      sign_in(:doctor, session[:subcribe_doc])
      flash[:success]="Your free plan is activated now."
      redirect_to root_path
    end
  end

  def how_it_works

  end

  def coming_soon

  end

  def terms_condition

  end

  def services

  end

  def about_us

  end

  def contat_us
    
  end

  def contact_us_submit
    @contact_us = ContactUs.new(params[:contact_us])
    if @contact_us.save
      DoctorMailer.contact_us_notification(@contact_us).deliver
      flash[:notice] = "Successfully Submit."
      redirect_to contact_us_path
    end
  end

end
 