class ReportQuestion < ActiveRecord::Base
  attr_accessible :question, :report_menu_id

  belongs_to :report_menu

  validates :question, presence: true

end
