class RemoveVideoCountFromReportQuestion < ActiveRecord::Migration
  def up
    remove_column :report_questions, :video_count
  end

  def down
    add_column :report_questions, :video_count, :integer
  end
end
