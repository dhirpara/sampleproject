Feature: Procedure_type

  In order to add procedures' type 
  As an admin,
  I want to add procedure type

  Background:
    Given Admin already logged in

  Scenario: Check for procedure type link
    And I am on dashboard
    When I click on procedure menu
    Then I should see procedure type link

  Scenario: Click on Procedure Type link
    Given Admin clicks on procedure menu
    When I click on procedure types link
    Then I should see list of procedure types
    And I should see New Procedure Type 

  Scenario: Create procedure type with invalid information
    Given Admin visits new procedure type page
    When He submit invalid procedure type information
    Then He should see error message
    And He visits new procedure type page

  Scenario: Procedure type with valid information
    Given the folowing procedure types exists  
      |Type Name |Procedure  |
      |General       |Ortho |
    And Procedures availables
    And Admin visits new procedure type page
    And I fill valid procedure type information
    When I select "Ortho" from the "procedure_type[procedure_id]"
    And I press create procedure type button
    Then I should see successful message for procedure type 

  