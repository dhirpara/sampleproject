class ChangeZipColumnFromIntegerToStringInPatients < ActiveRecord::Migration
  def up
  	change_table :patients do |t|
  		t.change :zip, :string
  	end
  end

  def down
  	change_table :patients do |t|
  		t.change :zip, :integer
  	end
  end
end
