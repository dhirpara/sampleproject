require 'spec_helper'

describe "doctors_testimonials/show" do
  before(:each) do
    @doctors_testimonial = assign(:doctors_testimonial, stub_model(DoctorsTestimonial,
      :description => "MyText",
      :status => "Status",
      :doctor => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/MyText/)
    rendered.should match(/Status/)
    rendered.should match(//)
  end
end
