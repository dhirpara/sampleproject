ActiveAdmin.register Subscriber do
	actions :all, except: [:new]
	menu :label => "Newsletter Subscribers"
	# index download_links: [:csv]

	csv do
	  column :email
	end

	index download_links: [:csv], :title => "Newsletter Subscribers" do
    column :id
		column :email
	end

	action_item :only => :index do
  	link_to "Export to CSV",admin_subscribers_path(format: "csv")
	end

end
