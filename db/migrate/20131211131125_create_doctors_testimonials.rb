class CreateDoctorsTestimonials < ActiveRecord::Migration
  def change
    create_table :doctors_testimonials do |t|
      t.text :description
      t.string :status
      t.references :doctor

      t.timestamps
    end
    add_index :doctors_testimonials, :doctor_id
  end
end
