class DoctorsTestimonial < ActiveRecord::Base
  belongs_to :doctor
  attr_accessible :description, :status, :doctor_id
  
  delegate :doctor_name,:city,:state,:practise_name, :to => :doctor, :prefix => true

end
