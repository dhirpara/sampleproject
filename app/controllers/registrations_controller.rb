class RegistrationsController < Devise::RegistrationsController
  helper :authorize_net
  def new
    session[:doctor_params] ||= {}
    @doctor = Doctor.new(session[:doctor_params])
    @doctor.current_step = session[:doctor_step]
    logger.info "<---------------------------#{@doctor.current_step}------------------------------------->"
  end

  def create
    session[:doctor_params].deep_merge!(params[:doctor]) if params[:doctor]
    @doctor = Doctor.new(session[:doctor_params])
    session[:current_doctor] = @doctor.email
    @doctor.current_step = session[:doctor_step]
    # if @doctor.valid?
      if params[:back_button]
        @doctor.previous_step
      elsif @doctor.last_step?
        if @doctor.valid?
          @doctor.save if @doctor.all_valid?
        end
        if @doctor.save
          session[:subcribe_doc] = @doctor
          logger.info "subscription doctor>>>>>>>>>.#{session[:subcribe_doc].doctor_name}"
        end
      else
        if @doctor.current_step == "speciality_select"
          if @doctor.valid_attribute?('speciality_id')
              resource.errors.clear
              @doctor.next_step
          end
        else
          @doctor.next_step
        end
      end
        session[:doctor_step] = @doctor.current_step
    # end

    if @doctor.new_record?
      render "new"
    else
      session[:doctor_step] = session[:doctor_params] = nil
      # checking for free plan
      if cookies[:free_plan].present?
        @doctor.update_attributes(:subscription_plan => "Free", :subscription_date => Time.now, :expire_date => Time.now + 30.days, :trial_expired => false, :trial_period => "30")
        DoctorMailer.delay(queue: "doctor-#{@doctor.id}", run_at: @doctor.expire_date.to_datetime - 7.days).trial_expired_notification(@doctor)
        # DoctorMailer.delay(queue: "doctor-#{@doctor.id}", run_at: 1.minutes.from_now).trial_expired_notification(@doctor)
        cookies.delete :free_plan
        sign_in(:doctor, @doctor)
        flash[:success]="Your free plan is activated now."
        redirect_to root_path
      elsif cookies[:subscribed_to].present?
        if cookies[:subscribed_to] == "1_year"
          @one_year_amount = 495
          @one_year_sim_transaction = SetAuthorizeObject.new(@one_year_amount, payments_thank_you_url(:only_path => false)).set_sim_transaction
        elsif cookies[:subscribed_to] == "2_year"
          @two_years_amount = 895
          @two_years_sim_transaction = SetAuthorizeObject.new(@two_years_amount, payments_thank_you_url(:only_path => false)).set_sim_transaction
        else
          flash[:error] = "Unexpected error : Try to login and select plan again!"
          redirect_to root_path
        end
      else
        # sign_in(:doctor, @doctor)
        flash[:success] = "You are successfully registered.Please select a plan to continue."
        redirect_to select_plan_path
      end
    end
  end

  def update
    super
  end
end