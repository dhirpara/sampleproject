class ReportQuestionsController < InheritedResources::Base
  respond_to :js, :json, :html, :xml

  def reply
    @report = Report.find(session[:report])
    @report_menu = session[:report_menu]

    @report_m=ReportMenu.find(session[:report_menu])
    @report_m.video_count += 1
    @report_m.update_attributes(:video_count=>@report_m.video_count)
    @s = Presentation.find_by_js_key(session[:report_menu].sequence)

    respond_with do |format|
      format.js
    end
  end

  def index
    @procedure_types = ProcedureType.all
    @parent_id = []

    @report = Report.find(session[:report])
    session[:question_page]= DateTime.now
    @report.update_attributes(:personal_info_confirmation_time_spent => session[:personal_info])
    @report_menus = @report.report_menus.all
    @rep_men = ReportMenu.find(params[:id])
    @presentations = Presentation.all

    session[:report_menu] = @rep_men

    @report_questions = @rep_men.report_questions.all
    @procedure_type=ProcedureType.all
    @s = Presentation.find_by_js_key(@rep_men.sequence)
    @date = Time.now
    @rep_men.update_attributes(:video_section_time=>@date)
    @rp = []

    @report_menus.each do |p|
      @presentations.each do |op|
        if p.sequence == op.js_key
          @parent_id << op.video_url
          @rp << p
        end
      end
    end
    logger.info "----> #{@rp}"
  end

  def show
    @report_question = ReportQuestion.find(params[:id])
  end

  def new
    @report_question = ReportQuestion.new
    respond_with do |format|
      format.html { render :layout => ! request.xhr? }
    end
  end

  def create
    @report = Report.find(session[:report])
    @report_menu = @report.report_menus.all
    @procedure_type = ProcedureType.all

    @report_question = ReportQuestion.create(params[:report_question])
    if @report_question.save
      @report_question.update_attributes(:report_menu_id => params[:id])
      respond_with(report_questions_path(params[:id]))
      # format.js
    end
  end


  def destroy
    @report_question = ReportQuestion.destroy(params[:id])
  end
end