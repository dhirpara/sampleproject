Feature: Sign out
  To sign out from web application
  As a doctor
  Should be able to sign out

    Scenario: doctor sign out
      Given I am logged in
      When I sign out
      Then I should see a signed out message
      When I return to the site
      Then I should be signed out