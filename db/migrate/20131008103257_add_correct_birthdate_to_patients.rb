class AddCorrectBirthdateToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_birthdate, :date
  end
end
