module ControllerMacros
  def sign_in(doctor = double('doctor'))
    if doctor.nil?
      request.env['warden'].stub(:authenticate!).
        and_throw(:warden, {:scope => :doctor})
      controller.stub :current_doctor => nil
    else
      request.env['warden'].stub :authenticate! => doctor
      controller.stub :current_doctor => doctor
    end
  end
end