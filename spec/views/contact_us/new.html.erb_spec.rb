require 'spec_helper'

describe "contact_us/new" do
  before(:each) do
    assign(:contact_u, stub_model(ContactU,
      :name => "MyString",
      :email => "MyString",
      :phone => "MyString",
      :comment => "MyString"
    ).as_new_record)
  end

  it "renders new contact_u form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", contact_us_path, "post" do
      assert_select "input#contact_u_name[name=?]", "contact_u[name]"
      assert_select "input#contact_u_email[name=?]", "contact_u[email]"
      assert_select "input#contact_u_phone[name=?]", "contact_u[phone]"
      assert_select "input#contact_u_comment[name=?]", "contact_u[comment]"
    end
  end
end
