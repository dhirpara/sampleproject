ActiveAdmin.register Audio do
  # menu :parent => "Uploads"
  menu false
  
  controller do
    def create
      params[:audio][:audio_file].each do |i|
        @params = {}
        @params['audio_file'] = i
        @audio = Audio.new(@params)
        @audio.save
      end
      if @audio.save
        redirect_to admin_audios_path
      end
    end
  end

  form(:html => { :multipart => true }) do |f|
    f.inputs "Audio Details" do
      f.input :audio_file, :as => :file, :input_html => { :multiple => true }
    end
    f.actions
  end

  show do
    render "show", :audio => @audio
    #active_admin_comments
  end
end
