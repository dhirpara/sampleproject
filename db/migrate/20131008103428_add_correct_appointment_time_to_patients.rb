class AddCorrectAppointmentTimeToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_appointment_time, :time
  end
end
