require 'spec_helper'

describe "patients/new.html.erb" do
  before(:each) do
    @patient = assign(:patient, stub_model(Patient)).as_new_record.as_null_object
  end
  it "renders the form partial" do
    render

    rendered.should have_selector('form#new_patient') do |form|

      form.should have_selector('label',:for=>'patient_email',:content=>'email')
      form.should have_selector('input',:type => "text",:name=>'patient[email]',:id=>'patient_email')

      form.should have_selector('label',:for=>'patient_First_Name',:content=>'first_name')
      form.should have_selector('text',:name=>'patient[first_name]',:id=>'patient_first_name')

      form.should have_selector('label',:for=>'patient_Last_Name',:content=>'last_name')
      form.should have_selector('text',:name=>'patient[last_name]',:id=>'patient_last_name')


      form.should have_selector('label',:for=>'patient_Address',:content=>'address')
      form.should have_selector('text',:name=>'patient_Address',:id=>'patient_address')


      form.should have_selector('label',:for=>'patient_Gender',:content=>'gender')
      form.should have_selector('radio_button',:name=>'patient[Gender]',:id=>'patient_gender')

      form.should have_selector('label',:for=>'patient_City',:content=>'city')
      form.should have_selector('text',:name=>'patient[city]',:id=>'patient_city')

      form.should have_selector('label',:for=>'patient_State',:content=>'state')
      form.should have_selector('text',:name=>'patient[state]',:id=>'patient_state')

      form.should have_selector('label',:for=>'patient_Phone_Number',:content=>'phone_number')
      form.should have_selector('text',:name=>'patient[phone_code]',:id=>'patient_phone_code')
      form.should have_selector('text',:name=>'patient[phone_number]',:id=>'patient_phone_number')

      # form.should have_selector('label',:for=>'patient_Birthdate',:content=>'birthdate')
      # form.should have_selector('text',:name=>'patient[last_name]',:id=>'patient_last_name')

      form.should have_selector('input',:type=>'submit',:value=>'Create')
      end
    end
  end