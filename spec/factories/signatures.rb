# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :signature do
    sig_text "MyString"
    flag false
  end
end
