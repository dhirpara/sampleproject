require 'spec_helper'

describe ReportMenu do
  before do
    @report_menu = ReportMenu.new(report_id: "1",procedure_type_ids: '1')
  end
  subject { @report_menu }

  it { should respond_to(:report) }
  it { should respond_to(:procedure_type) }
end
