class CreateSpecialities < ActiveRecord::Migration
  def change
    create_table :specialities do |t|
      t.string :spe_name
      t.text :spe_description

      t.timestamps
    end
  end
end
