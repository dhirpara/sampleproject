# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :report do
    report_code "hello"
    signature "abc"
    association :doctor
    association :patient
  end

  factory :invalid_report, parent: :report do
    report_code nil
  end
end