require 'spec_helper'

describe PatientReportController do

  describe 'GET #new' do

    before(:each) do
      @doctor = create(:doctor)
      sign_in @doctor
      @patient= build(:patient, doctor: @doctor)
    end

    it "assigns a new Patient to @patient" do
      get :new
      expect(assigns(:patient_report)).to be_a_new(Patient)
    end

    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    let!(:doctor) { create(:doctor) }

    before(:each) do
      sign_in doctor
    end

    context "with valid attributes" do
      it "saves the new patient in the database" do
        expect{
          post :create, patient: attributes_for(:patient)
        }.to change(Patient, :count).by(1)
      end

      it "redirects to the patient show page" do
        post :create, patient: attributes_for(:patient)
        expect(response).to respond_with 200
      end
    end

    context "with invalid attributes" do
      let!(:invalid_patient) {FactoryGirl.build(:invalid_patient)}

      it "does not save the new patient in the database" do
        expect{
        post :create,
        patient: attributes_for(:invalid_patient)
        }.to_not change(Patient, :count)
      end

      it "re-renders the :new template" do
        post :create,
        patient: attributes_for(:invalid_patient)
        expect(response).to render_template :new
      end
    end
  end

end

