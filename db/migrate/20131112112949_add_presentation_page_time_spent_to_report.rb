class AddPresentationPageTimeSpentToReport < ActiveRecord::Migration
  def change
    add_column :reports, :presentation_page_time_spent, :datetime
  end
end
