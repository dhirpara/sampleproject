class AddCorrectCityToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_city, :string
  end
end
