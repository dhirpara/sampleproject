require "spec_helper"

describe DoctorsTestimonialsController do
  describe "routing" do

    it "routes to #index" do
      get("/doctors_testimonials").should route_to("doctors_testimonials#index")
    end

    it "routes to #new" do
      get("/doctors_testimonials/new").should route_to("doctors_testimonials#new")
    end

    it "routes to #show" do
      get("/doctors_testimonials/1").should route_to("doctors_testimonials#show", :id => "1")
    end

    it "routes to #edit" do
      get("/doctors_testimonials/1/edit").should route_to("doctors_testimonials#edit", :id => "1")
    end

    it "routes to #create" do
      post("/doctors_testimonials").should route_to("doctors_testimonials#create")
    end

    it "routes to #update" do
      put("/doctors_testimonials/1").should route_to("doctors_testimonials#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/doctors_testimonials/1").should route_to("doctors_testimonials#destroy", :id => "1")
    end

  end
end
