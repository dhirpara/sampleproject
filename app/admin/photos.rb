ActiveAdmin.register Photo do
  # menu :parent => "Uploads"
   menu false
   
  controller do
    def create
      @photo_new = params[:photo][:image] unless nil?
      @photo_new.each do |i|
        @params = {}
        @params['image'] = i
        @photo = Photo.new(@params)
        logger.info "..........#{ @params.inspect }....."
        @photo.save
      end
    end
  end

  form :partial => "form", :photo => @photo

  index do
    render "index"
  end

  # form(:html => { :multipart => true }) do |f|
  #   f.inputs "Image Details" do
  #     f.input :image, :as => :file, :input_html => { :multiple => true, :class => "image-fileinput-button" }
  #   end
  #   f.actions
  # end

  show do
    attributes_table do
      row :image_name
      row "Images" do |photo|
        image_tag(photo.image.url(:thumb).to_s)
      end
    end
  end
end