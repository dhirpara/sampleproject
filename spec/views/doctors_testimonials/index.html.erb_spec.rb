require 'spec_helper'

describe "doctors_testimonials/index" do
  before(:each) do
    assign(:doctors_testimonials, [
      stub_model(DoctorsTestimonial,
        :description => "MyText",
        :status => "Status",
        :doctor => nil
      ),
      stub_model(DoctorsTestimonial,
        :description => "MyText",
        :status => "Status",
        :doctor => nil
      )
    ])
  end

  it "renders a list of doctors_testimonials" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
