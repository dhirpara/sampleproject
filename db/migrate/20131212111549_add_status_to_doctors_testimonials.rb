class AddStatusToDoctorsTestimonials < ActiveRecord::Migration
  def change
    add_column :doctors_testimonials, :status, :boolean, :default => false
  end
end
