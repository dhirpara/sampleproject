ActiveAdmin.register Patient do

  menu :priority => 4

  member_action :show_patient_reports, :title => "Patients Reports" do
    @pat = Patient.find(params[:patient_id])
    @repo = @pat.reports.all
    @procedure_types = ProcedureType.all
    @report_menu = ReportMenu.all
  end


  form do |f|
    f.inputs "Details" do
      f.input :doctor_id, :label => "Doctor name", :as => :select, :collection => Doctor.all.map{ |p| [p.doctor_name, p.id] }, :include_blank => "Please select..."
      f.input :first_name
      f.input :last_name
      f.input :email
      # if f.object.new_record?
      #   f.input :password
      # end
      f.input :phone_code
      f.input :phone_number
      f.input :gender, :as => "radio", :collection => ["Male","Female"]
      f.input :birthdate, :as => :datepicker
      f.input :address, :label => "Address", :input_html => { :rows => 5, :cols => 30, :maxlength => 10  }
      f.input :city
      f.input :state
      f.input :country, :as => :select, :collection => ["Afghanistan", "Aland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola",
        "Anguilla", "Antarctica", "Antigua And Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria",
        "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin",
        "Bermuda", "Bhutan", "Bolivia, Plurinational State of", "Bonaire, Sint Eustatius and Saba", "Bosnia and Herzegovina",
        "Botswana", "Bouvet Island", "Brazil",
        "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia",
        "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China",
        "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo",
        "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba",
        "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt",
        "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)",
        "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia",
        "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece",
        "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea",
        "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City State)",
        "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran, Islamic Republic of", "Iraq",
        "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya",
        "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan",
        "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya",
        "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia, The Former Yugoslav Republic Of",
        "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique",
        "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of",
        "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru",
        "Nepal", "Netherlands", "New Caledonia", "New Zealand", "Nicaragua", "Niger",
        "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau",
        "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines",
        "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation",
        "Rwanda", "Saint Barthelemy", "Saint Helena, Ascension and Tristan da Cunha", "Saint Kitts and Nevis", "Saint Lucia",
        "Saint Martin (French Part)", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino",
        "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore",
        "Sint Maarten (Dutch Part)", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa",
        "South Georgia and the South Sandwich Islands", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname",
        "Svalbard and Jan Mayen", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic",
        "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Timor-Leste",
        "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan",
        "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom",
        "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu",
        "Venezuela, Bolivarian Republic of", "Viet Nam", "Virgin Islands, British", "Virgin Islands, U.S.",
        "Wallis and Futuna", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"],:include_blank => "United States"
      f.input :zip
      #f.input :procedure_id, :label => "procedure", :as => :select, :collection => Procedure.all.map{ |p| [p.procedure_name, p.id] }, :include_blank => "Please select..."
      # f.input :appointment_date, :as => :datepicker
      # f.input :appointment_time,:as => :time_picker
    end
    f.actions
  end


  index :download_links => false do
    @doctor = Doctor.all
    selectable_column
    column :first_name
    column :last_name
    column :city
    column :email
    column "Doctor Name" do |patient|
      patient.doctor_doctor_name
    end
    default_actions
    column do |post|
      link_to "View Reports", show_patient_reports_admin_patient_path(post)
    end
  end

  show do |user|
    attributes_table do
      row :id
      row :first_name
      row :last_name
      row :email
      row :phone_code
      row :phone_number
      row :gender
      row :birthdate
      row :address
      row :city
      row :state
      row :country
      row :zip
      # row "Procedure Name" do |spe|
      #   spe.procedure.procedure_name
      # end
      # row :appointment_date
      # row "Appointment Time" do |record|
        # record.appointment_time.strftime('%H:%M')
      # end
      row :doctor
      row :created_at
      row :updated_at
    end
  end

  filter :doctor_id, :collection => proc {(Doctor.all).map{|c| [c.doctor_name, c.id]}}, :label => "Doctor Name"
  filter :first_name, :label => "Patient First Name"
  filter :last_name, :label => "Patient Last Name"
  filter :email, :label => "Patient Email"
  filter :address, :label => "Patient Address"
  filter :city, :label => "City"
  filter :state, :label => "State"
  filter :country, :label => "Country"
  filter :zip, :label => "Zip"
  # filter :appointment_date
  filter :created_at
  filter :updated_at
end
