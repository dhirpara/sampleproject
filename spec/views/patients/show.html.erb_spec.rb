require 'spec_helper'

describe "patients/show.html.erb" do
  let!(:doctor) { create(:doctor)}
  let!(:patient) { create(:patient, doctor: doctor)}
  before { sign_in doctor }
  before { visit patient_path(patient) }

  it "displays the patient first_name and last_name and email" do
    expect(page).to have_content(patient.first_name)
    expect(page).to have_content(patient.last_name)
    expect(page).to have_content(patient.email)
  end
end