class DoctorsTestimonialsController < ApplicationController
	
  def index
    @doctors_testimonials = DoctorsTestimonial.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @doctors_testimonials }
    end
  end

  # GET /doctors_testimonials/1
  # GET /doctors_testimonials/1.json
  def show
    @doctors_testimonial = DoctorsTestimonial.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @doctors_testimonial }
    end
  end

  # GET /doctors_testimonials/new
  # GET /doctors_testimonials/new.json
  def new
    @doctors_testimonial = DoctorsTestimonial.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @doctors_testimonial }
    end
  end

  # GET /doctors_testimonials/1/edit
  def edit
    @doctors_testimonial = DoctorsTestimonial.find(params[:id])
  end

  # POST /doctors_testimonials
  # POST /doctors_testimonials.json
  def create
    @doctors_testimonial = DoctorsTestimonial.new(params[:doctors_testimonial])

    respond_to do |format|
      if @doctors_testimonial.save
        format.html { redirect_to @doctors_testimonial, notice: 'Doctors testimonial was successfully created.' }
        format.json { render json: @doctors_testimonial, status: :created, location: @doctors_testimonial }
      else
        format.html { render action: "new" }
        format.json { render json: @doctors_testimonial.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /doctors_testimonials/1
  # PUT /doctors_testimonials/1.json
  def update
    @doctors_testimonial = DoctorsTestimonial.find(params[:id])

    respond_to do |format|
      if @doctors_testimonial.update_attributes(params[:doctors_testimonial])
        format.html { redirect_to @doctors_testimonial, notice: 'Doctors testimonial was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @doctors_testimonial.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /doctors_testimonials/1
  # DELETE /doctors_testimonials/1.json
  def destroy
    @doctors_testimonial = DoctorsTestimonial.find(params[:id])
    @doctors_testimonial.destroy

    respond_to do |format|
      format.html { redirect_to doctors_testimonials_url }
      format.json { head :no_content }
    end
  end
end
