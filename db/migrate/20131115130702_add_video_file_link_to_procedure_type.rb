class AddVideoFileLinkToProcedureType < ActiveRecord::Migration
  def change
    add_column :procedure_types, :video_file_link, :string
  end
end
