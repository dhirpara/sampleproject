require 'spec_helper'

describe Procedure do
  before do
    @procedure = Procedure.new(procedure_name: "TestProcedure", procedure_description: "TestDescription", speciality_id: 1)
  end

  subject { @procedure }

  it { should respond_to(:procedure_name) }
  it { should respond_to(:procedure_description) }

  it { should be_valid }

  describe "when procedure name is not present" do
    before { @procedure.procedure_name = " " }
    it {should_not be_valid}
  end
end
