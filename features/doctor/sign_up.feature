Feature: Sign up

  In order to use the web application
  As a doctor
  I want to signup in the database

  Scenario: Doctor signup with valid data
    Given I am on doctor signup page
    And I click on i agree button
    And I select speciality "doesn"
    And I click on next button
    When I fill all doctor information
    Then I click on finish button
    And I should be redirected to select plan path
    And I can see Advance Plan as title

  Scenario: Doctor is not accepting terms and conditions
    Given I am on doctor signup page
    When I click on i do not agree link
    Then I should be redirected to root

  Scenario: User signs up with invalid email
    Given I am on doctor signup page
    And I click on i agree button
    And I select speciality "doesn"
    And I click on next button
    When I fill doctor email incorrect and blank password
    Then I click on finish button
    And I should see email cant blank and Password can't be blank

  Scenario: User signs up without password
    Given I am on doctor signup page
    And I click on i agree button
    And I select speciality "doesn"
    And I click on next button
    When I sign up without a password
    Then I should see a missing password cant be blank

  Scenario: User signs up with mismatched password and confirmation
    Given I am on doctor signup page
    And I click on i agree button
    And I select speciality "doesn"
    And I click on next button
    When I sign up with a mismatched password confirmation
    Then I should see a Password doesn't match confirmation