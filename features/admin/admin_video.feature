Feature: Video

	In order to manage video in our application
	As a admin
	I want to add a video

	Scenario: Admin wants to add video
		Given Admin already signed in
		When I click on videos link
		Then I should see list of videos
		And I should see new video link
		And I click on new video link
		And I should see form of video

	Scenario: Admin create video with invalid data
		Given Admin already signed in
		And visit new video page
		When I submit video form with blank data
		Then I shold see error message for video validation
		And I see add video page again

	Scenario: Admin create video with valid data
		Given Admin already signed in
		And visit new video page
		And fill valid information
		When I press create button
		Then I should see upload video page