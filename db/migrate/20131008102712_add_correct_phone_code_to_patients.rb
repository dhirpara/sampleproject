class AddCorrectPhoneCodeToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_phone_code, :string
  end
end
