class ProcedureType < ActiveRecord::Base
  attr_accessible :procedure_id, :type_name,:parent_type_id,:video_file_link, :js_key
  belongs_to :procedure
  has_many :report_menus

  has_many :subtypes, :class_name => "ProcedureType", :foreign_key => "parent_type_id"
  belongs_to :parent_type, :class_name => "ProcedureType" , :foreign_key => "parent_type_id"

  delegate :procedure_name, :to => :procedure, :prefix => true

  validates :procedure_id, :type_name, presence: true

  scope :procedure_type, where(:parent_type_id => nil)
end