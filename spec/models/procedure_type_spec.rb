require 'spec_helper'

describe ProcedureType do
  let(:procedure) { FactoryGirl.create(:procedure) }
  before do
    @procedure_type = procedure.procedure_types.new(type_name: "General")
  end

  subject { @procedure_type }

  it { should respond_to(:type_name) }
  it { should respond_to(:procedure) }
  its(:procedure) {should == procedure}

  it { should be_valid }

  describe "when procedure is not present" do
    before { @procedure_type.procedure_id = " " }
    it {should_not be_valid}
  end

  describe "when procedure type name is not present" do
    before { @procedure_type.type_name = " " }
    it {should_not be_valid}
  end

end
