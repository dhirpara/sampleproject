Feature: Doctor

  Background:
    Given Admin already logged in
  
  Scenario: Check for doctors link
    When I click on doctors link
    Then I should see list of doctors
    And I should see new doctor button

  Scenario: Doctor with invalid information
    And Admin visits new doctor page
    When I submit invalid doctor information
    Then I should see error message
    And I visits new doctor page

  Scenario: Doctor with valid information
    Given the folowing doctor exists  
      |Doctor Name  | Email               |password |speciality   |code     |zip      |city       |
      |savan        | svnrvl001@gmail.com |password | Ortho       |079      |382443   |Ahmedabad  |
      |Jigar        | svnrvl001@gmail.com |password | Dentist     |022      |382456   |baroda     |
    And Speciality availables
    And Admin visits new doctor page
    And I fill valid information
    When I select "Ortho" from "doctor_speciality_id"
    And I press create doctor button
    Then I should see successful message