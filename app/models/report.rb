class Report < ActiveRecord::Base
  attr_accessible :doctor_id,:patient_id,:signature,:connection_establish_time, :login_page_time_spent, :desclaimer_time_spent, :personal_info_confirmation_time_spent,:presentation_page_time_spent, :esignature_page_time_spent,:status,:econnection,:video_count

  belongs_to :doctor
  belongs_to :patient, :class_name => "Patient"
  has_many :report_menus


  delegate :first_name, :last_name, :email, :birthdate, :gender, :address, :city, :state, :country, :to => :patient, :prefix => true
  delegate :correct_first_name, :correct_last_name, :correct_birthdate, :correct_gender_number, :correct_address, :correct_city, :correct_state, :correct_country, :to => :patient, :prefix => true

  before_create :generate_token

  after_create :status_signature
  def generate_token
    self.token = Digest::SHA1.hexdigest([Time.now, rand].join)
  end

  def status_signature
    self.status = "Pending"
  end
end


