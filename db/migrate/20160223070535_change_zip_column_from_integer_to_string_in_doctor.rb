class ChangeZipColumnFromIntegerToStringInDoctor < ActiveRecord::Migration
  def up
  	change_table :doctors do |t|
  		t.change :zip, :string
  	end
  end

  def down
  	change_table :doctors do |t|
  		t.change :zip, :integer
  	end
  end
end
