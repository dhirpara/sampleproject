require 'spec_helper'

describe "Doctor" do

  before do 
     @doctor = build(:doctor)
  end

  subject { @doctor }

  
  it { should respond_to(:doctor_name) }
  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:practise_name) }
  it { should respond_to(:address) }
  it { should respond_to(:city) }
  it { should respond_to(:state) }
  it { should respond_to(:zip) }
  it { should respond_to(:sex) }
  it { should respond_to(:speciality) }
  it { should be_valid }
  

  describe "when Doctor name is not present" do
    before { @doctor.doctor_name = " " }
    it { should_not be_valid }
  end
  

  describe "when email is not present" do
    before { @doctor.email = " " }
    it { should_not be_valid }
  end

  describe "when email format is invalid" do
    it "should be invalid" do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                     foo@bar_baz.com foo@bar+baz.com]
      addresses.each do |invalid_address|
        @doctor.email = invalid_address
        @doctor.should_not be_valid 
      end      
    end
  end

  describe "when email format is valid" do
    it "should be valid" do
      addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
      addresses.each do |valid_address|
        @doctor.email = valid_address
        @doctor.should be_valid
      end      
    end
  end

  describe "when email address is already taken" do
    before do
      user_with_same_email = @doctor.dup
      user_with_same_email.save
    end

    it { should_not be_valid }
  end

  

   
end