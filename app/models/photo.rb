class Photo < ActiveRecord::Base
  attr_accessible :image, :image_name
  mount_uploader :image, ImageUploader

  # validates :image, :image_name, :presence => true
end
