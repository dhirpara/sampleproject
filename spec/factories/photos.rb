# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :photo do
    image_name "MyString"
    image "MyString"
  end
end
