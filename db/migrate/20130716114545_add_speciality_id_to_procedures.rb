class AddSpecialityIdToProcedures < ActiveRecord::Migration
  def change
    add_column :procedures, :speciality_id, :integer
  end
end
