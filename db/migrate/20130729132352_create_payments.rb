class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :first_name
      t.string :last_name
      t.string :credit_card_number
      t.date :expire_date
      t.integer :verification_value

      t.timestamps
    end
  end
end
