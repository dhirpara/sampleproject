# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :patients_testimonial do
    description "MyText"
    status "MyString"
    patient nil
  end
end
