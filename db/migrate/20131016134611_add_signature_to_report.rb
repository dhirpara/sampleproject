class AddSignatureToReport < ActiveRecord::Migration
  def change
    add_column :reports, :signature, :string
  end
end
