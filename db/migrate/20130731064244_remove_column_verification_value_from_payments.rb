class RemoveColumnVerificationValueFromPayments < ActiveRecord::Migration
  def up
    remove_column :payments, :verification_value
  end

  def down
    add_column :payments, :verification_value, :integer
  end
end
