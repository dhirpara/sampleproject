Then(/^I should see procedure type link$/) do
  page.should have_content('Procedure Types')
end

When(/^I click on procedure types link$/) do
   click_link 'Procedure Types'
end

Then(/^I should see list of procedure types$/) do
  visit admin_procedure_types_path
end

Then(/^I should see New Procedure Type$/) do
  page.should have_content('New Procedure Type')
end

Given(/^Admin visits new procedure type page$/) do
  visit new_admin_procedure_type_path
end

When(/^He submit invalid procedure type information$/) do
  click_button("Create Procedure type")
end

Then(/^He visits new procedure type page$/) do
  visit new_admin_procedure_type_path
end

Given(/^the folowing procedure types exists$/) do |table|
  table.hashes.each do |attributes|
    ProcedureType.create :type_name => attributes[:type_name]
  end
end

Given(/^Procedures availables$/) do
     FactoryGirl.create(:procedure)
end

Given(/^I fill valid procedure type information$/) do
  @procedure_type = FactoryGirl.build(:procedure_type)
  fill_in "Name of procedure type",:with => @procedure_type.type_name
end

When(/^I select "(.*?)" from the "(.*?)"$/) do |arg1, arg2|
  page.select "Ortho", :from => "procedure_type[procedure_id]"
end

When(/^I press create procedure type button$/) do
  click_button "Create Procedure type"
end

Then(/^I should see successful message for procedure type$/) do
  page.should have_content "Procedure type was successfully created."
end
