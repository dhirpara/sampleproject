ActiveAdmin.register ProcedureType do

  menu :parent => "Procedure", :priority => 2

  scope :procedure_types, :default => true do |p|
     ProcedureType.procedure_type
  end
  controller do

    def create_subtype
      @procedure_type = ProcedureType.new(params[:procedure_type])
      @procedure_type.procedure_id = 0
      if @procedure_type.save
        redirect_to admin_procedure_types_path
      end
    end
  end

  member_action :sub_type, :title => "Procedure Type's subtype" do
    @procedure_type = ProcedureType.all
    @p1 = ProcedureType.find(params[:procedure_type_id])
  end

  form do |f|
    f.inputs "Details" do
      f.input :procedure, :label => "select procedure ", :collection => Procedure.all.map{ |s| [s.procedure_name, s.id] }, :html_options => { :enable => false }
      f.input :type_name, :label => "Name of procedure type "
      f.input :video_file_link, :label => "Video file link "
      f.input :js_key, :label => "Js Key"
      f.actions
    end
  end

  index do
    selectable_column
    column :id
      # column "Procedure Name" do |procedure_type|
      #   procedure_type.procedure_procedure_name
      # end
    column :type_name
    column :video_file_link

    default_actions

    column do |pts|
      link_to "View procedure sub types", sub_type_admin_procedure_type_path(pts)
    end
    column do |pts|
      link_to image_tag('/assets/create.png', :size => '15x15'), create_subtype_path(pts.id)
    end
  end

  show do |user|
    attributes_table do
      row :id
      row :type_name
      row :video_file_link
      # row "Procedure Name" do |p|
      #   p.procedure_procedure_name
      # end
      row :created_at
      row :updated_at
    end
    # active_admin_comments
  end

  filter :procedure_id, :collection => proc {(Procedure.all).map{|p| [p.procedure_name, p.id]}}, :label => "Procedure Name"
end