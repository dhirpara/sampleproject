class PatientMailer < ActionMailer::Base
  default from: "from@example.com"

  def registration_confirmation(report,doctor,speciality)
    @report = report
    @doctor = doctor
    @speciality=speciality
    @doctor_full_name = @doctor.doctor_name.strip.split(/\s+/).map(&:capitalize)
    email_logo = File.read(Rails.root.join('app/assets/images/maillogo/logo.png'))
    attachments.inline['logo.png'] = email_logo
	  mail(:to => @report.patient_email, :subject => "An important message regarding your proposed treatment")
  end

  def confirm_report(patient)
    @patient = patient
    mail(:to => @patient.email, :subject => "Signature on report")
  end

  def thank_you_message(report,patient,doctor,speciality_doc)
    @report = report
    @patient = patient
    @doctor = doctor
    @speciality = speciality_doc

    @doctor_full_name = @doctor.doctor_name.strip.split(/\s+/).map(&:capitalize)
    email_logo = File.read(Rails.root.join('app/assets/images/maillogo/logo.png'))
    attachments.inline['logo.png'] = email_logo


    mail(:to => @patient.email, :subject => "Thank you message")
  end
end