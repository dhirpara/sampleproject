﻿//SETTING UP OUR POPUP
//0 means disabled; 1 means enabled;
var popupStatus = 0;
//loading popup with jQuery
function loadPopup(){
	//loads popup only if it is disabled
	if(popupStatus==0){
		$(".background_popup").css({
			"opacity": "0.43"
		});
		$(".background_popup").fadeIn("slow");
		$(".contact_form, .profile_popup").fadeIn("slow");
		popupStatus = 1;
	}

}

//disabling popup  
function disablePopup(){

  //disables popup only if it is enabled
  if(popupStatus==1){
    $(".background_popup").fadeOut("slow");
    $(".contact_form, .profile_popup").fadeOut("slow");
    popupStatus = 0;
  }

	//disables popup only if it is enabled
	if(popupStatus==1){
		$(".background_popup").fadeOut("slow");
		$(".contact_form, .profile_popup").fadeOut("slow");
		popupStatus = 0;
	}

}

//centering popup
function centerPopup(){

  
  var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var popupHeight = $(".contact_form").height();
	var popupWidth = $(".contact_form").width();
	
	var popupHeight1 = $(".profile_popup").height();
	var popupWidth1 = $(".profile_popup").width();
	
	// center popup
	$(".contact_form").css({
		"position": "absolute"		
	});
	
	$(".profile_popup").css({
		"position": "absolute",
		"top": (windowHeight/2)-(popupHeight1/2),
		"left": (windowWidth/2)-(popupWidth1/2)
	});
	//only need force for IE6
	
	$(".background_popup").css({
		"height": windowHeight
	});
	
}

$(document).ready(function(){
	
	//LOADING POPUP
	$(".contact_popup").click(function(){
		centerPopup();
		loadPopup();
	});
			
	//CLOSING POPUP
	$("#popupContactClose,.background_popup").click(function(){
		disablePopup();
	});
	
	//Press Escape event
	$(document).keypress(function(e){
		if(e.keyCode==27 && popupStatus==1){
			disablePopup();
		}
	});
	
	$(window).resize(function(){
		centerPopup();	
	});
});