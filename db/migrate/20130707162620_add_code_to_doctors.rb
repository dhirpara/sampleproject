class AddCodeToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :code, :integer
  end
end
