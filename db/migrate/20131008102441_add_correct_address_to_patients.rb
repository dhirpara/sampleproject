class AddCorrectAddressToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_address, :text
  end
end
