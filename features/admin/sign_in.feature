Feature: Sign In

  In order to use the web application
  As a admin
  I want to sign in

  Scenario: Unsuccessful signin
    Given A admin visits the signin page
    When He submits invalid signin information
    Then He should see an error message

  Scenario: Successful signin
    Given A admin visits the signin page
    And The admin has an account
    When The admin submits valid signin information
    Then He should see welcome message
    And He should see a logout link
    And He should see a Dashboard link
    And He should see a Admin Users link
    And He should see a Doctors link
    And He should see a Procedures link
    And He should see a Specialities link