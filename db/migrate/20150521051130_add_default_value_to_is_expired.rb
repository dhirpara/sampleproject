class AddDefaultValueToIsExpired < ActiveRecord::Migration
  def up
  	change_column :doctors, :is_expired, :boolean, :default => false
	end

	def down
		change_column :doctors, :is_expired, :boolean, :default => false
	end
end
