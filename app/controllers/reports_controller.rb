class ReportsController < ApplicationController

  def login_page
    logger.info ">>>>>>>>>>>>>>>>login_page"
    session[:patient_id] = params[:id]
    session[:con_start]= Time.now
    session[:login_page]= Time.now
    logger.info "connection_establish:- >>>>>>>>>>>>>> #{session[:con_start]}"
  end

  def pat_report
    @patient = Patient.find(params[:id])
    @patient_reports = @patient.reports
  end

  def access_report
      logger.info ">>>>>>>>>>>>>>>> access_report"
      @report_code = params[:report_code].delete(' ')
      @report1 = Report.find_by_report_code(@report_code)

      if !params[:report_code].blank?
        if @report1 == nil
          flash[:notice] = "Sorry! you have entered invalid Patient Passcode.Try again."
          render :action =>'login_page'
        else
          if @report1.status == "Active"
            redirect_to signature_created_msg_path
          else
            @report1.update_attributes(:connection_establish_time => session[:con_start])
            redirect_to website_desclaimer_path(@report1)
          end
        end
      else
        flash[:notice] = "Sorry! you have entered invalid Patient Passcode.Try again."
        render :action =>'login_page'
      end
  end

  def send_mail
    @report = Report.find(params[:id])
    @patient = Patient.all
    @doctor = current_doctor
    @speciality = Speciality.find_by_id(@doctor.speciality_id)
    @speciality_doc = @speciality.spe_name.capitalize

    @patient.each do |pa|
      if pa.id == @report.patient_id
        @patient_report_f = pa.first_name
        @patient_report_l = pa.last_name
        if pa.gender.to_s == "Male"
          @patient_perticuler = "He"
        else
          @patient_perticuler = "She"
        end
      end
    end

    PatientMailer.registration_confirmation(@report,@doctor,@speciality_doc).deliver
    DoctorMailer.patient_registration_confirmation(@report,@doctor,@speciality_doc,@patient_report_f,@patient_report_l,@patient_perticuler).deliver

    redirect_to root_url
    flash[:notice] = "Presentation has been successfully emailed"
  end

  def past_report
    @reports = current_doctor.reports.all
  end

  def view_report
    @report = Report.find(params[:id])
    @procedure_types = ProcedureType.all
    @report_menu = ReportMenu.all
  end

  def website_desclaimer
    @report = Report.find(params[:id])
    # cookies[:user_name] = @report.patient_first_name
    @report.update_attributes(:login_page_time_spent => session[:login_page])
    session[:website_des] = Time.now
    logger.info ">>>>>>>>>>#{session[:website_des]}>>>>>> website_desclaimer"
  end

  def i_do_not_agree
    logger.info ">>>>>>>>>>>>>>>> i_do_not_agree"
    @report = Report.find(params[:id])
    @patients = Patient.all
    @patients.each do |pa|
      if pa.id == @report.patient_id
        @patient = pa
      end
    end
    @doctors = Doctor.all
    @doctors.each do |doc|
      if doc.id == @report.doctor_id
        @doctor = doc
        @speciality = Speciality.find_by_id(@doctor.speciality_id)
        @speciality_doc = @speciality.spe_name.capitalize
      end
    end
    PatientMailer.thank_you_message(@report,@patient,@doctor,@speciality_doc).deliver
    DoctorMailer.dont_agee_notification(@report,@patient,@doctor,@speciality_doc).deliver
    logger.info ">>>>>>>>>>> #{@report.id} <<<<<<<"
  end

  def access_rep
    # @report = Report.find(params[:id])
    # w = @report.login_page_time_spent
    # e = session[:website_des]
    # @drr = w - e
    # logger.info "Difference------> #{@drr}"
    session[:personal_info]= Time.now
    @report = Report.find(params[:id])
    @report.update_attributes(:desclaimer_time_spent => session[:website_des])
    session[:report] = @report.id
  end

  def signature_create
    @report = Report.find(params[:id])
    session[:signature_time] = Time.now
    @report.update_attributes(:presentation_page_time_spent => session[:question_page],:esignature_page_time_spent => session[:signature_time])
  end

  def sig_create
    session[:sig_create] = Time.now
    @signature = params[:report]
    @report = Report.find(params[:id])
    if !@signature.blank?
      @report.update_attributes(:econnection=>session[:sig_create])
      @report.update_attributes(:signature =>@signature[:signature],:status=>"Active")
    end

    if !@report.signature.blank?
      @presentations = Presentation.all
      @patients = Patient.all
      @doctors = Doctor.all
      @report_questions= ReportQuestion.all
      @report_menu = @report.report_menus.all
      @proce_type = ProcedureType.all

      @doctors.each do |doc|
        if doc.id == @report.doctor_id
          @doctor = doc
          @speciality = Speciality.find_by_id(@doctor.speciality_id)
          @speciality_doc = @speciality.spe_name.capitalize
        end
      end

      @patients.each do |pa|
        if pa.id == @report.patient_id
          @patient = pa
        end
      end

      ReportMailer.patient_report(@report,@doctor,@speciality_doc,@patient,@report_questions,@report_menu,@presentations).deliver
      # flash[:notice] = "Thank you for your time. Presentation is successfully reviewed."
      redirect_to  login_page_path(session[:patient_id])
    else
      @report.update_attributes(:signature =>@signature[:signature],:status=>"Pending")
      flash[:notice] = 'Please provide your signature.'
      render :action =>'signature_create'
    end
  end

  def thnkyou_msg

  end

  def download_as_pdf
    @presentations = Presentation.all
    @report = Report.find(params[:id])
    @patients = Patient.all
    @doctors = Doctor.all
    @report_questions= ReportQuestion.all
    @report_menu = @report.report_menus.all
    @proce_type = ProcedureType.all

    @doctors.each do |doc|
      if doc.id == @report.doctor_id
        @doctor = doc
        @speciality = Speciality.find_by_id(@doctor.speciality_id)
        @speciality_doc = @speciality.spe_name.capitalize
      end
    end
    @patients.each do |pa|
      if pa.id == @report.patient_id
        @patient = pa
      end
    end
    pdf = ReportPdf.new(@report,@doctor,@speciality_doc,@patient,@report_questions,@report_menu,@presentations)
    send_data pdf.render, :filename => "#{@patient.first_name} #{@patient.last_name} #{@report.created_at.strftime("%d %B %Y")}.pdf", :type => "application/pdf"
  end

  def signature_created_msg
  end

end
