Feature: Audio upload

  In order to upload an audio file
  As a admin
  I want to upload audio

  Background:
    Given I am sign in as admin

  Scenario: Admin visit to audios link
    When I click on audios link
    Then I should see list of audio files

  Scenario: New audio upload form
    And I click on audios link
    When I click on new audio button
    Then I should see new audio form

  Scenario: Audio upload with valid information
    And I am on new audio upload form
    When I fill valid audio information
    Then I should be redirected to audio index page with success message