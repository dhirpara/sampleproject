class ChangeDataTypeForDoctorCode < ActiveRecord::Migration
  def up
  	change_table :doctors do |t|
      t.change :code, :string
    end
  end

  def down
  	change_table :doctors do |t|
      t.change :code, :integer
    end
  end
end
