#doctor signup with valid data

  #Scenario 1

  Given(/^I am on doctor signup page$/) do
    @speciality = FactoryGirl.create(:speciality, :spe_name => "doesn")
    visit "/doctors/sign_up"
  end

  Given(/^I click on i agree button$/) do
    using_wait_time 3 do
      click('I Agree')
    end
  end

  Given(/^I select speciality "(.*?)"$/) do |arg1|
    select @speciality.spe_name, :from => "doctor_speciality_id"
  end

  Given(/^I click on next button$/) do
    click_button "Next"
  end

  When(/^I fill all doctor information$/) do
    fill_in('Email', :with => 'doctor@yahoo.com')
    fill_in('Password', :with => 'passwords')
    fill_in('Password confirmation', :with => 'passwords')
    fill_in('Doctor name', :with => 'doc')
    fill_in('City', :with => 'ahmedabad')
    fill_in('State', :with => 'guj')
    # fill_in('Address', :with => 'tha')
    fill_in('Zip', :with => '2020')
    fill_in('Practise name', :with => 'dentist')
    choose('doctor_sex_male')
    fill_in('Phone', :with => '2020')
  end

  Then(/^I click on finish button$/) do
    click_button "Next"
  end

  Then(/^I should be redirected to select plan path$/) do
    get send("select_plan_path")
    page.status_code.should == 200
  end

  Then(/^I can see Advance Plan as title$/) do
    page.should have_selector('h2',"Advance Plan")
  end

#Scenario 2

  When(/^I click on i do not agree link$/) do
    click_link "I do not Agree"
  end

  Then(/^I should be redirected to root$/) do
    get send("root_url")
    page.status_code.should == 200
  end

#Scenario 3

  When(/^I fill doctor email incorrect and blank password$/) do
    fill_in('Email', :with => 'doctoryahoo.com')
    fill_in('Password', :with => '')
    fill_in('Password confirmation', :with => '')
    fill_in('Doctor name', :with => 'doc')
    fill_in('City', :with => 'ahmedabad')
    fill_in('State', :with => 'guj')
    #fill_in('Address / suite', :with => 'tha')
    fill_in('Zip', :with => '2020')
    fill_in('Practise name', :with => 'dentist')
    choose('doctor_sex_male')
    fill_in('Phone', :with => '2020')
  end

  Then(/^I should see email cant blank and Password can't be blank$/) do
    page.should have_content("Email is invalid")
    page.should have_content("Password can't be blank")
  end

#Scenario 4

  When(/^I sign up without a password$/) do
    visit "/doctors/sign_up"
    fill_in('Password', :with => "")
    click_button 'Next'
  end

  Then(/^I should see a missing password cant be blank$/) do
    page.should have_content "Password can't be blank"
  end

#Scenario 5

  When(/^I sign up with a mismatched password confirmation$/) do
    visit "/doctors/sign_up"
    fill_in('Password', :with => 'pass')
    fill_in('Password confirmation', :with => 'password')
    click_button('Next')
  end

  Then(/^I should see a Password doesn't match confirmation$/) do
    page.should have_content "Password doesn't match confirmation"
  end

#-------------------doctor sign in steps ----------------------------------

Given(/^a doctor visits the signin page$/) do
   visit root_path
end

When(/^he submits invalid signin information$/) do
  click_button "Log in Now!"
end

Then(/^he should see an error message$/) do
 page.should have_content "Invalid email or password."
end

Given(/^the doctor has an account$/) do
  @doctor = FactoryGirl.create(:doctor)
end

Given(/^the doctor submits valid signin information$/) do
  within('.subscribe-form') do
    fill_in "Email", with: @doctor.email
    fill_in "Password", with: @doctor.password
    click_button "Log in Now!"
  end
end

Then(/^he should see welcome message$/) do
  page.should have_content "Signed in successfully."
end

Then(/^he should see a Add patient link$/) do
  page.should have_link('New Patient', href: new_patient_path)
end


Then(/^he should see a signout link$/) do
  page.should have_link('Logout', href:  destroy_doctor_session_path)
end

#---------------------------doctor remember me steps ----------------------------------------------

Given(/^a doctor visits  signin page$/) do
   visit root_path
end

And(/^the doctor has a  valid account$/) do
  @doctor = FactoryGirl.create(:doctor)
end

When(/^the doctor submits valid information$/) do
  within('.subscribe-form') do
    fill_in "Email", with: @doctor.email
    fill_in "Password", with: @doctor.password
  end
end

And(/^checks Remember me box$/) do
  within('.subscribe-form') do
    check('Remember me !')
    click_button "Sign in"
  end
end

Then(/^Session token must be generated$/) do
  @doctor = Doctor.create(:remember_me => "1")
end

#---------------------------doctor forgot password steps ----------------------------------------------

Before do
    ActionMailer::Base.deliveries.clear
end

Given(/^a doctor visits signin page$/) do
  visit root_path
end

And(/^the doctor has a valid account$/) do
  @doctor = FactoryGirl.create(:doctor)
end

When(/^doctor clicks on forgot password link$/) do
  within('.subscribe-form') do
    click_link "Forgot Your Password"
  end
end

Then(/^he should be asked to enter email$/) do
  visit new_doctor_password_path
  within('.details') do
    fill_in "Email", with: @doctor.email
    click_button ('Send me reset password instructions')
  end
end

And(/^mail should be sent to the email$/) do
  ActionMailer::Base.deliveries.should_not be_empty
end


#---------------------------doctor sign out steps ----------------------------------------------

Given(/^I am logged in$/) do
  @doctor = create(:doctor)
  visit root_path
  within('.subscribe-form') do
    fill_in "Email", with: @doctor.email
    fill_in "Password", with: @doctor.password
    click_button "Log in Now!"
  end
end

When(/^I sign out$/) do
  visit '/doctors/sign_out'
end


Then(/^I should see a signed out message$/) do
  page.should have_content "Signed out successfully."
end

When(/^I return to the site$/) do
  visit root_path
end

Then(/^I should be signed out$/) do
  page.should have_content "Subscribe Now"
  # page.should have_content "Doctor Login"
  # page.should_not have_content "Log in Now!"
end