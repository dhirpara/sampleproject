class AddParentTypeIdToProcedureTypes < ActiveRecord::Migration
  def change
    add_column :procedure_types, :parent_type_id, :integer
  end
end
