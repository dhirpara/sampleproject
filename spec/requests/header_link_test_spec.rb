require 'spec_helper'
describe "Home Page" do
  describe "Index Page" do
    before { visit root_path }
    subject { page }
    it { should have_link('How it Works', href:  home_coming_soon_path) }
    it { should have_link('Services', href: home_coming_soon_path) }
    it { should have_link('Advantages', href: home_coming_soon_path) }
    it { should have_link('Subscription Plans', href: plan_detail_path) }
    it { should have_link('Contact us', href: home_coming_soon_path) }
  end
end