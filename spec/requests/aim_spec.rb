require "spec_helper"

  describe AuthorizeNet::AIM::Transaction do
    before :all do
      begin
        creds = YAML.load_file(File.dirname(__FILE__) + "/credentials.yml")
        @api_key = creds['api_transaction_key']
        @api_login = creds['api_login_id']
        @md5_value = creds['md5_value']
        rescue Errno::ENOENT => e
        @api_key = "LIVE"
        @api_login = "LIVE"
        @md5_value = "LIVE"
      end
    end

    before do
      @type = AuthorizeNet::AIM::Transaction::Type::AUTHORIZE_AND_CAPTURE
      @test_mode = false
      @gateway = :production
      @amount = (rand(10000) + 100) / 100.0
      @credit_card = AuthorizeNet::CreditCard.new('4111111111111111', '01' + (Time.now + (3600 * 24 * 365)).strftime('%y'))
      @response = '1,1,1,This transaction has been  approved.,000000,P,0,,,10.00,CC,auth_capture,,,,,,,,,,,,,,,,,,,,,,,,,,7A3C09A367FED29C9902038440CD8A52,,,,,,,,,,,,,XXXX0027,Visa,,,,,,,,,,,,,,,,'
    end



    it "should support instantiation" do
      AuthorizeNet::AIM::Transaction.new(@api_login, @api_key).should be_instance_of(AuthorizeNet::AIM::Transaction)
    end

    it "should know if its in test mode" do
      transaction = AuthorizeNet::AIM::Transaction.new(@api_login, @api_key, :transaction_type => @type, :gateway => AuthorizeNet::AIM::Transaction::Gateway::TEST, :test => true)
      transaction.test?.should be_true
      transaction = AuthorizeNet::AIM::Transaction.new(@api_login, @api_key, :transaction_type => @type, :gateway => AuthorizeNet::AIM::Transaction::Gateway::TEST, :test => false)
      transaction.test?.should be_true
      transaction = AuthorizeNet::AIM::Transaction.new(@api_login, @api_key, :transaction_type => @type, :gateway => AuthorizeNet::AIM::Transaction::Gateway::LIVE, :test => true)
      transaction.test?.should be_true
      transaction = AuthorizeNet::AIM::Transaction.new(@api_login, @api_key, :transaction_type => @type, :gateway => AuthorizeNet::AIM::Transaction::Gateway::LIVE, :test => false)
      transaction.test?.should be_false
    end

    it "should not have a response if the transaciton hasn't been run" do
      transaction = AuthorizeNet::AIM::Transaction.new(@api_login, @api_key, :transaction_type => @type, :gateway => @gateway, :test => @test_mode)
      transaction.has_response?.should be_false
    end

    it "should support the returning its response object" do
      transaction = AuthorizeNet::AIM::Transaction.new(@api_login, @api_key, :transaction_type => @type, :gateway => @gateway, :test => @test_mode)
      transaction.should respond_to(:response)
    end

    it "should be able to run a transaction" do
      transaction = AuthorizeNet::AIM::Transaction.new(@api_login, @api_key, :transaction_type => @type, :gateway => @gateway, :test => @test_mode)
      transaction.run.should be_kind_of(AuthorizeNet::AIM::Response)
    end
  
    it "should not be able to run a transaction a second time" do
      transaction = AuthorizeNet::AIM::Transaction.new(@api_login, @api_key, :transaction_type => @type, :gateway => @gateway, :test => @test_mode)
      transaction.run.should be_kind_of(AuthorizeNet::AIM::Response)
      transaction.run.should be_nil
    end

  end