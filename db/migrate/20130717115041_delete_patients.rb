class DeletePatients < ActiveRecord::Migration
  def up
    drop_table :patients
  end

  def down
    create_table :patients do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.text :address
      t.string :gender
      t.string :city
      t.string :state
      t.string :country
      t.integer :phone_code
      t.integer :phone_number
      t.date :birthdate
      t.integer :procedure_id
      t.date :appointment_date
      t.time :appointment_time

      t.timestamps
    end
  end
end
