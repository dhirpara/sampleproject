class AddCardCodeToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :card_code, :integer
  end
end
