class RemoveStatusFromPatientsTestimonials < ActiveRecord::Migration
  def up
    remove_column :patients_testimonials, :status
  end

  def down
    add_column :patients_testimonials, :status, :string
  end
end
