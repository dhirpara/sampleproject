class AddCorrectEmailToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_email, :string
  end
end
