
Given(/^Admin is already logged in$/) do
	@admin = FactoryGirl.create(:admin_user, :email => 'admin@example.com', :password => 'password')
    visit new_admin_admin_user_path
    fill_in 'Email', :with => @admin.email
    fill_in 'Password', :with => @admin.password
    click_button('Login')
end

When(/^Admin clicks on Patients Link$/) do
  click_link "Patients"
end

Then(/^he should see firstname,lastname,city and doctor's name$/) do
	visit admin_patients_path
	expect(page).to have_selector('a', text: @first_name)
	expect(page).to have_selector('a', text: @last_name)
	expect(page).to have_selector('a', text: @city)
	expect(page).to have_selector('a', text: @doctor_name)
end

Then(/^he should be able to edit,delete and view the user$/) do
	page.should have_link(@default_actions)
end

Then(/^He should see New Patient Link$/) do
	expect(page).to have_link('New Patient', href: new_admin_patient_path)
end
