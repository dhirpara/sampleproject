  Feature: Sign In

  In order to use the web application
  As a doctor
  I want to sign in

  Scenario: Unsuccessful signin
    Given a doctor visits the signin page
    When he submits invalid signin information
    Then he should see an error message

  Scenario: Successful signin
    Given a doctor visits the signin page
    And the doctor has an account
    When the doctor submits valid signin information
    Then he should see welcome message
    And he should see a signout link
    And he should see a Add patient link

  Scenario: Remember me functionality
    Given a doctor visits signin page
    And the doctor has a valid account
    When the doctor submits valid information
    And checks Remember me box
    Then Session token must be generated 

  Scenario: Forgot Password functionality
    Given a doctor visits  signin page
    And the doctor has a  valid account
    When doctor clicks on forgot password link
    Then he should be asked to enter email
    And mail should be sent to the email 

