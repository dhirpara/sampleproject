class RemoveColumnExpireDateFromPayments < ActiveRecord::Migration
  def up
    remove_column :payments, :expire_date
  end

  def down
    add_column :payments, :expire_date, :date
  end
end
