class ReportMenu < ActiveRecord::Base
  attr_accessible :procedure_id, :procedure_type_ids, :report_id, :type_subtype_id,:video_count,:video_section_time, :sequence, :query_str
  belongs_to :report
  belongs_to :procedure_type, :class_name => "ReportMenu"
  belongs_to :procedure
  has_many :report_questions
  before_create :set_video_cnt

   def set_video_cnt
    self.video_count=0
  end
end
