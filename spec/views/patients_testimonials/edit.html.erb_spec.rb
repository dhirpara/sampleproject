require 'spec_helper'

describe "patients_testimonials/edit" do
  before(:each) do
    @patients_testimonial = assign(:patients_testimonial, stub_model(PatientsTestimonial,
      :description => "MyText",
      :status => "MyString",
      :patient => nil
    ))
  end

  it "renders the edit patients_testimonial form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", patients_testimonial_path(@patients_testimonial), "post" do
      assert_select "textarea#patients_testimonial_description[name=?]", "patients_testimonial[description]"
      assert_select "input#patients_testimonial_status[name=?]", "patients_testimonial[status]"
      assert_select "input#patients_testimonial_patient[name=?]", "patients_testimonial[patient]"
    end
  end
end
