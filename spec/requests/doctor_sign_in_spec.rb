require "spec_helper"
  include Devise::TestHelpers

  describe "user sign in" do

    it "allows users to sign in after they have registered" do
      @doctor = FactoryGirl.create(:doctor)
      visit root_path
        within('.subscribe-form') do
          fill_in "Email",    :with => @doctor.email
          fill_in "Password", :with => @doctor.password
          click_button "Log in Now!" 
        end
      page.should have_content("Signed in successfully.")
    end
end