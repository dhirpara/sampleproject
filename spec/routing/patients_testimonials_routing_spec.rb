require "spec_helper"

describe PatientsTestimonialsController do
  describe "routing" do

    it "routes to #index" do
      get("/patients_testimonials").should route_to("patients_testimonials#index")
    end

    it "routes to #new" do
      get("/patients_testimonials/new").should route_to("patients_testimonials#new")
    end

    it "routes to #show" do
      get("/patients_testimonials/1").should route_to("patients_testimonials#show", :id => "1")
    end

    it "routes to #edit" do
      get("/patients_testimonials/1/edit").should route_to("patients_testimonials#edit", :id => "1")
    end

    it "routes to #create" do
      post("/patients_testimonials").should route_to("patients_testimonials#create")
    end

    it "routes to #update" do
      put("/patients_testimonials/1").should route_to("patients_testimonials#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/patients_testimonials/1").should route_to("patients_testimonials#destroy", :id => "1")
    end

  end
end
