class AddVideoStatusToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :video_status, :boolean
  end
end
