class Patient < ActiveRecord::Base

  before_save { |patient| patient.email = patient.email.downcase }

  #belongs_to :procedure
  belongs_to :doctor
  has_many :reports
  delegate :doctor_name, :to => :doctor, :prefix => true

  # VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  # VALID_EMAIL_REGEX = /\A[^\.]+[-a-z0-9_+\.]+[^\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}\z/i
  VALID_EMAIL_REGEX = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i
  VALID_ZIP_REGEX = /\A[-a-zA-Z0-9\s]*\z/i
  VALID_PHONE_REGEX = /\A[-0-9\s]*\z/i

  validates :first_name, :last_name, :address, :gender, :email, :doctor_id, :phone_number, :city, :country, :state, :zip, :birthdate, :presence => true
  validates :email, format: { with: VALID_EMAIL_REGEX }, uniqueness: true
  validates :zip, format: { with: VALID_ZIP_REGEX }
  validates :phone_number, format: { with: VALID_PHONE_REGEX }

  #before_create :generate_token
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable

  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  #attr_accessible :password, :password_confirmation, :remember_me,:token

  attr_accessible :address, :appointment_date, :appointment_time, :birthdate, :city, :country, :email, :first_name, :gender, :last_name, :phone_code, :phone_number, :state, :doctor_id,:zip
  attr_accessible :correct_address, :correct_appointment_date, :correct_appointment_time, :correct_birthdate, :correct_city, :correct_country, :correct_email, :correct_first_name, :correct_gender_number, :correct_last_name, :correct_phone_code, :correct_phone_number, :correct_state, :correct_zip

  def generate_token
    self.token = Digest::SHA1.hexdigest([Time.now, rand].join)
  end

  def name
    [first_name, last_name, email].join "  "
  end

  def fullname
    [first_name, last_name].join "  "
  end 
end