module ApplicationHelper
	def remote_request(type, path, params={}, target_tag_id)
	  "$.#{type}('#{path}',
	             {#{params.collect { |p| "#{p[0]}: #{p[1]}" }.join(", ")}},
	             function(data) {$('##{target_tag_id}').html(data);}
	   );"
	end
  def resource_name
    :doctor
  end

  def resource
    @resource ||= Doctor.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:doctor]
  end

  def doctor_resource?
    if doctor_signed_in?
      true
    end
  end

end

