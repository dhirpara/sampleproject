#Scenario 1

  Given(/^I am sign in as admin$/) do
    @admin = FactoryGirl.create(:admin_user, :email => 'admin@example.com', :password => 'password')
    visit new_admin_admin_user_path
    fill_in 'Email', :with => @admin.email
    fill_in 'Password', :with => @admin.password
    click_button('Login')
  end

  When(/^I click on photos link$/) do
    click_link 'Photos'
  end

  Then(/^I should see list of images$/) do
    visit '/admin/photos'
  end

#Scenario 2

  When(/^I click on new photo button$/) do
    click_link "New Photo"
  end

  Then(/^I should see new photo form$/) do
    visit '/admin/photos/new'
  end

#Scenario 3

  Given(/^I am on new photo upload form$/) do
    visit new_admin_photo_path
  end

  When(/^I fill image valid information$/) do
    attach_file('Image', "#{Rails.root}/features/rails.jpg")
    click_button('Save Photo')
  end

  # Then(/^I should be redirected to index show page with success message$/) do
  #   visit '/admin/photos'
  # end
  
  Then(/^I should be redirected to image show page with success message$/) do
    pending # express the regexp above with the code you wish you had
  end
