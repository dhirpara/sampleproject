class DoctorProfileController < ApplicationController
  before_filter :authenticate_doctor!, :only => [:edit, :update, :index]
  skip_before_filter :check_plan_expiration, except: [:doctor_cancel_trials]

  def index
    @doctor_profile = current_doctor
    @specialities = Speciality.all
    respond_to do |format|
      format.html { render :layout => false }
      format.js
    end
  end

  def edit
  	@doctor_profile = current_doctor
    respond_to do |format|
      format.js
      format.html { render :layout => false }
    end
  end

  def update
    @doctor_profile = current_doctor
    respond_to do |format|
      if @doctor_profile.update_attributes(params[:doctor].except(:doctor_name))
        format.html { redirect_to patients_path, notice: 'Profile was successfully updated.' }
        format.json { head :no_content }
         format.html { render :layout => false }
        format.js
      else
        format.html { render "edit" }
        format.json { render json: @doctor_profile.errors, status: :unprocessable_entity }
        format.html { render :layout => false }
        format.js
      end
    end
  end

  def doctor_cancel_trials
    @doctor = Doctor.find(params[:id])
    @doctor.update_attributes(subscription_plan: "Expired", expire_date: Time.zone.now,trial_expired: true, trial_period: "0")
    @job = Delayed::Job.find_by_queue("doctor-#{@doctor.id}")
    @job.delete if @job
    redirect_to root_path
  end
end
