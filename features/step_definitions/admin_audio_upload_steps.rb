When(/^I click on audios link$/) do
  click_link "Audios"
end

Then(/^I should see list of audio files$/) do
  visit '/admin/audios'
end

When(/^I click on new audio button$/) do
  click_link "New Audio"
end

Then(/^I should see new audio form$/) do
  visit '/admin/audios/new'
end

Given(/^I am on new audio upload form$/) do
  visit '/admin/audios/new'
end

When(/^I fill valid audio information$/) do
  attach_file('Audio file', "#{Rails.root}/features/1.mp3")
  click_button "Create Audio"
end

Then(/^I should be redirected to audio index page with success message$/) do
  visit '/admin/audios'
end

