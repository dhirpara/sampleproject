class SignaturePadsController < ApplicationController
  def open_form
    # @patient = Patient.find(params[:id])

    respond_to do |format|
      format.html { render :layout => true }
      format.json
    end
  end

  def send_mail
    @patient = Patient.find(params[:id])
    PatientMailer.confirm_report(@patient).deliver
    redirect_to root_url
  end

  def confirm_signature
    # @signature = Signature.new
     @patient = Patient.find(params[:pat_id])
  end

  def sig_create
    @signature = Signature.new(params[:signature])

    if @signature.save
      @signature.update_attributes(:flag => true,:report_id =>params[:id])
      flash[:notice] = "Thank you for your time. Presentation is successfully reviewed."
      redirect_to root_url
    else
      render 'confirm_signature'
    end
  end
end