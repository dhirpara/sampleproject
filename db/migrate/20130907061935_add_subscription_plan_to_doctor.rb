class AddSubscriptionPlanToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :subscription_plan, :string
  end
end
