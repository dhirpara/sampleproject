class AddJsKeyToProcedureTypes < ActiveRecord::Migration
  def change
    add_column :procedure_types, :js_key, :string
  end
end
