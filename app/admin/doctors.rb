ActiveAdmin.register Doctor do
  menu :priority => 3
  form :partial => "form"

  config.comments = false

  member_action :pat_show, :title => "Patients Information " do
    @d = Doctor.find(params[:doctor_id])
  end

  member_action :active_doctors do
    @active_doctors = Doctor.where("subscription_plan IS NOT ?", nil).where("subscription_date IS NOT ?", nil)
  end

  member_action :non_active_doctors do
    @non_active_doctors = Doctor.where("subscription_plan IS ?", nil).where("subscription_date IS ?", nil)
  end

  member_action :admin_cancel_trials do
    @doctor = Doctor.find(params[:id])
    @doctor.update_attributes(subscription_plan: "Expired", expire_date: Time.zone.now, trial_expired: true, trial_period: "0")
    @job = Delayed::Job.find_by_queue("doctor-#{@doctor.id}")
    @job.delete if @job
    redirect_to :action => :edit
  end

  index do
    selectable_column

    column :id
    column "Doctor Name" do |doctor|
      link_to doctor.doctor_name.capitalize, admin_doctor_path(doctor)
    end

    column "Speciality" do |doctor|
      doctor.speciality_spe_name
    end

    column :email
    column :city
    column :state
    column "Plan" do |doctor|
      if doctor.subscription_plan.nil?
        "Not Activated Yet"
      elsif doctor.subscription_plan == "Free"
        # !doctor.expire_date.nil? ? rem = ((doctor.expire_date).to_date - (Time.zone.now).to_date ).to_i : rem = 1
	    	if (doctor.expire_date.to_date - Time.zone.now.to_date ).to_i <= 0
	      	doctor.update_attributes(subscription_plan: "Expired", trial_expired: true, trial_period: "0")
		      doctor.subscription_plan
		      @job = Delayed::Job.find_by_queue("doctor-#{doctor.id}")
		      @job.delete if @job
		    else
	      	"Free (" + doctor.trial_period.to_s + " days)"
	      end
	    elsif doctor.subscription_plan == "Unlimited"
	      "Unlimited"
      elsif doctor.subscription_plan == "Expired" || doctor.expire_date.to_datetime < Time.zone.now
        doctor.update_attributes(subscription_plan: "Expired")
        "Expired"
      else
        doctor.subscription_plan
	    end
    end
    column "Has Paid?" do |doctor|
      if doctor.subscription_plan.nil? || ["Free", "Unlimited","Expired"].include?(doctor.subscription_plan)
        div :class => "has_not_paid" do
          'No'
        end
      else
        div :class => "has_paid" do
          'Yes'
        end
      end
    end
    column "Subscription Start" do |doctor|
      if ["Expired", nil, ""].include?(doctor.subscription_plan)
        "-"
      else
        doctor.subscription_date.to_date
      end
    end
    column "Subscription End" do |doctor|
      if ["Unlimited", "Expired", nil, ""].include?(doctor.subscription_plan)
        "-"
      else
        doctor.expire_date.to_date
      end
    end

    default_actions
    column do |post|
      link_to "View Patients", pat_show_admin_doctor_path(post)
    end
  end

  # form do |f|
  #   f.inputs "Details" do
  #   #   if Doctor.find(params[:id]).subscription_plan == "Free" || 0
  #   #     f.input :trial_period,  as: :select, collection: ["30","60","90","120", ["Unlimited", "0"]], :include_blank => false
  #   #     puts "helloo"
  #   #     # input :type => :text
  #   #     # f.input :city, :label => "heloo"
  #   #     # render partial: 'cancel_trial'
  #   #   end
  #   #   # render partial: 'cancel_trial', :layout => true

  #   #   f.input :speciality, :label => "Speciality", :include_blank => "Please select...", :collection => Speciality.all.map{ |s| [s.spe_name, s.id] }
  #   #   f.input :doctor_name, :label => "Doctor Name"
  #   #   f.input :email, :label => "Email"
  #   #   f.input :password, :label => "Password"
  #   #   f.input :practise_name
  #   #   f.input :state
  #   #   f.input :city
  #   #   f.input :address, :input_html => { :rows => 5, :cols => 30, :maxlength => 10  }
  #   #   f.input :zip
  #   #   f.input :code
  #   #   f.input :phone
  #   #   f.input :sex, :as => "radio", :collection => ["Male","Female"]
  #   # end
  #   # f.actions
  #   # render 'form'
  # end

  show do
    attributes_table do
      row :doctor_name
      row :email
      row :practise_name
      row :speciality_spe_name
      row :sex
      row :city
      row :state
      row :address
      row :zip
      row :code
      row :phone
      row "subscription_plan" do |doctor|
        if doctor.subscription_plan == nil
          "Not Yet Activated"
        elsif doctor.subscription_plan == "Free"
          doctor.trial_period.to_s + " days free trial (" + ((doctor.expire_date.to_date - Time.zone.now.to_date).to_i).to_s + " days remaining)"
        else
          doctor.subscription_plan
        end
      end
      row "Subscription Start" do |doctor|
        if ["Expired", nil, ""].include?(doctor.subscription_plan)
          "-"
        else
          doctor.subscription_date.to_date
        end
      end
      row "Subscription End" do |doctor|
        if ["Unlimited", "Expired", nil, ""].include?(doctor.subscription_plan)
          "-"
        else
          doctor.expire_date.to_date
        end
      end
    end
 end
  filter :email
  filter :reset_password_sent_at
  filter :remember_created_at
  filter :current_sign_in_at
  filter :last_sign_in_at
  filter :currnet_sign_in_ip
  filter :last_sign_in_ip
  filter :created_at
  filter :updated_at
  filter :doctor_name
  filter :practise_name


	controller do
	  def update
      if params[:doctor][:password].blank?
        params[:doctor].delete("password")
      end
      @doctor = Doctor.find(params[:id])
      trial_period = @doctor.trial_period

      # Update Plan
      if super && params[:doctor][:trial_period].present?
        if  params[:doctor][:trial_period] != "0"
          @doctor.update_attributes(subscription_plan: "Free", expire_date: Time.parse(@doctor.subscription_date) + params[:doctor][:trial_period].to_i.days)
        else
          @doctor.update_attributes(subscription_plan: "Unlimited", subscription_date: Time.zone.now(), expire_date: nil)
        end
      end

      # Update expiration notification mail sending timing
      @job = Delayed::Job.find_by_queue("doctor-#{@doctor.id}")
      rem = (@doctor.expire_date.to_date - Time.zone.now.to_date ).to_i unless @doctor.expire_date.nil?
      if @doctor.subscription_plan == "Free"
        if rem <= 0
          @doctor.update_attributes(subscription_plan: "Expired", trial_expired: true, trial_period: "0")
          @job.delete if @job
        elsif trial_period != @doctor.trial_period
          if rem <= 7
            @job.delete if @job
            DoctorMailer.delay(queue: "doctor-#{@doctor.id}", run_at: Time.zone.now()).trial_expired_notification(@doctor)
          else
            if @job
              @job.update_attributes(run_at: @doctor.expire_date.to_date - 7.days)
            else
              DoctorMailer.delay(queue: "doctor-#{@doctor.id}", run_at: @doctor.expire_date.to_date - 7.days).trial_expired_notification(@doctor)
            end
          end
        end
      elsif @doctor.subscription_plan == "Unlimited"
        @job.delete if @job
      end
	  end
	end
end