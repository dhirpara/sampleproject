class AddVideoCountToReport < ActiveRecord::Migration
  def change
    add_column :reports, :video_count, :integer
  end
end
