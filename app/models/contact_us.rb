class ContactUs < ActiveRecord::Base
  attr_accessible :comment, :email, :name, :phone
end
