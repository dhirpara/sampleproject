class ReportPdf < Prawn::Document


  def initialize(report,doctor,speciality,patient,rq,report_menu,proce_type)
    super()
    @report = report
    @doctor =doctor
    @speciality =speciality
    @patient = patient
    @report_menu = report_menu
    @presentations = proce_type
    # @connection_establish = ActiveSupport::TimeZone["Central Time (US & Canada)"].parse(DateTime.now.strftime("%d %B, %Y  %I:%M %p").to_s)
    @connection_establish = @report.connection_establish_time.utc.in_time_zone("Eastern Time (US & Canada)")
    puts "--------------------------#{@connection_establish}"
    # @connection_establish=@report.connection_establish_time
    login_page_time=@report.login_page_time_spent
    desclaimer_page_time=@report.desclaimer_time_spent
    personal_confirmation_time_spent=@report.personal_info_confirmation_time_spent
    presentation_page_time=@report.presentation_page_time_spent
    esignature_page_time = @report.esignature_page_time_spent
    @esignature_page_time_new = @report.esignature_page_time_spent

    es = @report.econnection
    @esi = es
    @presentations_end = @esi.utc.in_time_zone("Eastern Time (US & Canada)")
    @presentation_time = presentation_page_time

    # Time.now.utc.in_time_zone("Eastern Time (US & Canada)")
    def self.distance_between(start_date, end_date)
      difference = end_date - start_date
      difference_int = difference.to_i
      seconds = difference_int % 60
      minutes = (difference_int / 60) % 60
      hours = difference_int / 3600

      return "#{hours.to_s}:#{minutes.to_s}:#{seconds.to_s}"
    end

    @login_page_time_spent = distance_between(login_page_time,desclaimer_page_time)
    @desclaimer_page_time = distance_between(desclaimer_page_time,personal_confirmation_time_spent)
    puts "-------------------#{personal_confirmation_time_spent}------"
    puts "-------------------#{@report_menu.first.video_section_time}------"

    @personal_confirmation_time = distance_between(personal_confirmation_time_spent,@report_menu.first.video_section_time)
    @presentation_page_time = distance_between(presentation_page_time,esignature_page_time)
    @esignature_page_time = distance_between(esignature_page_time,es)
    @report_questions=rq

    @doctor_first_last= @doctor.doctor_name
    @doctor_full_name = @doctor_first_last.strip.split(/\s+/).map(&:titleize)


  end

  def render

    doc = Prawn::Document.new

    # Draw some stuff...
    doc.image "#{Rails.root}/app/assets/images/pdf/logo.png",:width => 250, :height => 53,:position => 150

    doc.move_down 20

    doc.indent 380, 0 do
      doc.text "Location",:inline_format => true, :size => 17,:color => "01667e"
    end

    doc.indent 380,10 do
      @add = @doctor.address.titleize
      @b = @add.split(" ,")
      @c= @b.join(",")
      @d = @c.split(", ")
      @e = @d.join(",")

      doc.text @e.to_s,:inline_format => true,:size => 10,:color => "01667e"
    end

    doc.indent 380,20 do
      doc.text @doctor.city.to_s.titleize+","+@doctor.state.to_s.titleize+","+@doctor.zip.to_s+".",:inline_format => true,:size => 10,:color => "01667e"
    end

      # doc.move_down 20

      # doc.formatted_text_box [
      #   { :text => "Location",:size => 15,
      #   :color => "01667e"}
      #   ], :at => [380, 640], :width => 100, :height => 30

      # doc.formatted_text_box [
      #   { :text => @doctor.address.to_s+","+@doctor.city.to_s+","+@doctor.state.to_s+","+@doctor.zip.to_s+".",
      #     :size => 10,
      #   :color => "01667e"}
      #   ], :at => [380, 620], :width => 200, :height => 60

      doc.formatted_text_box [
        { :text => "Dr. #{@doctor_full_name[0]} #{@doctor_full_name[1]}",:size => 15,
        :color => "01667e"}
        ], :at => [20, 640], :width => 300, :height => 30

      doc.formatted_text_box [
        { :text => @doctor.phone,
        :color => "01667e"}
        ], :at => [20, 620], :width => 100, :height => 30

      # doc.formatted_text_box [
      #   { :text => @speciality,
      #     :size => 10,
      #   :color => "01667e"}
      #   ], :at => [20, 620], :width => 100, :height => 30

      # doc.formatted_text_box [
      #   { :text => "Contact: "+@doctor.code+"-"+ @doctor.phone,
      #     :size => 10,
      #   :color => "01667e"}
      #   ], :at => [20, 605], :width => 300, :height => 30

      # doc.formatted_text_box [
      #   { :text => "Email: "+@doctor.email,
      #     :size => 10,
      #   :color => "01667e"}
      #   ], :at => [20, 590], :width => 300, :height => 30

      doc.move_down 10

      doc.indent 20, 20 do
        doc.table([[ {:content => '<b><i><color rgb="ffffff">Patient Details</color></b></i>', :colspan => 2, :width => 500 } ]],:row_colors => ["0e98c7"],:cell_style => {:size => 10, :inline_format => true,:border_color => "0e98c7"})
      end
      @add = @patient.address.titleize
      @b = @add.split(" ,")
      @c= @b.join(",")
      @d = @c.split(", ")
      @e = @d.join(",")

      doc.table([["Patient Name","#{@patient.first_name.titleize} #{@patient.last_name.titleize}"],
            ["Address","#{@e}"],
            ["City","#{@patient.city.titleize}"],
            ["State","#{@patient.state.titleize}"],
            ["Zip","#{@patient.zip}"],
            ["Date of Birth","#{@patient.birthdate.strftime("%d %B, %Y")}"]
          ], :position => :center, :row_colors => ["ffffff", "f4fcff"], :column_widths => {0 => 100, 1 => 400},:cell_style => { :size => 8,:inline_format => true,:border_color => "0e98c7"
      })

      doc.move_down 20

      doc.indent 20, 20 do
        doc.text "<b><i>Presentation Preview Started on :- #{@connection_establish.strftime("%d %B, %Y  %I:%M:%S %p %Z")} </i></b>",:inline_format => true, :size => 8
      end

      doc.move_down 10

      doc.indent 20, 20 do
        doc.table([ ["<b><i><color rgb='ffffff'>Segment Name</color></b></i>","<b><i><color rgb='ffffff'>Time Spent</color></b></i>","<b><i><color rgb='ffffff'>No.of Replays</color></b></i>"],
            ], :position => :center,:row_colors => ["0e98c7"], :column_widths => {0 => 250, 1 => 125,2=>125},:cell_style => { :size => 10,:inline_format => true,:border_color => "0e98c7"
      })
      end
       doc.table([ ["Login Page Total","#{@login_page_time_spent}","N/A"],
            ["Disclaimer Page","#{@desclaimer_page_time}","N/A"],
            ["Personal Info Confirmation Page","#{@personal_confirmation_time}","N/A"],
            ], :position => :center, :row_colors => ["ffffff", "f4fcff"], :column_widths => {0 => 250, 1 => 125,2=>125},:cell_style => { :size => 8,:inline_format => true,:border_color => "0e98c7"
      })


      @a = Array.new

      @report_menu.each do |rep_menu|
        @presentations.each do |pre_type|
          if pre_type.js_key == rep_menu.sequence
            @a << rep_menu.video_section_time
          end
        end
      end

      @rev = @a.reverse
      @ans_array = []
      @cnt = 0
      @temp = 0
      @rev.each do |i|

        if @cnt == 0
          @temp = i
          @cnt = 1
          @esignature_page_time = distance_between(i,@esi)
          @ans_array << @esignature_page_time
        else
          @page_time = distance_between(i,@temp)
          @ans_array << @page_time
          @temp = i
        end
      end

      @ans_array = @ans_array.reverse
      @initialize=0

      @report_menu.each do |rep_menu|
        @presentations.each do |pre_type|
          if rep_menu.sequence == pre_type.js_key
            if @report_menu.last.id == rep_menu.id
              @last_video_time = distance_between(@report_menu.last.video_section_time,@esignature_page_time_new)

               doc.table([ ["#{pre_type.title.titleize}","#{@last_video_time }","#{rep_menu.video_count}"],
              ], :position => :center, :row_colors => ["ffffff", "f4fcff"], :column_widths => {0 => 250, 1 => 125,2=>125},:cell_style => { :size => 8,:inline_format => true,:border_color => "0e98c7"
             })
            else
               doc.table([ ["#{pre_type.title.titleize}","#{@ans_array[@initialize]}","#{rep_menu.video_count}"],
              ], :position => :center, :row_colors => ["ffffff", "f4fcff"], :column_widths => {0 => 250, 1 => 125,2=>125},:cell_style => { :size => 8,:inline_format => true,:border_color => "0e98c7"
             })
             @initialize= @initialize + 1
            end
          end
        end
      end

      doc.table([ ["E-signature Page","#{distance_between(@esignature_page_time_new,@esi)}","N/A"],
            ], :position => :center, :row_colors => ["ffffff", "f4fcff"], :column_widths => {0 => 250, 1 => 125,2=>125},:cell_style => { :size => 8,:inline_format => true,:border_color => "0e98c7"
      })

      doc.move_down 20

      doc.indent 20, 20 do
        doc.text "<b><i>Presentation Preview Ended on :- #{@presentations_end.strftime("%d %B, %Y  %I:%M:%S %p %Z")} </i></b>",:inline_format => true, :size => 8
      end

      doc.move_down 20

      doc.indent 20, 20 do
        doc.table([[ {:content => '<b><i><color rgb="ffffff">Question List</color></b></i>', :colspan => 2, :width => 500 } ]],:row_colors => ["0e98c7"],:cell_style => {:size => 10, :inline_format => true,:border_color => "0e98c7"})
      end

      @q=1
      @report_menu.each do |rep_menu|
        @presentations.each do |pre_type|
          if rep_menu.sequence == pre_type.js_key
            doc.table([ ["<b>Segment : #{pre_type.title.titleize}</b>"],
              ], :position => :center, :row_colors => ["ffffff"], :column_widths => {0 => 500},:cell_style => { :size => 12,:inline_format => true,:border_color => "0e98c7"
            })
          end
        end
        @report_questions.each do |rep_que|
          if rep_que.report_menu_id == rep_menu.id
            doc.table([ ["#{@q}. #{rep_que.question}"],
              ], :position => :center, :row_colors => ["ffffff"],:column_widths => {0 => 500},:cell_style => {:size => 8,:inline_format => true,:border_color => "0e98c7"
            })
            @q = @q+1
          end
        end
      end

      doc.move_down 10

      doc.indent 20, 20 do
        doc.text "<b><i>Patient signature:</b></i>",:size=>8,:inline_format => true
      end

      doc.move_down 5

      doc.indent 30, 20 do
        doc.text " 'I understand the proposed treatment and considered the possible alternative treatment. I consent to Proceed with the treatment <b><i>Dr. #{@doctor.doctor_name}</i></b> has proposed for me.' ",:size=>8,:inline_format => true
      end


      doc.move_down 20

      doc.font(Rails.root.join("app/assets/font/signerica_fat.ttf")) do
        doc.indent 350, 20 do
          doc.text @report.signature.to_s, :size => 20,:color => "0e98c7",:rotate => 20
           #,:rotate => 30
        end
      end

      doc.move_down 15

      doc.indent 30, 20 do
        doc.text " 'Doctor and his staff have answered my questions regarding the proposed treatment.  I have elect to proceed with the proposed treatment.' ",:size=>8,:inline_format => true
      end

      doc.move_down 10

      doc.indent 30, 20 do
        doc.text "Date : ________     Patient Signature : ______________________________     Witness Signature : ______________________________    ",:size=>8,:inline_format => true
      end

      # Return the PDF, rendered to a string
      doc.render
  end
end

