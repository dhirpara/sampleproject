class CreateReportMenus < ActiveRecord::Migration
  def change
    create_table :report_menus do |t|
      t.integer :report_id
      t.integer :procedure_id
      t.integer :procedure_type_id
      t.integer :type_subtype_id

      t.timestamps
    end
  end
end
