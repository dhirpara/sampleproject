ActiveAdmin.register ContactUs do
    
  actions :all, :except => [:edit,:new]
   
  index  do
    selectable_column
    column :id
    column :name
    column :email
    column :phone
    column :comment

    default_actions

  end

  action_item :only => :index do
    link_to "Export to CSV",admin_contact_us_path(format: "csv")
  end
 
  csv do
    column :name
    column :email
    column :phone
  end
end
