window.CONSENT=window.CONSENT || {};


(function(module) {

  module.validate_email_on_patient_create = function() {

    var generateNewEmail = function(full_name, emails) {
      var new_email, number;
      number = "";
      while (number.length < 3) {
        number += String(Math.floor(Math.random() * 9));
      }
      new_email = full_name + number + "@eic.com";
      if ($.inArray(new_email, emails) <= 0) {
        return new_email;
      } else {
        generateNewEmail(full_name, emails);
      }
    };

    $(document).on("change", "#email_generatore_chk", function(event) {
      main_ele = $(event.target).parents("table")
      first_name_ele = $(main_ele).find("#patient_first_name")
      last_name_ele = $(main_ele).find("#patient_last_name")
      if(this.checked){
        if($.trim($(first_name_ele).val()).length > 0 && $.trim($(last_name_ele).val()).length > 0){
          f_name = $(first_name_ele).val().replace(/^\.+|\.+$/g,"")
          l_name = $(last_name_ele).val().replace(/^\.+|\.+$/g,"")
          full_name = (f_name + "." +l_name).replace(/[^a-zA-Z0-9.]/g, "").toLowerCase()
          fake_email = full_name + "@eic.com"
          $.get("/patients/get_emails", (function(emails){
            if($.inArray(fake_email, emails) <= 0){
              $(main_ele).find("#fname").val(full_name + "@eic.com")
            }
            else{
              $(main_ele).find("#fname").val(generateNewEmail(full_name, emails))
            }
          }), "json")
          $(main_ele).find("#fname")[0].readOnly = true;
        }
        else if($.trim($(first_name_ele).val()).length == 0 && $.trim($(last_name_ele).val()).length == 0){
          alert("Please enter first name and last name.")
          this.checked = false
        }
        else if($.trim($(first_name_ele).val()).length == 0){
          alert("Please enter first name.")
          this.checked = false
        }
        else if($.trim($(last_name_ele).val()).length == 0){
          alert("Please enter last name.")
          this.checked = false
        }
      }
      else{
        $(main_ele).find("#fname")[0].readOnly = false;
        $(main_ele).find("#fname").val("")
      }
    });

  };

})(window.CONSENT);