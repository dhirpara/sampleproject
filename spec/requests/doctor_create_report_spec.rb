require "spec_helper"

  include Devise::TestHelpers

  describe "doctor already sign in" do

    before :each do
      @procedure = create(:procedure)
      @procedure_types = create(:procedure_type,procedure: @procedure)
    end

    it "allows doctor to sign in" do
      @doctor = FactoryGirl.create(:doctor)
      visit root_path
      within('.subscribe-form') do
        fill_in "Email",    :with => @doctor.email
        fill_in "Password", :with => @doctor.password
        click_button "Log in Now!"
      end
      page.should have_content("Signed in successfully.")
    end

    describe "create report" do
      let(:patient) { FactoryGirl.create(:patient) }
      before { visit consent_report_path(patient) }

      it "allows doctor to create report" do
        find(:xpath, "//input[@value='1']").set(true)
      end
    end
  end