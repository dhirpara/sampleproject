  #Doctor test-cases admin side

  Given(/^Admin already logged in$/) do
    @admin = FactoryGirl.create(:admin_user, :email => 'admin@example.com', :password => 'password')
    visit new_admin_admin_user_path
    fill_in 'Email', :with => @admin.email
    fill_in 'Password', :with => @admin.password
    click_button('Login')
  end

  When(/^I click on doctors link$/) do
    click_link "Doctors"
  end

  Then(/^I should see list of doctors$/) do
    visit admin_doctors_path
  end

  Then(/^I should see new doctor button$/) do
    page.should have_link('New Doctor', href:  new_admin_doctor_path)
  end

#Scenario 2

  Given(/^Admin visits new doctor page$/) do
    visit new_admin_doctor_path
  end

  When(/^I submit invalid doctor information$/) do
    click_button("Create Doctor")
  end

  Then(/^I should see error message$/) do
    page.should have_content "can't be blank"
  end

  Then(/^I visits new doctor page$/) do
    visit new_admin_doctor_path
  end

#Scenario 3
  Given(/^the folowing doctor exists$/) do |table|
    table.hashes.each do |attributes|
      Doctor.create :doctor_name => attributes[:doctor_name]
    end
  end

  Given(/^Speciality availables$/) do
    FactoryGirl.create(:speciality)
  end

  Given(/^I fill valid information$/) do
    @doctor = FactoryGirl.build(:doctor)
    fill_in "Email", :with => @doctor.email 
    fill_in "Doctor Name", :with => @doctor.doctor_name 
    fill_in "Password", :with => @doctor.password 
    fill_in "City", :with => @doctor.city 
    fill_in "Zip", :with => @doctor.zip 
    fill_in "Address", :with => @doctor.address 
    fill_in "State", :with => @doctor.state 
    fill_in "Code", :with => @doctor.code 
    fill_in "Phone", :with => @doctor.phone 
    fill_in "Practise name", :with => @doctor.practise_name 
  end

  When(/^I select "(.*?)" from "(.*?)"$/) do |option, field|
    page.select "Ortho", :from => "doctor[speciality_id]"
  end

  When(/^I press create doctor button$/) do
    click_button "Create Doctor"
  end

  Then(/^I should see successful message$/) do
    page.should have_content "Doctor was successfully created."
  end