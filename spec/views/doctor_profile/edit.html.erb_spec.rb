require 'spec_helper'

describe "doctor_profile/edit.html.erb" do
 
  before(:each) do
    @doctor_profile = assign(:doctor, stub_model(Doctor))
  end

  it "renders the form partial" do
    stub_template "doctor_profile/edit.html.erb" => "profile"
    render
    expect(rendered).to match /profile/
  end

end
