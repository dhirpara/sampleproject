class ReportMenusController < ApplicationController

  def new
    @report_menu = ReportMenu.new
    @procedures = Procedure.all
  end

  def show
    @report = Report.find(session[:report])
    session[:question_page]= DateTime.now
    @report.update_attributes(:personal_info_confirmation_time_spent => session[:personal_info])
    @report_menus = @report.report_menus.all
    @rep_men = ReportMenu.find(params[:id])
    @presentations = Presentation.all

    session[:report_menu] = @rep_men

    @report_questions = @rep_men.report_questions.all
    @procedure_type=ProcedureType.all
    @date = Time.now
    @rep_men.update_attributes(:video_section_time => @date)
    if @report.report_menus.last.id.to_i == params[:id].to_i
      @report_menu_s = ReportMenu.find(params[:id])
      @report_menu_seq = @report_menu_s.sequence
      @presentations = Presentation.find_by_js_key(@report_menu_seq)
    else
      @report_menu_s = ReportMenu.find(params[:id].to_i)
      @report_menu_seq = @report_menu_s.sequence
      @presentations = Presentation.find_by_js_key(@report_menu_seq)
      
      @next_video = @report_menu_s.id + 1
      @next_video_detail = ReportMenu.find(@next_video)
      @next_video_qry = @next_video_detail.query_str
    end
    @report_menu = @rep_men.report_id
  end


  def create
    #@patient = Patient.find(params[:id])
    @report = Report.new(:doctor_id => current_doctor.id)
    @procedures = Procedure.all
    if @report.save
    @report_menu = ReportMenu.new(params[:report_menu])
    @report_menu.report_id = @report.id
    if @report_menu.save
      redirect_to root_url
    end
   end
  end
end