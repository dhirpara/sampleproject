class RemoveProcedureIdFromPatient < ActiveRecord::Migration
  def up
    remove_column :patients, :procedure_id
  end

  def down
    add_column :patients, :procedure_id, :integer
  end
end
