class Audio < ActiveRecord::Base
  attr_accessible :audio_file
  mount_uploader :audio_file, AudioUploader
end
