class AddConnectionEstablishTimeAndLoginPageTimeSpentAndDesclaimerTimeSpentAndPersonalInfoConfirmationTimeSpentAndPresentationPageTimeSpentAndEsignaturePageTimeSpentToReport < ActiveRecord::Migration
  def change
    add_column :reports, :connection_establish_time, :datetime
    add_column :reports, :login_page_time_spent, :datetime
    add_column :reports, :esignature_page_time_spent, :datetime
    add_column :reports, :desclaimer_time_spent, :datetime
    add_column :reports, :personal_info_confirmation_time_spent, :datetime
  end
end
