class AddReportMenuIdToReportQuestion < ActiveRecord::Migration
  def change
    add_column :report_questions, :report_menu_id, :integer
  end
end
