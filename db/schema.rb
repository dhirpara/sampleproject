# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20160223070535) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "audios", :force => true do |t|
    t.string   "audio_file"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "contact_us", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "comment"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "doctor_transaction_histories", :force => true do |t|
    t.integer  "doctor_id"
    t.string   "subscription_plan"
    t.datetime "subscription_date"
    t.datetime "recuring_date"
    t.boolean  "is_expired"
    t.string   "account_number"
    t.string   "status"
    t.string   "transaction_id"
    t.string   "cancle_subscription_plan"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "subscription_id"
  end

  add_index "doctor_transaction_histories", ["doctor_id"], :name => "index_doctor_transaction_histories_on_doctor_id"

  create_table "doctors", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "doctor_name"
    t.string   "practise_name"
    t.string   "speciality"
    t.text     "address"
    t.string   "phone"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "code"
    t.string   "sex"
    t.integer  "speciality_id"
    t.boolean  "is_accept",              :default => false
    t.string   "subscription_plan"
    t.string   "subscription_date"
    t.string   "recuring_date"
    t.boolean  "trial_expired",          :default => false
    t.integer  "trial_period"
    t.datetime "expire_date"
  end

  add_index "doctors", ["email"], :name => "index_doctors_on_email", :unique => true
  add_index "doctors", ["reset_password_token"], :name => "index_doctors_on_reset_password_token", :unique => true

  create_table "doctors_testimonials", :force => true do |t|
    t.text     "description"
    t.integer  "doctor_id"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.boolean  "status",      :default => false
  end

  add_index "doctors_testimonials", ["doctor_id"], :name => "index_doctors_testimonials_on_doctor_id"

  create_table "patient_reports", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "patients", :force => true do |t|
    t.string   "email",                    :default => "", :null => false
    t.string   "encrypted_password",       :default => "", :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.text     "address"
    t.string   "gender"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "phone_code"
    t.string   "phone_number"
    t.date     "birthdate"
    t.date     "appointment_date"
    t.time     "appointment_time"
    t.string   "patientcode"
    t.integer  "doctor_id"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "token"
    t.string   "zip"
    t.string   "correct_email"
    t.string   "correct_first_name"
    t.string   "correct_last_name"
    t.text     "correct_address"
    t.string   "correct_city"
    t.string   "correct_state"
    t.string   "correct_country"
    t.string   "correct_phone_code"
    t.string   "correct_phone_number"
    t.integer  "correct_zip"
    t.date     "correct_birthdate"
    t.date     "correct_appointment_date"
    t.time     "correct_appointment_time"
    t.string   "correct_gender_number"
  end

  add_index "patients", ["doctor_id"], :name => "index_patients_on_doctor_id"
  add_index "patients", ["reset_password_token"], :name => "index_patients_on_reset_password_token", :unique => true

  create_table "patients_testimonials", :force => true do |t|
    t.text     "description"
    t.integer  "patient_id"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.boolean  "status",      :default => false
  end

  add_index "patients_testimonials", ["patient_id"], :name => "index_patients_testimonials_on_patient_id"

  create_table "payments", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "credit_card_number"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "expire_date"
    t.string   "status"
    t.integer  "card_code"
  end

  create_table "photos", :force => true do |t|
    t.string   "image_name"
    t.string   "image"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "presentations", :force => true do |t|
    t.string   "title"
    t.string   "video_url"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "js_key"
  end

  create_table "procedure_types", :force => true do |t|
    t.integer  "procedure_id"
    t.string   "type_name"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "parent_type_id"
    t.string   "video_file_link"
    t.string   "js_key"
  end

  create_table "procedures", :force => true do |t|
    t.string   "procedure_name"
    t.text     "procedure_description"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "speciality_id"
  end

  create_table "report_menus", :force => true do |t|
    t.integer  "report_id"
    t.integer  "procedure_id"
    t.integer  "procedure_type_ids"
    t.integer  "type_subtype_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "video_count"
    t.datetime "video_section_time"
    t.string   "sequence"
    t.string   "query_str"
  end

  create_table "report_questions", :force => true do |t|
    t.string   "question"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "report_menu_id"
  end

  create_table "reports", :force => true do |t|
    t.integer  "doctor_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.integer  "patient_id"
    t.string   "report_code"
    t.string   "token"
    t.string   "signature"
    t.string   "status"
    t.datetime "connection_establish_time"
    t.datetime "login_page_time_spent"
    t.datetime "esignature_page_time_spent"
    t.datetime "desclaimer_time_spent"
    t.datetime "personal_info_confirmation_time_spent"
    t.datetime "presentation_page_time_spent"
    t.datetime "econnection"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "specialities", :force => true do |t|
    t.string   "spe_name"
    t.text     "spe_description"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "subscribers", :force => true do |t|
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "videos", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.string   "yt_video_id"
    t.boolean  "is_complete"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "procedure_id"
    t.integer  "speciality_id"
    t.boolean  "video_status"
  end

end
