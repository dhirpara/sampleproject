require 'spec_helper'

describe "contact_us/show" do
  before(:each) do
    @contact_u = assign(:contact_u, stub_model(ContactU,
      :name => "Name",
      :email => "Email",
      :phone => "Phone",
      :comment => "Comment"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Email/)
    rendered.should match(/Phone/)
    rendered.should match(/Comment/)
  end
end
