require "faker"

FactoryGirl.define do
  factory :doctor do
    email { Faker::Internet.email }
    password "foobar123"
    password_confirmation "foobar123"
    practise_name "xyz"
    address { Faker::Address.street_address }
    doctor_name "savan"
    code "022"
    phone { Faker::PhoneNumber.phone_number }
    association :speciality
    city { Faker::Address.city }
    state {  Faker::Address.state }
    sex "Female"
    zip { Faker::Address.zip }
    is_accept "true"
  end

  factory :invalid_doctor, parent: :doctor do
    email nil
  end
end
