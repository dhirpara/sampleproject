class PatientPdf < Prawn::Document

  def initialize(patient)
    super()
    @patient = patient
    text "Patient Created #{@patient.id}"
    text "#{patient.first_name}"
  end
end