class AddVideoSectionTimeToReportMenu < ActiveRecord::Migration
  def change
    add_column :report_menus, :video_section_time, :datetime
  end
end
