Feature: patient

  In order to add the patient
  As a doctor
  I want sign in

  Scenario: doctor wants to add patient
    Given doctor already signin
    When he click on add patient link
    Then he should see form of patient

  Scenario: doctor create patient with invalid data
    Given doctor already signin
    And doctor visits add patient page
    When he submit patient form with invalid data
    Then he should see error message for patient validation
    And he visits add patient page again

  Scenario: Doctor create patient with valid data
    Given doctor already signin
    When He fill valid information
    And He press create button
    Then he should see show patient page