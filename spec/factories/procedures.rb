# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :procedure do
    procedure_name "Ortho"
    procedure_description {Faker::Lorem.sentences}
    association :speciality
  end
end
