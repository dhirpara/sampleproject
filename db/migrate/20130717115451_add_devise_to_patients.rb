class AddDeviseToPatients < ActiveRecord::Migration
  def change
    create_table(:patients) do |t|
      ## Database authenticatable
      t.string :email,              :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""
      t.string :first_name
      t.string :last_name
      t.text   :address
      t.string :gender
      t.string :city
      t.string :state
      t.string :country
      t.integer :phone_code
      t.integer :phone_number
      t.date    :birthdate
      t.integer :procedure_id
      t.date    :appointment_date
      t.time    :appointment_time
      t.string   :patientcode
      t.integer  :doctor_id

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Confirmable
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, :default => 0 # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at

      ## Token authenticatable
      # t.string :authentication_token


      # Uncomment below if timestamps were not included in your original model.
      t.timestamps
    end


    add_index :patients, :reset_password_token, :unique => true
    add_index :patients, :procedure_id
    add_index :patients, :doctor_id
    # add_index :patients, :confirmation_token,   :unique => true
    # add_index :patients, :unlock_token,         :unique => true
    # add_index :patients, :authentication_token, :unique => true
  end
end
