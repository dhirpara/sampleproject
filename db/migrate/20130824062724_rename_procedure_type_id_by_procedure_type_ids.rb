class RenameProcedureTypeIdByProcedureTypeIds < ActiveRecord::Migration
  def up
    rename_column :report_menus, :procedure_type_id, :procedure_type_ids
  end

  def down
  end
end
