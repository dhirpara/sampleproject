class AddReportIdToReportQuestion < ActiveRecord::Migration
  def change
    add_column :report_questions, :report_id, :integer
  end
end
