class AddJsKeyToPresentations < ActiveRecord::Migration
  def change
    add_column :presentations, :js_key, :string
  end
end
