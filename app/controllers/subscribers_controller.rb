class SubscribersController < ApplicationController
  # layout "home", only: [:all_newsletter]

  def new
  	# @subscriber = Subscriber.new
  end

  def create
  	@subscriber = Subscriber.new(params[:subscriber])
  		if @subscriber.save
        respond_to do |format|
          format.js
        end
  		end
      cookies[:newsletter] = { value: "newsletter_subscribe", expires: 24.hour.from_now}
  end

end
