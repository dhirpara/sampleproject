
Given(/^admin already signin$/) do
  @admin = FactoryGirl.create(:admin_user, :email => 'admin@example.com', :password => 'password')
  visit new_admin_admin_user_path
  fill_in 'Email', :with => @admin.email
  fill_in 'Password', :with => @admin.password
  click_button('Login')
end

When(/^he click on patients link$/) do
  click_link "Patients"
end

Then(/^he should see list of patients$/) do
  visit admin_patients_path
end

Then(/^he should see new patient button$/) do
  page.should have_link('New Patient', href:  new_admin_patient_path)
end

# ------------------admin create patient with invalid information-------------------------------

Given(/^admin visits new patient page$/) do
  visit new_admin_patient_path
end

When(/^he submits invalid patient information$/) do
  click_button('Create Patient')
end

Then(/^he should see error message$/) do
 page.should have_content "can't be blank"
end

Then(/^he visits new patient page$/) do
  visit new_admin_patient_path
end

# ------------------admin create patient with valid information-------------------------------

Given(/^the folowing patient exists$/) do |table|
  table.hashes.each do |attributes|
      Patient.create :first_name => attributes[:first_name]
    end
end

Given(/^doctor available$/) do
  FactoryGirl.create(:doctor)

end

Given(/^procedure available$/) do
  FactoryGirl.create(:procedure)  
end

Given(/^Admin visits new patient page$/) do
  visit new_admin_patient_path
end

Given(/^he fill valid information$/) do
  @patient = FactoryGirl.build(:patient)
  fill_in('First name', :with => "jigs")
  fill_in('Last name',:with => @patient.last_name)
  fill_in('Address', :with => @patient.address)
  fill_in('Email',:with => @patient.email)
  fill_in "City", :with => @patient.city 
  fill_in "State", :with => @patient.state
  fill_in "Phone code", :with => @patient.phone_code
  fill_in "Phone number",:with => @patient.phone_number
  fill_in('Zip', :with => '2020')
  fill_in "Birthdate" , :with => @patient.birthdate
  select('India', :from => "patient[country]")
  # fill_in('Appointment time',:with => '05:00')
  choose('Female')
end

When(/^he select "(.*?)" from "(.*?)"$/) do |arg1, arg2|
  page.select "savan", :from => "patient[doctor_id]"
  #page.select "Ortho", :from => "patient[procedure_id]"
 end

When(/^he press create patient button$/) do
  click_button('Create Patient')
end

Then(/^he should see successful message$/) do
  page.should have_content "Patient was successfully created."
end