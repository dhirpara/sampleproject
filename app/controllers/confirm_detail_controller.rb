class ConfirmDetailController < ApplicationController
  before_filter :get_report

  def get_report
    @report = Report.find_by_id(session[:report])
    @report_menu = @report.report_menus.all
  end

  def edit
    @patient = Patient.find(params[:id])

    respond_to do |format|
      format.html
      format.js
    end
  end

  def update
    if params[:patient][:birthdate].present?
      date_params = params[:patient][:birthdate].split('/')
      params[:patient][:birthdate] = date_params[1]+'-'+date_params[0]+'-'+date_params[2]
    end
    @patient = Patient.find(params[:id])
    respond_to do |format|
      if @patient.update_attributes(params[:patient].except(:email))
        format.html { redirect_to show_path(@patient), notice: 'Patient was successfully updated.' }
        format.js
      else
        format.html { render "edit" }
        format.js
      end
    end
  end

  def show
    @patient = Patient.find(params[:id])
      respond_to do |format|
        format.html
        format.js
      end
  end
end
