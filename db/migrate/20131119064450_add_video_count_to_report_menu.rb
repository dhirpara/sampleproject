class AddVideoCountToReportMenu < ActiveRecord::Migration
  def change
    add_column :report_menus, :video_count, :integer
  end
end
