class RemoveReportIdFromReportQuestion < ActiveRecord::Migration
  def up
    remove_column :report_questions, :report_id
  end

  def down
    add_column :report_questions, :report_id, :integer
  end
end
