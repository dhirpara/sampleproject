Feature: Manage Patients

    In order to create the patient in our web application
    As a admin
    I want to add patient

  Background:
    Given admin already signin
  
  Scenario:admin redirect to new patient page
    When he click on patients link
    Then he should see list of patients
    And he should see new patient button

  Scenario:admin create patient with invalid information
    And admin visits new patient page
    When he submits invalid patient information
    Then he should see error message
    And he visits new patient page

  Scenario:admin create patient with valid information
    Given the folowing patient exists  
      |Doctor Name  | Email               |password |procedure    |code     |zip      |city       |First name| Last name| 
      |savan        | svnrvl001@gmail.com |password | Ortho       |079      |382443   |Ahmedabad  |jigar     |bhatt| 
      |Jigar        | svnrvl001@gmail.com |password | Dentist     |022      |382456   |baroda     |dhaval    |bhatt|
    And doctor available
    And procedure available
    And Admin visits new patient page
    And he fill valid information
    When he select "savan" from "patient_doctor_id" 
    And he select "Ortho" from "patient_procedure_id"
    And he press create patient button
    Then he should see successful message