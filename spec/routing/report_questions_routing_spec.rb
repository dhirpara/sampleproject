require "spec_helper"

describe ReportQuestionsController do
  describe "routing" do

    it "routes to #index" do
      get("/report_questions").should route_to("report_questions#index")
    end

    it "routes to #new" do
      get("/report_questions/new").should route_to("report_questions#new")
    end

    it "routes to #create" do
      post("/report_questions").should route_to("report_questions#create")
    end

    it "routes to #destroy" do
      delete("/report_questions/1").should route_to("report_questions#destroy", :id => "1")
    end

  end
end
