When(/^I click on procedure menu$/) do
  page.should have_content('Procedure')
end

Then(/^I should see procedures link$/) do
  page.should have_content('Procedures')
end

Given(/^Admin clicks on procedure menu$/) do
  page.should have_content('Procedure')
end

When(/^I click on procedures link$/) do
  click_link 'Procedures'
end

Then(/^I should see list of procedures$/) do
  visit admin_procedures_path
end

Then(/^I should see New Procedure$/) do
  page.should have_content('New Procedure')
end

Given(/^Admin visits new procedure page$/) do
  visit new_admin_procedure_path
end

When(/^He submit invalid procedure information$/) do
  click_button("Create Procedure")
end

Then(/^He visits new procedure page$/) do
  visit new_admin_procedure_path
end

Given(/^the folowing procedure exists$/) do |table|
  table.hashes.each do |attributes|
    Procedure.create :procedure_name => attributes[:procedure_name]
  end
end

Given(/^I fill valid procedure information$/) do
    @procedure = FactoryGirl.build(:procedure)
    fill_in "Procedure Name", :with => @procedure.procedure_name 
    #fill_in "Procedure Description", :with => @procedure.procedure_description 
end

When(/^I select "(.*?)" from a "(.*?)"$/) do |arg1, arg2|
 page.select "Ortho", :from => "procedure[speciality_id]"
end



When(/^I press create procedure button$/) do
  click_button "Create Procedure"
end

Then(/^I should see successful message for procedure$/) do
  page.should have_content "Procedure was successfully created."
end

