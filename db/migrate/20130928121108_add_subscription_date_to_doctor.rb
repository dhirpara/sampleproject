class AddSubscriptionDateToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :subscription_date, :string
  end
end
