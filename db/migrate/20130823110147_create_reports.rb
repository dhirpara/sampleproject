class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :doctor_id

      t.timestamps
    end
  end
end
