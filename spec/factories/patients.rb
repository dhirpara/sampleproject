# Read about factories at https://github.com/thoughtbot/factory_girl
require "faker"

FactoryGirl.define do
  factory :patient do
    email {Faker::Internet.email}
    first_name {Faker::Name.first_name}
    address {Faker::Address.street_address}
    last_name {Faker::Name.last_name}
    phone_code "022"
    phone_number { Faker::PhoneNumber.phone_number }
    #association :procedure
    association :doctor
    city { Faker::Address.city }
    state {  Faker::Address.state }
    appointment_date "30-8-2013"
    birthdate "24-2-1989"
    appointment_time "07:15"
    gender "female"
    zip "3800028"
    country { Faker::Address.country }
  end

  factory :invalid_patient, parent: :patient do
    email nil
  end
end
