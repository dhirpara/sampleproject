class CreateReportQuestions < ActiveRecord::Migration
  def change
    create_table :report_questions do |t|
      t.string :question

      t.timestamps
    end
  end
end
