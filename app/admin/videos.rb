ActiveAdmin.register Video do
  # menu :parent => "Uploads"
  menu false

	member_action :new do
    @video = Video.new
			respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @videos }
      end
  end

  member_action :upload do
    @video = Video.new(params[:video])
    if @video.save
      @upload_info = Video.token_form(params[:video], save_video_new_admin_video_url(:video_id => @video.id))
    else
      respond_to do |format|
        format.html {render action: "new" , :layout => false}
        format.json { render json: @video.errors }
      end
    end
  end

  member_action :save_video do
    @video = Video.find(params[:video_id])
    if params[:status].to_i == 200
      @video.update_attributes(:yt_video_id => params[:id].to_s, :is_complete => true)
      Video.delete_incomplete_videos
    else
      Video.delete_video(@video)
    end
    redirect_to admin_videos_path, :notice => "video successfully uploaded"
    end

  member_action :edit do
    @video = Video.find(params[:id])
  end

  member_action :update do
    @video     = Video.find(params[:id])
    @result    = Video.update_video(@video, params[:video])
    respond_to do |format|
      format.html do
        if @result
          redirect_to admin_video_path, :notice => "video successfully updated"
        else
          respond_to do |format|
            format.html { render "/admin/videos/edit" }
          end
        end
      end
    end
  end

  controller do
    def change_procedure
      @procedures = Speciality.find_by_id(params[:speciality_id]).try(:procedures)
      if @procedures.nil?
        render :text=>"<option>Select Procedure</option>"
      else
        render :text=>view_context.options_from_collection_for_select(@procedures, :id, :procedure_name,{:include_blank=>true})
      end
    end
  end

  form :partial => "form", :video => @video

  show do
    render "show", :video => @video
    # active_admin_comments
  end

  index do
    selectable_column
    column :id
    column :title
    column "Speciality" do |spec|
      spec.speciality_spe_name
    end
    column "Procedure" do |proc|
      proc.procedure_procedure_name
    end
    column :is_complete
    default_actions
  end
end