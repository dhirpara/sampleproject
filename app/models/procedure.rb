class Procedure < ActiveRecord::Base
  attr_accessible :procedure_name, :procedure_description, :speciality_id
  validates :procedure_name, :speciality_id, :presence => true
  belongs_to :speciality
  #has_many :patients
  has_many :procedure_types
  has_many :report_menus
  has_many :videos, dependent: :destroy
  delegate :spe_name, :to => :speciality, :prefix => true

end
