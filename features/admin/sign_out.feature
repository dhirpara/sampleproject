Feature: Sign out

  To sign out from web application
  As a Admin
  Should be able to sign out

    Scenario: Admin sign out
      Given Admin logged in
      When Admin click logout link
      Then Admin should be logout
      And Admin should see message