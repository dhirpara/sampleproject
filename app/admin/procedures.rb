ActiveAdmin.register Procedure do
  menu :parent => "Procedure", :priority => 1

  member_action :procedure_show_type, :title => "Procedure Types" do
    @procedure = Procedure.find(params[:procedure_id])
    @procedure_type = @procedure.procedure_types
  end

  form do |f|
    f.inputs "Details" do
      f.input :speciality, :label => "Speciality", :collection => Speciality.all.map{ |s| [s.spe_name, s.id] }, :include_blank => "Please select..."
      f.input :procedure_name, :label => "Procedure Name"
      f.input :procedure_description, :label => "Procedure Description", :input_html => { :rows => 5, :cols => 30, :maxlength => 10  }
    end
    f.buttons
  end

  index do
    selectable_column
    column :id
    column "Procedure Name" do |procedure|
      link_to procedure.procedure_name.capitalize, admin_procedure_path(procedure)
    end

    column :created_at
    column :updated_at
    default_actions

    column do |procedure_type|
      link_to "View procedure types", procedure_show_type_admin_procedure_path(procedure_type)
    end

  end

  show do
    attributes_table do
      row :id
      row :procedure_name
      row :procedure_description
      row("Speciality") { procedure.speciality_spe_name }
    end
  end

  # filter :speciality, :label => "Speciality", :collection => Speciality.all.map{ |s| [s.spe_name, s.id] }
  filter :procedure_name
  filter :created_at
  filter :updated_at
end