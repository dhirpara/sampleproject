class AddColumnsToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :is_expired, :boolean
    add_column :doctors, :trial_period, :integer
  end
end
