require 'spec_helper'

describe "devise/registrations/new.html.erb" do
  before(:each) do
    @doctor = assign(:doctor, stub_model(Doctor)).as_new_record.as_null_object
  end

  it "renders the form partial" do
    visit "/doctors/sign_up"
    
 		page.should have_selector('form#new_doctor') do |form|
		 	form.should have_selector('label',:text=>'Speciality')
		 	select ("Ortho")

		 	form.should have_selector('label',:text=>'Doctor name')
     	form.should have_selector('input',:type => "text",:name=>'doctor[doctor_name]',:id=>'doctor_doctor_name')

		 	form.should have_selector('label',:text=>'Email')
     	form.should have_selector('input',:type => "text",:name=>'doctor[email]',:id=>'doctor_email')

		 	form.should have_selector('label',:text=>'Password')
     	form.should have_selector('input',:type => "password",:name=>'doctor[password]',:id=>'doctor_password')

		 	form.should have_selector('label',:text=>'Password confirmation')
			form.should have_selector('input',:type => "password",:name=>'doctor[password_confirmation]',:id=>'doctor_password_confirmation')			 
		 
		 	form.should have_selector('label',:text=>'Practise name')
		 	form.should have_selector('input',:type => "text",:name=>'doctor[practise_name]',:id=>'doctor_practise_name')

		 	form.should have_selector('label',:text=>'Address')
		 	form.should have_selector('input',:type => "text",:name=>'doctor[address]',:id=>'doctor_address')
		 
		 	form.should have_selector('label',:text=>'City')
		 	form.should have_selector('input',:type => "text",:name=>'doctor[city]',:id=>'doctor_city')
		 
		 	form.should have_selector('label',:text=>'State')
		 	form.should have_selector('input',:type => "text",:name=>'doctor[state]',:id=>'doctor_state')
		 
		 	form.should have_selector('label',:text=>'Zip')
		 	form.should have_selector('input',:type => "text",:name=>'doctor[zip]',:id=>'doctor_zip')
		 
		 	form.should have_selector('label',:text=>'Phone')
		 	form.should have_selector('input',:type => "text",:name=>'doctor[code]',:id=>'doctor_code')
		 
		 	form.should have_selector('input',:type => "text",:name=>'doctor[phone]',:id=>'doctor_phone')
		 	form.should have_selector('label',:text=>'Gender')
		 	choose ("Male")

		 click_button('Next')

		end
	end
end