require 'spec_helper'

  describe Patient do

    let(:doctor) { FactoryGirl.create(:doctor) }

    before do
      @patient = doctor.patients.build(first_name: "swati", last_name: "modi", email: "s@y.com",address: "MyText",gender: "Female", city: "a'bad",state: "guj", country: "india",phone_code: 022,phone_number: 123456,birthdate: "2013-07-12",zip: "3800028",appointment_date: "2013-07-12",appointment_time: "2013-07-12 11:01:49")
    end

    subject { @patient }

    it { should respond_to(:first_name) }
    it { should respond_to(:doctor_id) }
    it { should respond_to(:last_name) }
    it { should respond_to(:email) }
    it { should respond_to(:address) }
    it { should respond_to(:city) }
    it { should respond_to(:state) }
    it { should respond_to(:country) }
    it { should respond_to(:gender) }
    it { should respond_to(:phone_code) }
    it { should respond_to(:phone_number) }
    it { should respond_to(:birthdate) }
    it { should respond_to(:phone_number) }
    it { should respond_to(:doctor) }
    it { should respond_to(:zip) }
    its(:doctor) { should == doctor }
    it { should be_valid }

    describe "when first name is not present" do
      before { @patient.first_name = " " }
      it { should_not be_valid }
    end

     describe "when last name is not present" do
      before { @patient.last_name = " " }
      it { should_not be_valid }
    end

    describe "when address is not present" do
      before { @patient.address = " " }
      it { should_not be_valid }
    end

    # describe "when gender is not present" do
    #   before { @patient.gender = " " }
    #   it { should_not be_valid }
    # end


    describe "when doctor_id is not present" do
      before { @patient.doctor_id = " " }
      it { should_not be_valid }
    end

    describe "when email is not present" do
      before { @patient.email = " " }
      it { should_not be_valid }
    end



    describe "when email format is invalid" do
      it "should be invalid" do
        addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                       foo@bar_baz.com foo@bar+baz.com]
        addresses.each do |invalid_address|
          @patient.email = invalid_address
          @patient.should_not be_valid
        end
      end
    end

    describe "when email format is valid" do
      it "should be valid" do
        addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
        addresses.each do |valid_address|
          @patient.email = valid_address
          @patient.should be_valid
        end
      end
    end

    describe "when email address is already taken" do
      before do
        patient_with_same_email = @patient.dup
        patient_with_same_email.save
      end
    it { should_not be_valid }
    end
end