class AddCorrectGenderToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_gender_number, :string
  end
end
