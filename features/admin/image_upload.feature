Feature: Image upload

  In order to upload an image
  As a admin
  I want to upload imgaes

  Background:
    Given I am sign in as admin

  Scenario: Admin visit to photos link
    When I click on photos link
    Then I should see list of images

  Scenario: New photo upload form
    And I click on photos link
    When I click on new photo button
    Then I should see new photo form

  Scenario: Photo upload with valid information
    And I am on new photo upload form
    When I fill image valid information
    Then I should be redirected to image show page with success message