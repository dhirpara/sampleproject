Feature: Patient Login

  After getting mail patient can access patient side website with valid usercode

  Scenario: Patient Login with valid usercode
    Given patient should see login page
    When he fill valid usercode
    Then he should see  Website Disclaimer page

  Scenario: Patient Login with Invalid usercode
    Given patient should see login page
    When he fill invalid usercode
    Then he should see invalid Usercode message
    And he should see login page

  Scenario: Patient data confirm and continue
    Given patient should see Website Disclaimer page
    When he click on I Agree button
    Then he should see patient detail
    And he can click on confirm and continue button
    And he should see report question page
    When he click on I Do Not Agree button
    Then he should see login page

  Scenario: Patient question and his signature
    Given patient should see report question page
    When he click on i understand link
    Then he should see E-signature form

