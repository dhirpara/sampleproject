class RemoveColumnValidCardFromPayment < ActiveRecord::Migration
  def up
    remove_column :payments, :valid_card
  end

  def down
    add_column :payments, :valid_card, :boolean
  end
end
