class UserStepsController < ApplicationController
  include Wicked::Wizard
  steps :speciality, :personal

  def show
    @doctor = current_doctor
    render_wizard
  end
end