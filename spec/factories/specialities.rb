# Read about factories at https://github.com/thoughtbot/factory_girl
require "faker"

FactoryGirl.define do
  factory :speciality do
    spe_name "Ortho"
    # sequence(:spe_name) { |n| "spe_name #{n}" }
    spe_description Faker::Lorem.sentences
  end
end
