require 'spec_helper'

  describe "Devise#SessionsController" do

      before do
        request.env["devise.mapping"] = Devise.mappings[:doctor] 
        @doctor = FactoryGirl.create(:doctor)
      end

      it "should allow the doctor to sign in" do
        sign_in @doctor
      end

      it "should allow the doctor to sign out" do
        sign_out @doctor
      end
  end    
