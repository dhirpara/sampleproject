require 'spec_helper'

describe "contact_us/edit" do
  before(:each) do
    @contact_u = assign(:contact_u, stub_model(ContactU,
      :name => "MyString",
      :email => "MyString",
      :phone => "MyString",
      :comment => "MyString"
    ))
  end

  it "renders the edit contact_u form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", contact_u_path(@contact_u), "post" do
      assert_select "input#contact_u_name[name=?]", "contact_u[name]"
      assert_select "input#contact_u_email[name=?]", "contact_u[email]"
      assert_select "input#contact_u_phone[name=?]", "contact_u[phone]"
      assert_select "input#contact_u_comment[name=?]", "contact_u[comment]"
    end
  end
end
