require 'spec_helper'

  describe PatientMailer do
    describe 'registration_confirmation' do
      before :each do
        @doctor = create(:doctor)
        @speciality = Speciality.create(:spe_name => "mcb")
        @report = create(:report)
      end
      let(:mail) { PatientMailer.registration_confirmation(@report,@doctor,@speciality)}

      it 'renders the subject' do
        mail.subject.should == 'Patient Report Access'
      end
      it 'renders the receiver email' do
        mail.to.should == [@report.patient.email]
      end
      it 'renders the sender email' do
        mail.from.should == ['from@example.com']
      end
      it 'assigns report access url' do
        mail.body.encoded.should match("Location")
      end
      it 'assigns patient report code' do
        mail.body.encoded.should match("#{@report.report_code}")
      end
    end
  end