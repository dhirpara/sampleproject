require 'spec_helper'

describe PatientsController do
  describe 'GET #index' do
    let!(:doctor) { create(:doctor) }
    let!(:patient) { create(:patient, doctor: doctor)}
    before { sign_in(doctor) }
    before { get :index }

    it "populates an array of patients" do
      expect(assigns(:patients)).to match_array [patient]
    end

    it "renders the :index view" do
      response.should render_template :index
      expect(response).to render_template :index
    end
  end

  describe 'GET #show' do
    let!(:patient) { create(:patient)}

    it "assigns the requested patient to @patient" do
      get :show, id: patient
      expect(assigns(:patient)).to eq patient
    end

    it "renders the :show template" do
      get :show, id: patient
      expect(response).to render_template :show
    end
  end

  describe 'GET #new' do

    before(:each) do
      @doctor = create(:doctor)
      sign_in @doctor
      @patient= build(:patient, doctor: @doctor)
    end

    it "assigns a new Patient to @patient" do
      get :new
      expect(assigns(:patient)).to be_a_new(Patient)
    end

    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    let!(:doctor) { create(:doctor) }

    before(:each) do
      sign_in doctor
    end

    context "with valid attributes" do
      it "saves the new patient in the database" do
        expect{
          post :create, patient: attributes_for(:patient)
        }.to change(Patient, :count).by(1)
      end

      it "redirects to the patient show page" do
        post :create, patient: attributes_for(:patient)
        expect(response).to redirect_to patient_path(1)
      end
    end

    context "with invalid attributes" do
      let!(:invalid_patient) {FactoryGirl.build(:invalid_patient)}

      it "does not save the new patient in the database" do
        expect{
        post :create,
        patient: attributes_for(:invalid_patient)
        }.to_not change(Patient, :count)
      end

      it "re-renders the :new template" do
        post :create,
        patient: attributes_for(:invalid_patient)
        expect(response).to render_template :new
      end
    end
  end

  describe 'GET #edit' do
    before(:each) do
      @doctor = create(:doctor)
      sign_in @doctor
    end

    let!(:patient) { create(:patient)}

    it "assigns the requested patient to @patient" do
      get :edit, id: patient
      expect(assigns(:patient)).to eq patient
    end

    it "renders the :edit template" do
      get :edit, id: patient
      expect(response).to render_template :edit
    end
  end

  describe 'PUT #update' do
    before :each do
      @patient = create(:patient,first_name: "swati",email: "hello@y.com")
    end

    it "locates the requested @patient" do
      put :update, id: @patient, patient: attributes_for(:patient)
      expect(assigns(:patient)).to eq(@patient)
    end

    context "valid attributes" do
      it "changes @patient's attributes" do
        put :update, id: @patient,
        patient: attributes_for(:patient,
        first_name: "Arpita")
        @patient.reload
        expect(@patient.first_name).to eq("Arpita")
      end

      it "redirects to the updated patient" do
        put :update, id: @patient, patient: attributes_for(:patient)
        response.should redirect_to patients_path
        expect(response).to redirect_to patients_path
      end
    end

    context "invalid attributes" do
      it "does not change @patient's attributes" do
        put :update, id: @patient,
        patient: attributes_for(:patient,
        first_name: "swati",
        email: nil)
        @patient.reload
        expect(@patient.first_name).to eq("swati")
      end

      it "re-renders the edit method" do
        put :update, id: @patient, patient: attributes_for(:invalid_patient)
        expect(response).to render_template :edit
      end
    end
  end

  describe 'DELETE destroy' do
    before :each do
      @patient = create(:patient)
    end

    it "deletes the patient" do
      expect{
      delete :destroy, id: @patient
      }.to change(Patient,:count).by(-1)
    end

    it "redirects to patients#index" do
      delete :destroy, id: @patient
      expect(response).to redirect_to patients_url
    end
  end

end
