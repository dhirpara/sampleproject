class AddCorrectLastNameToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_last_name, :string
  end
end
