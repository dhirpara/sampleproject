require 'spec_helper'

describe "patients/index" do
  it "displays all the patients" do
    @doctor = FactoryGirl.create(:doctor)
    visit root_path
    within('.subscribe-form') do
      fill_in "Email",    :with => @doctor.email
      fill_in "Password", :with => @doctor.password
      click_button "Log in Now!"
    end
  	assign(:patients, [
    	stub_model(Patient, :first_name => "slicer"),
    	stub_model(Patient, :last_name => "dicer")
  	])

		render
    expect(rendered).to match /slicer/
    expect(rendered).to match /dicer/
  end
end