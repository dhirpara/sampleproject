class RemoveStatusFromDoctorsTestimonials < ActiveRecord::Migration
  def up
    remove_column :doctors_testimonials, :status
  end

  def down
    add_column :doctors_testimonials, :status, :string
  end
end
