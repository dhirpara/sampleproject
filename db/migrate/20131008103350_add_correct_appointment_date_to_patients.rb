class AddCorrectAppointmentDateToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_appointment_date, :date
  end
end
