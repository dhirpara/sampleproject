require 'spec_helper'

describe "patients_testimonials/index" do
  before(:each) do
    assign(:patients_testimonials, [
      stub_model(PatientsTestimonial,
        :description => "MyText",
        :status => "Status",
        :patient => nil
      ),
      stub_model(PatientsTestimonial,
        :description => "MyText",
        :status => "Status",
        :patient => nil
      )
    ])
  end

  it "renders a list of patients_testimonials" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
