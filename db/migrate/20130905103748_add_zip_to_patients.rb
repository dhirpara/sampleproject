class AddZipToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :zip, :integer
  end
end
