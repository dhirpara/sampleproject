Given(/^I already logged in$/) do
  visit new_admin_user_session_path
  fill_in 'Email', :with => "test@admin.com"
  fill_in 'Password', :with => "@admin.password"
  click_button('Login')
end

#Speciality test-cases admin side

  Given(/^I am on dashboard$/) do
    visit admin_dashboard_path
  end

  When(/^I click on specialities link$/) do
    click_link 'Specialities'
  end

  Then(/^I should see list of specialities$/) do
    visit admin_specialities_path
  end

  Then(/^I should see "(.*?)"$/) do |name|
    find_link('New Speciality').should_not be_nil
  end

#Scenario 2

  Given(/^Admin visits new speciality page$/) do
    visit new_admin_speciality_path
  end

  When(/^He submit invalid speciality information$/) do
    click_button("Create Speciality")
  end

  Then(/^He should see error message$/) do
    page.should have_content "can't be blank"
  end

  Then(/^He visits new speciality page$/) do
    visit new_admin_speciality_path
  end

#Scenario 3

  When(/^He submit valid speciality information$/) do
    fill_in 'Speciality Name', :with => "TestCase"
    fill_in 'Speciality Discription', :with => "TestDEscription"
    click_button('Create Speciality')
  end

  Then(/^He should see successful message$/) do
    page.should have_content "Speciality was successfully created."
  end