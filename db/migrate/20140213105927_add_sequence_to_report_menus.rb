class AddSequenceToReportMenus < ActiveRecord::Migration
  def change
    add_column :report_menus, :sequence, :string
  end
end
