class ApplicationController < ActionController::Base
  protect_from_forgery

  require 'active_admin_views_pages_base.rb'

  helper_method :yt_client
  before_filter :check_plan_expiration, except: [:select_plan]

  def check_plan_expiration
    if params[:controller]!="sessions" && params[:controller]!="registrations"
      if doctor_signed_in? && !["Unlimited", nil].include?(current_doctor.subscription_plan)
        # if current_doctor.recuring_date.present?
          if current_doctor.subscription_plan == "Expired"
            @doctor = current_doctor
            # sign_out
            session[:subcribe_doc] = @doctor
            flash[:danger] = "Your subscription plan has been expired on #{@doctor.expire_date.to_date}. Please select a plan to continue."
            redirect_to select_plan_path
          elsif Time.zone.now > current_doctor.expire_date.to_datetime
            current_doctor.expire_plan
            @doctor = current_doctor
            # sign_out
            session[:subcribe_doc] = @doctor
            flash[:danger] = "Your subscription plan has been expired on #{@doctor.expire_date.to_date}. Please select a plan to continue."
            redirect_to select_plan_path
          end
        # end
      elsif doctor_signed_in? && current_doctor.subscription_plan.nil?
        @doctor = current_doctor
        # sign_out
        session[:subcribe_doc] = @doctor
        if session[:subcribe_doc].present? && cookies[:free_plan].present? && @doctor.trial_expired == false
          session[:subcribe_doc].update_attributes(:subscription_plan => "Free", :subscription_date => Time.now, :expire_date => Time.now + 30.days, :trial_expired => false, :trial_period => "30")
          DoctorMailer.delay(queue: "doctor-#{session[:subcribe_doc].id}", run_at: session[:subcribe_doc].expire_date.to_datetime - 7.days).trial_expired_notification(current_doctor)
          cookies.delete :free_plan
          sign_in(:doctor, session[:subcribe_doc])
          flash[:success]="Your free plan is activated now."
          redirect_to root_path
        else
          flash[:info] = "Please select your plan to continue."
          redirect_to select_plan_path
        end
      end
    end
  end

  def yt_client
    @yt_client ||= YouTubeIt::Client.new(:username => YouTubeITConfig.username , :password => YouTubeITConfig.password , :dev_key => YouTubeITConfig.dev_key)
  end


  def page_not_found
    respond_to do |format|
      format.html { render template: 'errors/not_found_error', layout: 'layouts/application', status: 404 }
      format.all  { render nothing: true, status: 404 }
    end
  end

  def server_error
    respond_to do |format|
      format.html { render template: 'errors/internal_server_error', layout: 'layouts/error', status: 500}
      format.all  { render nothing: true, status: 500}
    end
  end
  @HOSTNAME = "www.einformedconsent.com"
end