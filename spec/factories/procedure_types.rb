# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :procedure_type do
    type_name "General"
    association :procedure
  end
end
