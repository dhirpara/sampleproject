class AddCorrectPhoneNumberToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_phone_number, :string
  end
end
