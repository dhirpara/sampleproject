window.CONSENT=window.CONSENT || {};

$(document).ready(function(){
  autoPlayYouTubeModal();

  $("#how-it-works").click(function(){
    setTimeout(function(){
      $('#myvid').get(0).play();
    },2000);
    });

  //| this script is for placeholder compability in IE
  var input = document.createElement("input");
  if(('placeholder' in input)==false) {
      $('[placeholder]').focus(function() {
          var i = $(this);
          if(i.val() == i.attr('placeholder')) {
              i.val('').removeClass('placeholder');
              if(i.hasClass('password')) {
                  i.removeClass('password');
                  this.type='password';
              }
          }
      }).blur(function() {
          var i = $(this);
          if(i.val() == '' || i.val() == i.attr('placeholder')) {
              if(this.type=='password') {
                  i.addClass('password');
                  this.type='text';
              }
              i.addClass('placeholder').val(i.attr('placeholder'));
          }
      }).blur().parents('form').submit(function() {
          $(this).find('[placeholder]').each(function() {
              var i = $(this);
              if(i.val() == i.attr('placeholder'))
                  i.val('');
          })
      });
  }

  var flag = 1;
  $(function() {
    $(window).load(function(){
      show_hide_popup();
      if(getCookie( "show_presentation_popup" )){
        presentation_data = getCookie("show_presentation_popup")
        presentation_data = jQuery.parseJSON(presentation_data)
        // Presentation modal template
        $("#consentModal .modal-title").html("Presentation instructions")
        $("#consentModal .modal-body").html("<p>Here is the link to access the presentation : <a href='"+presentation_data.link+"' target='_blank'>"+ presentation_data.link +"</a></p><p>Use this passcode : "+presentation_data.passcode+"</p>")

        $("#consentModal").modal({
          backdrop: 'static'
        })
        deleteCookie("show_presentation_popup")
      }
    });

    function show_hide_popup(){
      sub = getCookie( "newsletter" );
      hide = getCookie( "hide_box" );
      if (sub || hide){
        $('.mail_popup_float').hide();
      }
      else{
        if (flag != 0){
          $('.mail_popup_float').animate({'right':'-35px', 'bottom':'4px'},300);
        }
      }
    }

    /* remove the slidebox when clicking the cross */
      $('.mail_popup_float #closebox').bind('click',function(){
        $('.mail_popup_float').hide();
        setCookie("hide_box", "hide_by_doctor");
        flag = 0;
      });
  });

  // function get_cookie ( cookie_name )
  // {
  //   // http://www.thesitewizard.com/javascripts/cookies.shtml
  //   var cookie_string = document.cookie ;
  //   if (cookie_string.length != 0) {
  //   console.log(cookie_string)
  //     var cookie_value = cookie_string.match ( '(^|;)[\s]*' + cookie_name + '=([^;]*)' );
  //     return decodeURIComponent ( cookie_value[2] ) ;
  //   }
  //   return '' ;
  // }

  function setCookie(cname, cvalue) {
    document.cookie = cname + "=" + cvalue + "; path=/";
  }

  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
  }

  function deleteCookie(cname) {
    document.cookie = cname + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

  $('#planModal').on('hidden.bs.modal', function() {
    $(".modal-backdrop").remove();
  });


});

function deleteAllCookie(allPlanCookies) {
  for (i=0; i<allPlanCookies.length; i++) {
    document.cookie = allPlanCookies[i] + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
  }
}

function set_cookie() {
  deleteCookie("subscribed_to")
  document.cookie = "free_plan=freePlan; path=/";
}
function deleteCookie(cname) {
  document.cookie = cname + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
}


//FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
function autoPlayYouTubeModal() {
  // $('#videoModal').on('shown.bs.modal', function () {
  //     $('#videoModal .modal-body').html("<div style='padding: 0 15px 6px 15px;position: relative;padding-bottom: 65.25%;padding-top: 30px;height: 0;overflow: hidden;'><iframe id='yplayer' src='https://www.youtube.com/embed/BBimem5o5G4?autoplay=1&showinfo=0&controls=0&rel=0&modestbranding=1' frameborder='0' allowfullscreen style='position: absolute;top: 0;left: 0;width: 98%;height: 98%;margin-left:2%;margin-bottom:2%'><video preload='auto' controls><source src='http://tpstracker.com/demo/einformedconsent/E-info_V3.mp4' type='video/mp4'><source src='http://tpstracker.com/demo/einformedconsent/E-info_V3.ogv' type='video/ogv'><source src='http://tpstracker.com/demo/einformedconsent/E-info_V3.webm' type='video/webm'></video></iframe></div>")

      // $('#videoModal .modal-body').append('<div style="padding: 0 15px 6px 15px;position: relative;padding-bottom: 65.25%;padding-top: 30px;height: 0;overflow: hidden;"><iframe frameborder="0" width="480" height="270" src="//www.dailymotion.com/embed/video/k2EKhfmIXHyQS3cmgyY?autoPlay=1&controls=0" allowfullscreen frameborder="0" style="position: absolute;top: 0;left: 0;width: 98%;height: 98%;margin-left:2%;margin-bottom:2%"></iframe></div>');

      //$('#videoModal .modal-body').append("<div style='padding: 0 15px 6px 15px;position: relative;padding-bottom: 65.25%;padding-top: 30px;height: 0;overflow: hidden;'><iframe id='yplayer' src='https://player.vimeo.com/video/135658230?autoplay=1&title=0&byline=0' frameborder='0' allowfullscreen style='position: absolute;top: 0;left: 0;width: 98%;height: 98%;margin-left:2%;margin-bottom:2%'></iframe></div>")

      // $('#videoModal .modal-body').append("<div style='padding: 0 15px 6px 15px;position: relative;padding-bottom: 65.25%;padding-top: 30px;height: 0;overflow: hidden;'><div id='vimeoEmbedder'></div></div>")
// });



    // $('#videoModal').on('shown.bs.modal', function () {
    //   $('#videoModal .modal-body').html('<div style="padding: 0 15px 6px 15px;position: relative;padding-bottom: 65.25%;padding-top: 30px;height: 0;overflow: hidden;"><iframe frameborder="0" width="480" height="270" src="//www.dailymotion.com/embed/video/k135QKymCewMWgcmgYA?autoPlay=1&controls=0&rel=0&html=1" allowfullscreen frameborder="0" style="position: absolute;top: 0;left: 0;width: 98%;height: 98%;margin-left:2%;margin-bottom:2%"></iframe></div>');
    // })

// var tag = document.createElement('script');
//         tag.src = "https://www.youtube.com/player_api";
//         var firstScriptTag = document.getElementsByTagName('script')[0];
//         firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//   var player;
//         function onYouTubePlayerAPIReady() {
//             player = new YT.Player('ytplayer', {
//                 height:'390',
//                 width:'640',
//                 videoId:'BBimem5o5G4',
//                 playerVars :{
//                     'showinfo':0
//                 },
//                 events:{
//                     'onReady':onPlayerReady,
//                     'onStateChange':onPlayerStateChange
//                 }
//             });
//         }

//         function onPlayerReady(event) {
//             _isPlayerReady = true;
//         }
//         function onPlayerStateChange(event) {
//             switch (event.data) {
//                 case YT.PlayerState.ENDED:
//                 $j  = jQuery.noConflict();
//                 $j(".vid-flow").hide();
//                 ShowCustomDialog();
//                     break;
//                 case YT.PlayerState.PLAYING:
//                     break;
//                 case YT.PlayerState.PAUSED:
//                     break;
//                 case YT.PlayerState.BUFFERING:
//                     break;
//                 case YT.PlayerState.CUED:
//                     break;
//                 default:
//                     break;
//             }
//         }



  // $('#videoModal').on('shown.bs.modal', function () {
      // $('#videoModal .modal-body').append('<div id="jp_container_1" class="jp-video jp-video-360p" role="application" aria-label="media player"><div class="jp-type-single"><div id="jquery_jplayer_1" class="jp-jplayer"></div><div class="jp-gui"><div class="jp-video-play"><button class="jp-video-play-icon" role="button" tabindex="0">play</button></div><div class="jp-interface"><div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div><div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div><div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div><div class="jp-controls-holder"><div class="jp-toggles"><button class="jp-full-screen" role="button" tabindex="0">full screen</button></div></div><div class="jp-details"><div class="jp-title" aria-label="title">&nbsp;</div></div></div></div><div class="jp-no-solution"><span>Update Required</span>To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.</div></div></div>')

// By narendra
//        if ( yourVideoElement.readyState === 4 ) {
//     // it's loaded
// }

// $('#videoModal .modal-body').append('<!-- Start EasyHtml5Video.com BODY section --><div class="easyhtml5video" style="position:relative;max-width:854px;"><video controls="controls"  autoplay="autoplay" poster="eh5v.files/html5video/E-info_V3.jpg" style="width:100%" title="E-info_V3"><source src="eh5v.files/html5video/E-info_V3.m4v" type="video/mp4" /><source src="eh5v.files/html5video/E-info_V3.webm" type="video/webm" /><source src="eh5v.files/html5video/E-info_V3.ogv" type="video/ogg" /><object type="application/x-shockwave-flash" data="eh5v.files/html5video/flashfox.swf" width="854" height="480" style="position:relative;">
// <param name="movie" value="eh5v.files/html5video/flashfox.swf" /><param name="allowFullScreen" value="true" />
// <param name="flashVars" value="autoplay=true&controls=true&fullScreenEnabled=true&posterOnEnd=true&loop=false&poster=eh5v.files/html5video/E-info_V3.jpg&src=E-info_V3.m4v" />
//  <embed src="eh5v.files/html5video/flashfox.swf" width="854" height="480" style="position:relative;"  flashVars="autoplay=true&controls=true&fullScreenEnabled=true&posterOnEnd=true&loop=false&poster=eh5v.files/html5video/E-info_V3.jpg&src=E-info_V3.m4v" allowFullScreen="true" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_en" />
// <img alt="E-info_V3" src="eh5v.files/html5video/E-info_V3.jpg" style="position:absolute;left:0;" width="100%" title="Video playback is not supported by your browser" />
// </object>
// </video><div class="eh5v_script"><a href="http://easyhtml5video.com">html 5 video tag</a> by EasyHtml5Video.com v3.5</div></div>
// <script src="eh5v.files/html5video/html5ext.js" type="text/javascript"></script>
// <!-- End EasyHtml5Video.com BODY section -->

//     ')

      // by narendra please dont remove
      // ===============================


      // $('#videoModal .modal-body').append("<div style='padding: 0 15px 6px 15px;'><div class='embed-responsive-16by9'><iframe style='border:none;' class='embed-responsive-item modal-video-iframe' src='http://tpstracker.com/demo/einformedconsent/E-info_V3.mp4'></iframe></div></div>")
      // var winH = $(window).width()/3;
      // var winW = $(window).width();
      // var winWW = winW / 1.5;
      // var winHH = winWW - winH;
      // // var winW = $("#videoModal").width();
      // // var winH = $("#videoModal").width() / 1.5;

      // $("iframe").css("height",winHH + "px");
      // $("iframe").css("width",winWW + "px");
      // $("iframe > video").css("height",winHH + "px");
      // $("iframe > video").css("width",winWW + "px");

      // $(window).resize(function(){
      //   var winH = $(window).width()/3;
      // var winW = $(window).width();
      // var winWW = winW / 1.5;
      // var winHH = winWW - winH;
      // // var winW = $("#videoModal").width();
      // // var winH = $("#videoModal").width() / 1.5;

      // $("iframe").css("height",winHH + "px");
      // $("iframe").css("width",winWW + "px");
      // $("iframe > video").css("height",winHH + "px");
      // $("iframe > video").css("width",winWW + "px");
      // });


  //   $("#jquery_jplayer_1").jPlayer({
  //   ready: function () {
  //     $(this).jPlayer("setMedia", {
  //       title: "Big Buck Bunny",
  //       m4v: "http://tpstracker.com/demo/einformedconsent/E-info_V3.mp4",
  //       ogv: "http://tpstracker.com/demo/einformedconsent/E-info_V3.ogv",
  //       webmv: "http://tpstracker.com/demo/einformedconsent/E-info_V3.webm"
  //       // poster: "http://www.jplayer.org/video/poster/Big_Buck_Bunny_Trailer_480x270.png"
  //     });
  //   },
  //   // swfPath: "../../dist/jplayer",
  //   supplied: "webmv, ogv, m4v",
  //   size: {
  //     width: "100%",
  //     height: "100%",
  //     cssClass: "jp-video-360p"
  //   },
  //   useStateClassSkin: true,
  //   autoBlur: false,
  //   smoothPlayBar: true,
  //   keyEnabled: true,
  //   remainingDuration: true,
  //   toggleDuration: true
  // });
// })


  // html 5 video code by ND

  // $(".link-video").click(function(){
  //   // $("p").html("Hello <b>world</b>!");
  //   // browser Detection
  //   var is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
  //   var is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
  //   var is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
  //   var is_safari = navigator.userAgent.indexOf("Safari") > -1;
  //   var is_opera = navigator.userAgent.toLowerCase().indexOf("op") > -1;
  //   if ((is_chrome) && (is_safari)) {
  //       is_safari = false;
  //   }
  //   if ((is_chrome) && (is_opera)) {
  //       is_chrome = false;
  //   }

  //   // IE detection
  //   var ua = window.navigator.userAgent;
  //   var msie = ua.indexOf("MSIE ");

  //   // If Internet Explorer,
  //   if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
  //    //  code goes here
  //   }
  //   if(is_chrome){
  //     $('#videoModal .modal-body').append("<div style='padding: 0 15px 6px 15px;'><video controls='false' preload='metadata'><source src='http://tpstracker.com/demo/einformedconsent/E-info_V3.mp4' type='video/mp4'><source src='http://tpstracker.com/demo/einformedconsent/E-info_V3.ogv' type='video/ogv'><source src='http://tpstracker.com/demo/einformedconsent/E-info_V3.webm' type='video/webm'></video> </div>")
  //   }
  //   else{

    // OLD CODE BEFORE SPELLING MISTACK
    // $('#videoModal').on('shown.bs.modal', function () {
    //   $('#videoModal .modal-body').html('<style type="text/css">.easyhtml5video .eh5v_script{display:none}</style><div class="easyhtml5video" style="position:relative;"><video controls="controls"  autoplay="autoplay" poster="/assets/E-info_V3.jpg" style="width:100%" title="E-info_V3"><source src="http://tpstracker.com/demo/einformedconsent/E-info_V3.mp4" type="video/mp4" /><source src="http://tpstracker.com/demo/einformedconsent/E-info_V3.ogv" type="video/ogv"><source src="http://tpstracker.com/demo/einformedconsent/E-info_V3.webm" type="video/webm" /><source src="/assets/E-info_V3.ogv" type="video/ogg" /><source src="/assets/E-info_v3.mpeg" type="video/mpeg" /></video>')
    // })

    // NEW CODE AFTER SPELLING MISTACK
    $('#videoModal').on('shown.bs.modal', function () {
      $('#videoModal .modal-body').html('<style type="text/css">.easyhtml5video .eh5v_script{display:none}</style><div class="easyhtml5video" style="position:relative;"><video id="myvid" autoplay controls poster="/assets/animation_poster.gif" style="width:100%" title="E-info_V3"><source type="video/mp4" src="http://www.tpstracker.com/demo/einformedconsent/V5/Einfo_V5.mp4"><source type="video/ogg" src="http://www.tpstracker.com/demo/einformedconsent/V5/Einfo_V5.ogv"><source type="video/webm" src="http://www.tpstracker.com/demo/einformedconsent/V5/Einfo_V5.webm"></video>')
    })




  //   }
  // });

  $('#videoModal .modal-close').on('click',function () {
    // alert("asdf");
    $('#videoModal .modal-body').html("");
  })
}

// js coding with modules
(function(module){

  function setCookie(cname, cvalue) {
    document.cookie = cname + "=" + cvalue + "; path=/";
  }

  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
  }

  function deleteCookie(cname) {
    document.cookie = cname + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

  module.generate_presentation_link=function(mail_send_path, report_code, report_link){

    $(document).on("change", "#show_link_chk", function(event) {
      if(this.checked){
        $("#process_link")[0].href = "/";
        $("#process_link").html("<span class='glyphicon glyphicon-list-alt'></span> Show me instructions to view Presentation");
      }
      else{
        $("#process_link")[0].href = mail_send_path;
        $("#process_link").html("<span class='glyphicon glyphicon-share-alt'></span> Send Email to Patient");
      }
    });

    $(document).on("click", "#process_link", function(e) {
      e.preventDefault();
      if($("#show_link_chk")[0].checked){
        setCookie("show_presentation_popup", '{"passcode": "'+report_code+'", "link": "'+report_link+'"}')
        window.location.href = "/"
      }
      else{
        window.location.href = mail_send_path
      }
    });

  } // END of "generate_presentation_link" function

})(window.CONSENT)