require 'spec_helper'

describe DoctorProfileController do  

  describe 'GET #index' do
    let!(:doctor) { create(:doctor)}      
    before { sign_in doctor }
    it "populates an information of doctor" do
      get :index         
      expect(assigns(:doctor_profile)).to eq doctor
    end

    it "renders the :index view" do
      get :index
      response.should render_template :index
      expect(response).to render_template :index
    end
  end
  
  describe 'GET #edit' do
    let!(:doctor) { create(:doctor)}
    context "if doctor signed in" do
      before { sign_in doctor }
      it "assigns the requested doctor to @doctor_profile" do
        get :edit, id: doctor
        expect(assigns(:doctor_profile)).to eq doctor
        response.should be_success
      end

      it "renders the :edit template" do
        get :edit, id: doctor
        expect(response).to render_template :edit
      end
    end

    context "if doctor is not signed in" do
      it "renders the :edit template" do
        get :edit, id: doctor
        expect(response).to redirect_to(new_doctor_session_path)      
      end

      it "renders the :edit template" do
        get :edit, id: doctor
        expect(response).to redirect_to(new_doctor_session_path)      
      end
    end     
  end

  describe 'PUT #update' do
    before :each do
      @doctor = create(:doctor, doctor_name: "Aaron Sumner",
      email: "aaron@everydayrails.com")
      sign_in @doctor
    end

    it "locates the requested @doctor" do
      put :update, id: @doctor, doctor: attributes_for(:doctor)
      expect(assigns(:doctor_profile)).to eq(@doctor)
    end

    context "valid attributes" do
      it "changes @doctor's attributes" do
        put :update, id: @doctor,
        doctor: attributes_for(:doctor,
        doctor_name: "A. Sumner")
        @doctor.reload
        expect(@doctor.doctor_name).to eq("A. Sumner")
      end

      it "redirects to the updated message" do
        put :update, id: @doctor, doctor: attributes_for(:doctor)
        response.should redirect_to patients_path
        expect(response).to redirect_to patients_path
      end
    end
  end
end
