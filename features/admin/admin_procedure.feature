Feature: Procedure

  In order to add procedures 
  As an admin,
  I want to add procedure

  Background:
    Given Admin already logged in

  Scenario: Check for procedure link
    And I am on dashboard
    When I click on procedure menu
    Then I should see procedures link

  Scenario: Click on Procedures link
    Given Admin clicks on procedure menu
    When I click on procedures link
    Then I should see list of procedures
    And I should see New Procedure  

  Scenario: Create procedure with invalid information
    Given Admin visits new procedure page
    When He submit invalid procedure information
    Then He should see error message
    And He visits new procedure page

  Scenario: Procedure with valid information
    Given the folowing procedure exists  
      |Procedure Name |speciality   |procedure description |
      |test - 1       |Ortho        |Testr                 |
    And Speciality availables
    And Admin visits new procedure page
    And I fill valid procedure information
    When I select "Ortho" from a "procedure_speciality_id"
    And I press create procedure button
    Then I should see successful message for procedure