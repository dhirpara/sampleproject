class AddCorrectCountryToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_country, :string
  end
end
