class PaymentsController < ApplicationController
  # before_filter :authenticate_doctor!
  include ActionView::Helpers::TextHelper

  layout 'authorize_net'
  helper :authorize_net

  def new
    @payment = Payment.new
  end

  def create
    logger.info "<--------------#{params}---------------------->"
    @payment = Payment.new(:first_name => params[:first_name],:last_name => params[:last_name])

    @c1=params[:payment][:credit_card_number1]
    @c2=params[:payment][:credit_card_number2]
    @c3=params[:payment][:credit_card_number3]
    @c4=params[:payment][:credit_card_number4]

    @month=params[:credit_month]
    @year= params[:credit_year]
    # logger.info ">>>>>month>>>>>#{@month}"
    # logger.info ">>>>>year>>>>>#{@year}"

    @cc = @c1 + @c2 + @c3 + @c4
    @exp_date = @month + @year
    @card_code = params[:payment][:card_code]
    logger.info "<--------------#{@cc}---------------------->"

    credit_card = AuthorizeNet::CreditCard.new("#{@cc}", "#{@exp_date}", :card_code => "#{@card_code}")
    name = AuthorizeNet::Address.new(:first_name => params[:payment][:first_name], :last_name => params[:payment][:last_name])
    @amount = 30.00

    @payment = @payment.process_payment(credit_card,name)
    if @payment.success?
        @payment.save
        @doctor =  session[:subcribe_doc]
        @doctor.update_attributes(:subscription_plan => "Recurring Plan",:recuring_date=>Time.now)
        logger.info ">>>>>> #{@doctor.subscription_plan}<<<<<<<<<"
        redirect_to  payments_thank_you_path
    else
        flash[:error] = "payment failed...please enter valid credit card number or expire date"
        render :action => :new
    end
  end

  def select_plan
    return redirect_to root_path unless session[:subcribe_doc]
    sign_in(:doctor, session[:subcribe_doc])

    @one_year_amount = 495
    @one_year_sim_transaction = SetAuthorizeObject.new(@one_year_amount, payments_thank_you_url(:only_path => false)).set_sim_transaction
    @two_years_amount = 895
    @two_years_sim_transaction = SetAuthorizeObject.new(@two_years_amount, payments_thank_you_url(:only_path => false)).set_sim_transaction
    # if session[:subcribe_doc].subscription_plan == "Expired"
    #   flash[:danger] = "Your subscription plan has been expired on #{session[:subcribe_doc].expire_date.to_date}. Please select a plan to continue."
    # end
  end

  def thank_you
    @doctor = session[:subcribe_doc]
    # @auth_code = params[:x_auth_code]
    plan = authenticate_params(params)
    @doctor.subcribe_to(plan)
    sign_in(:doctor, @doctor)
  end

  def plan_detail
    cookies.delete :free_plan
    cookies.delete :subscribed_to
  end

  def webhook
    logger.info"@@@@@@@@@@@@@@@@@@@@@@==========Webhook called==============@@@@@@@@@@@@@@@@@@@@@@"
    render text: "success"
  end

  private

    def authenticate_params(params)
      return plan = if params[:x_plan].present? && params[:x_plan_interval_in_year].present?
        { plan: params[:x_plan], interval: params[:x_plan_interval_in_year] }
      else
        if params[:x_amount] == "495.00"
          interval = "1"
        elsif params[:x_amount] == "895.00"
          interval = "2"
        end
        { plan: pluralize(interval, 'year') + " plan", interval: interval }
      end
    end
end