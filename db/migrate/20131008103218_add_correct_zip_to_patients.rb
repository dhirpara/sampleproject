class AddCorrectZipToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_zip, :integer
  end
end
