class DoctorMailer < ActionMailer::Base
 default from: "from@example.com"

  def patient_registration_confirmation(report,doctor,speciality,patient_report_f,patient_report_l,patient_perticuler)
    @report = report
    @doctor = doctor
    @speciality=speciality
    @patient_report = patient_report_f
    @patient_report_l = patient_report_l
    @patient_perticuler = patient_perticuler
    @doctor_full_name = @doctor.doctor_name.strip.split(/\s+/).map(&:capitalize)

    email_logo = File.read(Rails.root.join('app/assets/images/maillogo/logo.png'))
    attachments.inline['logo.png'] = email_logo

    report_pdf_view = DoctorPdf.new(@report,@doctor,@speciality,@patient_report,@patient_report_l,@patient_perticuler)

    report_pdf_content = report_pdf_view.render()


    attachments["Presentation_detail_of_#{@report.patient_first_name}.pdf"] = {
      mime_type: 'application/pdf',
      content: report_pdf_content
    }
    mail(:to => @doctor.email, :subject => "Information regarding your selected presentation for #{@report.patient_first_name.capitalize} #{@report.patient_last_name.capitalize}.")

  end

  def dont_agee_notification(report,patient,doctor,speciality_doc)
    @report = report
    @patient = patient
    @doctor = doctor
    @speciality = speciality_doc
    @doctor_full_name = @doctor.doctor_name.strip.split(/\s+/).map(&:capitalize)

    email_logo = File.read(Rails.root.join('app/assets/images/maillogo/logo.png'))
    attachments.inline['logo.png'] = email_logo

    mail(:to => @doctor.email, :subject => "Patient decided not to proceed")

  end

  def contact_us_notification(contact_us)
    @contact_us =contact_us


    email_logo = File.read(Rails.root.join('app/assets/images/maillogo/logo.png'))
    attachments.inline['logo.png'] = email_logo
    # ["bob1@professional-alliance.com","info@techplussoftware.com"]
    # mail(:to => "krunalb.07@gmail.com",bcc: ["krunal_babaria07@yahoo.co.in"], :subject => "Contact Us")
    mail(:to => "info@einformedconsent.com",:cc =>"rmaynard2600@yahoo.com",:bcc=>"info@techplussoftware.com", :subject => "Contact Us")
  end

  def trial_expired_notification(doctor)
    @doctor = doctor
    email_logo = File.read(Rails.root.join('app/assets/images/maillogo/logo.png'))
    attachments.inline['logo.png'] = email_logo
    # mail(:to => @doctor.email,:bcc=>"info@einformedconsent.com, rmaynard2600@yahoo.com", :subject => "Free period expiration Notification(Expires on #{@doctor.expire_date.to_datetime.strftime("%m/%d/%Y")})")
    mail(:to => @doctor.email,:bcc=>"jignesh.tps@gmail.com", :subject => "Free period expiration Notification(Expires on #{@doctor.expire_date.to_datetime.strftime("%m/%d/%Y")})")
  end
end


