ActiveAdmin.register Speciality do

  menu :priority => 5


  form do |f|
    f.inputs "Details" do
      f.input :spe_name, :label => "Speciality Name"
      f.input :spe_description, :label => "Speciality Discription", :input_html => { :rows => 5, :cols => 30, :maxlength => 10  }
    end
    f.buttons
  end

  index do
    selectable_column
    column :id
    column "Speciality Name" do |spe|
      link_to spe.spe_name.capitalize, admin_speciality_path(spe)
    end
    column "Procedure count" do |speciality|
      speciality.procedures.count
    end
    column :created_at
    column :updated_at

    default_actions
  end

  show do
    attributes_table do
      row :id
      row "Speciality Name" do |spe|
        spe.spe_name
      end
      row "Speciality Description" do |spe|
        spe.spe_description
      end

      row :created_at
      row :updated_at
    end
  end

  filter :spe_name, :label => "Speciality Name"
  filter :created_at
  filter :updated_at
end