class AddQueryStrToReportMenus < ActiveRecord::Migration
  def change
    add_column :report_menus, :query_str, :string
  end
end
