class ChangeDataTypeForPatientPhoneAndCode < ActiveRecord::Migration
  def up
  	change_table :patients do |t|
      t.change :phone_code, :string
      t.change :phone_number, :string
    end
  end

  def down
  	change_table :patients do |t|
      t.change :phone_code, :integer
      t.change :phone_number, :integer
    end
  end
end
