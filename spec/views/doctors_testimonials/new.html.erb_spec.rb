require 'spec_helper'

describe "doctors_testimonials/new" do
  before(:each) do
    assign(:doctors_testimonial, stub_model(DoctorsTestimonial,
      :description => "MyText",
      :status => "MyString",
      :doctor => nil
    ).as_new_record)
  end

  it "renders new doctors_testimonial form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", doctors_testimonials_path, "post" do
      assert_select "textarea#doctors_testimonial_description[name=?]", "doctors_testimonial[description]"
      assert_select "input#doctors_testimonial_status[name=?]", "doctors_testimonial[status]"
      assert_select "input#doctors_testimonial_doctor[name=?]", "doctors_testimonial[doctor]"
    end
  end
end
