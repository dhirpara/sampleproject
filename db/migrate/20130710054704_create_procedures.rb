class CreateProcedures < ActiveRecord::Migration
  def change
    create_table :procedures do |t|
      t.string :procedure_name
      t.text :procedure_description

      t.timestamps
    end
  end
end
