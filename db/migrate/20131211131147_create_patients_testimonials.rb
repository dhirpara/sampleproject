class CreatePatientsTestimonials < ActiveRecord::Migration
  def change
    create_table :patients_testimonials do |t|
      t.text :description
      t.string :status
      t.references :patient

      t.timestamps
    end
    add_index :patients_testimonials, :patient_id
  end
end
