Consent::Application.routes.draw do

  resources :subscribers, only: [:new, :create]
  resources :contact_us
  resources :patients_testimonials
  resources :doctors_testimonials
  resources :report_questions, :only => [:new, :create, :destroy]

  match "report_questions/reply",:to => "report_questions#reply",:via=> [:get]
  match ":id/report_questions",:to => "report_questions#index",:as=>:report_questions,:via=> [:get]

  match "report_questions/:id",:to => "report_menus#show",:as=>:report_question,:via=> [:get]

  match ":id/report_questions",:to => "report_questions#create",:as=>:report_questions,:via=> [:post]

  authenticated :doctor do
    root :to => 'patients#index'
  end

  match "/admin/active_doctors" => 'admin/doctors#active_doctors', via: :get, as: "active_doctors"
  match "/admin/non-active-doctors" => 'admin/doctors#non_active_doctors', via: :get, as: "non_active_doctors"
  match "/admin/cancel-trial/:id" => 'admin/doctors#admin_cancel_trials', via: :get, as: "admin_cancel_trials"

  authenticated :patient do
    root :to => 'patients#patient_info'
  end

  root :to => "home#index"

  match '/patients/get_emails', :to => "patients#get_emails", :as => :get_emails
  resources :patients
  devise_for :patients, :controllers => { :sessions => "sessions"}, :path => "patient", :path_names => { :sign_in => "login" } do
    match '/patients/signin/:token', :to => 'sessions#new', :as => 'signin_patient', :via => [:get]
  end

  resources :report_menus,:only => [ :create, :new, :update, :edit ]
  match '/report_menus/new/:id' , :to => 'report_menus#new' ,:as => :new_report,:via => [ :get ]

  # E-Signatre routes
  match "embeddocusign/create" =>"embeddocusign#create", :as => :embeddocusign_create


  resources :videos, :only => [:index, :show]

  resources :user_steps
  get "doctor_profile/index"
  get "doctor_profile/edit"
  get "doctor_profile/update"
  get "patient_report/new"
  get "patient_report/create"

  get '/report/download_as_pdf/:id',:to =>"reports#download_as_pdf",:as=> :download_as_pdf

  # match 'report/download_as_pdf', :controller => 'report', :action => 'download_as_pdf', :as => :report_download_as_pdf

  match '/update_consent_report/:id' => "patients#update_consent_report", :as => :update_consent_report

  resources :patient_report
  match 'patient_report/new', :controller => 'patient_report', :action => 'create', :as => :report_for_patient
  match 'profile' => "doctor_profile#edit", :as => :profile
  match 'profile/edit' => "doctor_profile#update", :via => [ :put ], :as => :update_profile
  match "/doctor/cancel-trial/:id" => 'doctor_profile#doctor_cancel_trials', via: :get, as: "doctor_cancel_trials"

  match 'patient/patient_info' => "patient#patient_info", :via => [ :get ]
  match '/consent_report/:id' => "patients#consent_report",:as => :consent_report
  match '/create_report/:id' => "patients#create_report", :as => :create_report
  match '/edit_consent_report/:id' => "patients#edit_consent_report", :as => :edit_consent_report
  match '/patients/show_code', :to => "patients#show_code", :as => :show_code
  match '/subscription/doctor_plan', :to => 'subscription#doctor_plan', :as => :doctor_plan

  match '/reports/access_report', :to => "reports#access_report", :as => :access_report
  match '/reports/access_rep/:id', :to => "reports#access_rep", :as => :access_rep
  match '/reports/show_report', :to => "reports#show_report" , :as => :show_report
  match ':id/login_page', :to => "reports#login_page" , :as => :login_page
  match '/reports/send_mail/:id', :to => "reports#send_mail", :as => :send_mail
  match '/reports/past_report',:to => "reports#past_report", :as => :past_report
  match '/reports/view_report/:id',:to => "reports#view_report",:as => :view_report
  match '/reports/:id/website_desclaimer',:to =>"reports#website_desclaimer",:as=> :website_desclaimer
  get '/reports/:id/pat_report',:to =>"reports#pat_report",:as=> :pat_report

  get '/reports/signature_created_msg',:to=>"reports#signature_created_msg",:as=> :signature_created_msg
  match '/:id/edit' => "confirm_detail#edit", :as => :edit
  match '/update/:id' => "confirm_detail#update", :as => :update
  match '/show/:id' => "confirm_detail#show",:as => :show

  match '/reports/:id/i_do_not_agree', :to => "reports#i_do_not_agree", :as => :i_do_not_agree
  match '/patient/patient_update' => "patient#pat_update", :as => :pat_update
  match 'home/terms_condition' => "home#terms_condition"

  # The priority is based upon order of creation:
  resources :payments, :only => [:new,  :create]
  match '/payments/payment', :to => 'payments#payment', :as => 'paymentspayment', :via => [:get]
  match '/payments/thank_you', :to => 'payments#thank_you', :as => 'payments_thank_you', :via => [:get]
  match '/payments/select_plan', :to => 'payments#select_plan', :as => 'select_plan'
  match '/payments/plan_detail', :to => 'payments#plan_detail', :as => 'plan_detail'
  match '/subscription/doctor_plan', :to => 'subscription#doctor_plan', :as => :doctor_plan
  post 'payments/webhook'

  resources :photos

  get '/home/coming_soon',:to =>'home#coming_soon'
  get '/home/contact_us',:to =>'home#contact_us'

  match '/home/services' =>'home#services', :as => :services
  match '/home/about_us' =>'home#about_us', :as => :about_us
  match '/home/contact_us' =>'home#contact_us', :as => :contact_us
  match '/home/contact_us_submit', :to => "home#contact_us_submit", :as => :contact_us_submit
  match '/home/how_it_works' =>'home#how_it_works', :as => :how_it_works


  devise_for :admin_users, ActiveAdmin::Devise.config


  devise_for :doctors, :controllers => { :registrations => "registrations", :sessions => "sessions",:passwords => "passwords" } do
     get '/doctors/sign_out' => 'devise/sessions#destroy'
     get '/doctors/sign_in' => 'devise/sessions#new'
  end

    match "embeddocusign/create_template" =>"embeddocusign#create_template", :as => :embeddocusign_create_template
    match "embeddocusign/create_envelope_from_template" =>"embeddocusign#create_envelope_from_template", :as => :create_envelope_from_template
    match "embeddocusign/create_env_from_doc/:id" =>"embeddocusign#create_env_from_doc", :as => :create_env_from_doc
    match "embeddocusign/generate_pdf/:id" =>"embeddocusign#generate_pdf", :as => :generate_pdf

  namespace "admin" do
    resources :doctors do
      get :pat_show
    end
    resources :patients do
      get :show_patient_reports
    end
    resources :procedures do
      get :procedure_show_type
    end
    resources :procedure_types do
      get :sub_type
    end
  end

  namespace "admin" do
    resources :videos do
      post :change_procedure
      new do
        post :upload
        get  :save_video
      end
    end
    resources :photos
  end

  match '/admin/videos/change_procedure', :to => "admin/videos#change_procedure"
  match '/admin/procedure_types/:id/create_subtype', :to => "admin/procedure_types#create_subtype" ,:as => 'create_subtype'

  match 'signature_pads/:id' => "signature_pads#open_form", :as => :open_form
  match 'send_mail/:id' => "signature_pads#send_mail", :as => :send_mail_pad


  match  'report/signature_create/:id'=> "reports#signature_create", :as => :signature_create
  match '/signatures/:id' => "reports#sig_create",:as => :sig_create
  match '/thnkyou_msg' => "reports#thnkyou_msg",:as => :thnkyou_msg

 if Rails.env.production?
    get '404', :to => 'application#page_not_found'
    get '422', :to => 'application#server_error'
    get '500', :to => 'application#server_error'
  end


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
  ActiveAdmin.routes(self)
end