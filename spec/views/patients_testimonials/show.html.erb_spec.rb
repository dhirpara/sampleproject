require 'spec_helper'

describe "patients_testimonials/show" do
  before(:each) do
    @patients_testimonial = assign(:patients_testimonial, stub_model(PatientsTestimonial,
      :description => "MyText",
      :status => "Status",
      :patient => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/MyText/)
    rendered.should match(/Status/)
    rendered.should match(//)
  end
end
