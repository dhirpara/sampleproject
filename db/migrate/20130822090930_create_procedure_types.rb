class CreateProcedureTypes < ActiveRecord::Migration
  def change
    create_table :procedure_types do |t|
      t.integer :procedure_id
      t.string :type_name

      t.timestamps
    end
  end
end
