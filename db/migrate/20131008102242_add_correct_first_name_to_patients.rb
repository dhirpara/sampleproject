class AddCorrectFirstNameToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_first_name, :string
  end
end
