Given(/^Admin already signed in$/) do
	@admin = FactoryGirl.create(:admin_user, :email => 'admin@example.com', :password => "password")
  visit new_admin_admin_user_path
  fill_in 'Email', :with => @admin.email
  fill_in 'Password', :with => @admin.password
  click_button('Login')
end

When(/^I click on videos link$/) do
  click_link "Videos"
end

Then(/^I should see list of videos$/) do
  expect(page).to have_selector('h2', text: 'Videos')
end

Then(/^I should see new video link$/) do
  page.should have_link('New Video', href: new_admin_video_path)
end

Then(/^I click on new video link$/) do
  click_link "New Video"
end

Then(/^I should see form of video$/) do
  expect(page).to have_selector('form#new_video')
end

#............................... Scenario: 2 add Video with invalid data...........................

Given(/^visit new video page$/) do
  @speciality = FactoryGirl.create(:speciality, :spe_name => "dentist")
  visit new_admin_video_path
end

When(/^I submit video form with blank data$/) do
  click_button "Next"
end

Then(/^I shold see error message for video validation$/) do
  page.should have_content "Title can't be blank"
end

Then(/^I see add video page again$/) do
  visit new_admin_video_path
end

#..................................Scenario: 3 add video with valid data.......................

Given(/^fill valid information$/) do
  @video = FactoryGirl.create(:video)

  fill_in('Title', :with => @video.title)
  fill_in('Description', :with => @video.description)
  page.select 'dentist', :from => 'video[speciality_id]'
  choose('video_video_status_true')
end

When(/^I press create button$/) do
  click_button "Next"
end

Then(/^I should see upload video page$/) do
  visit upload_new_admin_video_path
end