require 'spec_helper'

describe "contact_us/index" do
  before(:each) do
    assign(:contact_us, [
      stub_model(ContactU,
        :name => "Name",
        :email => "Email",
        :phone => "Phone",
        :comment => "Comment"
      ),
      stub_model(ContactU,
        :name => "Name",
        :email => "Email",
        :phone => "Phone",
        :comment => "Comment"
      )
    ])
  end

  it "renders a list of contact_us" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Comment".to_s, :count => 2
  end
end
