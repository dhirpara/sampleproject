require 'spec_helper'
describe "Home Page" do
  describe "Coming Soon Page" do
    it "should have content 'COMING SOON!'" do
      visit home_coming_soon_path
      expect(page).to have_selector('h1', text: 'Coming Soon!')
    end
  end
end