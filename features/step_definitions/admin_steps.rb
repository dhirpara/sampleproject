#Sign in test cases

  Given(/^A admin visits the signin page$/) do
    visit new_admin_user_session_path
  end

  When(/^He submits invalid signin information$/) do
    click_button "Login"
  end

  Then(/^He should see an error message$/) do
    page.should have_content "Invalid email or password"
  end

  Given(/^The admin has an account$/) do
    @admin = FactoryGirl.create(:admin_user)
  end

  When(/^The admin submits valid signin information$/) do
    visit '/admin/login'
    fill_in 'Email', :with => @admin.email
    fill_in 'Password', :with => @admin.password
    click_button('Login')
  end

  Then(/^He should see welcome message$/) do
    page.should have_content "Signed in successfully"
  end

  Then(/^He should see a logout link$/) do
    page.should have_link('Logout', href:  destroy_admin_user_session_path)
  end

  Then(/^He should see a Dashboard link$/) do
    page.should have_link('Dashboard', href:  admin_dashboard_path)
  end

  Then(/^He should see a Admin Users link$/) do
    page.should have_link('Admin Users', href:  admin_admin_users_path )
  end

  Then(/^He should see a Doctors link$/) do
    page.should have_link('Doctors', href: admin_doctors_path )
  end

  Then(/^He should see a Procedures link$/) do
    page.should have_link('Procedures', href: admin_procedures_path )
  end

  Then(/^He should see a Specialities link$/) do
    page.should have_link('Specialities', href: admin_specialities_path  )
  end

#Signout Test-Cases
  
  Given(/^Admin logged in$/) do
    @admin = FactoryGirl.create(:admin_user, :email => 'admin@example.com', :password => 'password')
    visit new_admin_admin_user_path
    fill_in 'Email', :with => @admin.email
    fill_in 'Password', :with => @admin.password
    click_button('Login')
  end

  When(/^Admin click logout link$/) do
    click_link 'Logout'
  end

  Then(/^Admin should be logout$/) do
    visit '/admin'
  end

  Then(/^Admin should see message$/) do
    expect(page).to have_content("You need to sign in or sign up before continuing.")
  end