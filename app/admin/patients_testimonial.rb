ActiveAdmin.register PatientsTestimonial do
menu :parent => "Testimonial", :priority => 2

  form do |f|
    f.inputs "Details" do
      f.input :patient_id, :label => "Patient name", :as => :select, :collection => Patient.all.map{ |p| [p.fullname, p.id] }, :include_blank => "Please select..."
      f.input :description, :label => "Description"
      f.input :status, :as => :select, :collection => [["Show", true],["Hide", false]]
    end
    f.actions
  end

  index :download_links => false do
    selectable_column
      column :id
      column "Patient Name" do |patienttestimonial|
        patienttestimonial.patient_fullname
      end
      column :description
      column :status
      default_actions
  end


  show do |user|
    attributes_table do
      row :id
      row "Patient Name" do |patienttestimonial|
        patienttestimonial.patient_fullname
      end
      row :description
      row :status
    end
  end

end
