Feature: Speciality

  Background:
    Given Admin already logged in

  Scenario: Check for specialities link
    And I am on dashboard
    When I click on specialities link
    Then I should see list of specialities
    And I should see "New Speciality"

  Scenario: Speciality with invalid information
    Given Admin visits new speciality page
    When He submit invalid speciality information
    Then He should see error message
    And He visits new speciality page

  Scenario: Speciality with valid information
    Given Admin visits new speciality page      
    When He submit valid speciality information
    Then He should see successful message