class RenameIsExpiredToTrialExpiredInDoctors < ActiveRecord::Migration
  def up
  	rename_column :doctors, :is_expired, :trial_expired
  end

  def down
  	rename_column :doctors, :trial_expired, :is_expired
  end
end
