# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :report_menu do
    report_id 1
    procedure_id 1
    procedure_type_id 1
    type_subtype_id 1
  end
end
