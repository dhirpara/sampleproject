require 'spec_helper'

describe Speciality do
  before do
    @speciality = Speciality.new(spe_name: "TestSpeciality", spe_description: "TestDescription")
  end

  subject { @speciality }

  it { should respond_to(:spe_name) }
  it { should respond_to(:spe_description) }

  it { should be_valid }

  describe "when speciality name is not present" do
    before { @speciality.spe_name = " " }
    it {should_not be_valid}
  end
end
