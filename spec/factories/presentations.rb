# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :presentation do
    title "MyString"
    video_url "MyString"
  end
end
