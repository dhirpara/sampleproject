class AddSpecialityIdToVideo < ActiveRecord::Migration
  def change
    add_column :videos, :speciality_id, :integer
  end
end
