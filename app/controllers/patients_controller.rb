class PatientsController < ApplicationController
  def new
    @speciality = Speciality.all
    @doctor = Doctor.all
    @patient = current_doctor.patients.new
    #@speciality_wise_procedure = current_doctor.speciality.procedures

    respond_to do |format|
      format.html { render :layout => false }
      format.json {render json: @patient}
      format.js
    end
  end

  def create
    if params[:patient][:birthdate].present?
        date_params = params[:patient][:birthdate].split('/')
        params[:patient][:birthdate] = date_params[1]+'-'+date_params[0]+'-'+date_params[2]
    end
    @patient = current_doctor.patients.new(params[:patient])
    @speciality_wise_procedure = current_doctor.speciality.procedures
    # puts "-------#{@patient.email}------------"
    # @patient_cnt = 0
    # current_doctor.patients.each do |patient|
    #   if patient.email == @patient.email
    #     puts "already taken"
    #     @patient_cnt = 1
    #   end
    # end
    respond_to do |format|
      # if @patient_cnt == 0
        if @patient.save
            @patient.update_attributes(:correct_first_name => @patient.first_name,:correct_email => @patient.email,:correct_last_name => @patient.last_name)
            @patient.update_attributes(:correct_city => @patient.city,:correct_state => @patient.state,:correct_country => @patient.country)
            @patient.update_attributes(:correct_address => @patient.address,:correct_zip=>@patient.zip)
            @patient.update_attributes(:correct_phone_number=>@patient.phone_number,:correct_birthdate => @patient.birthdate)
            @patient.update_attributes(:correct_gender_number=>@patient.gender)

            format.html { redirect_to @patient,notice: 'Patient was successfully created.' }
            format.json {render json: @patient, status: :created, location: @patient }
            format.js
        else
          format.html { render "new" }
          format.json { render json: @patient.errors, status: :unprocessable_entity }
          format.js
        end
      # else
      #   format.html { redirect_to patients_path,notice: 'Patient was already created.' }
      #   format.js {flash[:notice]='Patient was already created.'}
      # end
    end
  end

  def show
    @patient = Patient.find(params[:id])
     respond_to do |format|
      format.html { render :layout => false }
    end
  end

  def index
    session[:patient_report] = {}
    session[:report] = {}
    @patients = current_doctor.patients
    @reports = Report.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @patients }
    end
  end

  def edit
    @patient = Patient.find(params[:id])
    @speciality_wise_procedure = current_doctor.speciality.procedures
    respond_to do |format|
      format.html { render :layout => false }
      format.js
    end
  end

  def destroy
    @patient = Patient.find(params[:id])
    @patient.destroy

    respond_to do |format|
      format.html { redirect_to patients_url, notice: 'Patient was successfully delete.' }
      format.json { head :no_content }
    end
  end

  def update
    @patient = Patient.find(params[:id])
    if params[:patient][:birthdate].present?
      date_params = params[:patient][:birthdate].split('/')
      params[:patient][:birthdate] = date_params[1]+'-'+date_params[0]+'-'+date_params[2]
    end
    respond_to do |format|
      if @patient.update_attributes(params[:patient].except(:email))
        format.html { redirect_to patients_path, notice: 'Patient was successfully updated.' }
        format.json { head :no_content }
        format.html { render :layout => false }
        format.js
      else
        format.html { render "edit" }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
        format.html { render :layout => false }
        format.js
      end
    end
  end

  def patient_info
    @patient = current_patient
  end

  def consent_report
    @patient_report = Patient.find(params[:id])
    @procedures = Procedure.all
    @procedure_type = ProcedureType.all

    respond_to do |format|
      session[:patient_report] = @patient_report.id
      format.js #consent_report.js.erb
    end
  end

  def create_report
    @video_squence = params[:sequence]
    @video_squence_only_arr = @video_squence.split(",")

    @patient = Patient.find(params[:id])
    @procedure_type = ProcedureType.order( 'procedure_types.id' )
    @report = Report.new(:doctor_id => current_doctor.id,:patient_id => @patient.id)
    respond_to do |format|
      if @report.save
        session[:report] = @report.id
        @video_squence_only_arr.each do |arr|
          each_seq_arr = arr.split("?q=")
          seq = each_seq_arr[0]
          qry = each_seq_arr[1]

          @report_menu = ReportMenu.new( :report_id => @report.id, :sequence => seq, :query_str => qry)
          @report_menu.save
        end
        if @report_menu.save
          @report.report_code = Array.new(8){rand(36).to_s(36)}.join
          @report.save
          format.js
          #render :action => 'show_code'
        end
      end
    end
  end

  def edit_consent_report
    @patient = Patient.find(session[:patient_report])
    @procedures = Procedure.all
    @procedure_type = ProcedureType.all
    @report = Report.find(session[:report])
    @report_menu = @report.report_menus
    respond_to do |format|
      format.js
    end
  end

  def update_consent_report
    @patient = Patient.find(session[:patient_report])
    @procedure_type = ProcedureType.all
    @report = Report.find(session[:report])
    @report_menu = @report.report_menus
    @report_menu.each do |report_menu|
      report_menu.destroy
    end
    respond_to do |format|
      @procedure_type= params[:types_ids].collect {|id| id.to_i} if params[:types_ids]
      @procedure_type.each do |i|
          @report_menus = ReportMenu.new(:report_id => @report.id,:procedure_type_ids => i)
          @report_menus.save
      end
      if @report_menus.save
           @report.report_code = Array.new(8){rand(36).to_s(36)}.join
           @report.save
      end
      format.js
    end
  end

  def show_code
    @patient = Patient.find(session[:patient_report])
    @report = Report.find(params[:id])
  end

  def get_emails
    patient_emails = Patient.pluck(:email)
    render :json => patient_emails
  end
end