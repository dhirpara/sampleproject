class ChangeDataTypeForDoctorPhone < ActiveRecord::Migration
  def up
  	change_table :doctors do |t|
      t.change :phone, :string
    end
  end

  def down
  	change_table :doctors do |t|
      t.change :phone, :integer
    end
  end
end
