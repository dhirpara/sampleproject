class SessionsController < Devise::SessionsController
  respond_to :js

  def new
    super
  end

  def create
    @doctor = Doctor.find_by_email(params[:doctor][:email])
    if @doctor && @doctor.valid_password?(params[:doctor][:password])
      if @doctor.has_authenticate_plan?
        self.resource = warden.authenticate!(auth_options)
      else
        session[:subcribe_doc] = @doctor
      end
    else
      flash[:alert] = "Invalid email or password."
      render 'new'
      flash.discard
    end
  end

  private

  def after_sign_out_path_for(resource_or_scope)
    # To show newsletter box
    cookies.delete :hide_box if cookies[:hide_box]
    root_path
  end
end