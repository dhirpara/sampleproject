class AddCorrectStateToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :correct_state, :string
  end
end
