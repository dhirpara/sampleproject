class AddDoctorNameAndPracticeNameAndSpecialityAndAddressAndPhoneAndSexAndCityAndStateAndZipToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :doctor_name, :string
    add_column :doctors, :practise_name, :string
    add_column :doctors, :speciality, :string
    add_column :doctors, :address, :text
    add_column :doctors, :phone, :integer
    add_column :doctors, :sex, :boolean
    add_column :doctors, :city, :string
    add_column :doctors, :state, :string
    add_column :doctors, :zip, :integer
  end
end
