# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :doctors_testimonial do
    description "MyText"
    status "MyString"
    doctor nil
  end
end
