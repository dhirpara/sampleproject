# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# def speciality
# 	yield
# 	Speciality.create([
# 		 { spe_name: "Dental Implants", spe_description: "nothing..." }
# 	])
# end

# def presentation
# 	yield
# 	Presentation.create([
# 		{title: "Oral Sedation", video_url: "http://www.tpstracker.com/demo/consent/video/oralsedation/video", js_key: "s2"},
# 		{title: "IV Sedation", video_url: "http://www.tpstracker.com/demo/consent/video/intravenous/video", js_key: "s3"},
# 		{title: "Nitrous", video_url: "http://www.tpstracker.com/demo/consent/video/nitrousscript/video", js_key: "s4"},
# 		{title: "Bone Graft", video_url: "http://www.tpstracker.com/demo/consent/video/bonegraft/video", js_key: "s10"},
# 		{title: "Sinus Augmentation", video_url: "http://www.tpstracker.com/demo/consent/video/sinusaugmentation/video", js_key: "s11"},
# 		{title: "Implant Introduction", video_url: "http://www.tpstracker.com/demo/consent/video/programintro/video", js_key: "s1"},
# 		{title: "Extraction Standalone", video_url: "http://www.tpstracker.com/demo/consent/video/standalone/video", js_key: "s7"},
# 		{title: "Extraction with Socket Graft ", video_url: "http://www.tpstracker.com/demo/consent/video/standalonesocket/video",js_key: "s8"},
# 		{title: "Extration Generic", video_url: "http://www.tpstracker.com/demo/consent/video/generic/video", js_key: "s5"},
# 		{title: "Extraction Generic with Socket", video_url: "http://www.tpstracker.com/demo/consent/video/genericsocket/video",js_key: "s6"},
# 		{title: "Implant Surgery Healing", video_url: "http://www.tpstracker.com/demo/consent/video/surgeryhealing/video", js_key:"s9"},
# 		{title: "What To Expect Video List part1 without sedation", video_url: "http://www.tpstracker.com/demo/consent/video/whattoexpect1/part1_withoutivsedation/video", js_key: "s13"},
# 		{title: "What To Expect Video List", video_url: "http://www.tpstracker.com/demo/consent/video/whattoexpect1/video", js_key:"s12"},
# 		{title: "What To Expect Video List Summary", video_url: "http://www.tpstracker.com/demo/consent/video/whattoexpect1/part3/video",js_key: "s21"},
# 		{title: "What To Expect Video List - Possible Complications(Part 3)", video_url: "http://www.tpstracker.com/demo/consent/video/whattoexpect1/part2_a_b_c/video", js_key: "s14"},
# 		{title: "What To Expect Video List - Possible Complications(Part 2)", video_url: "http://www.tpstracker.com/demo/consent/video/whattoexpect1/part2_a_b/video", js_key: "s15"},
# 		{title: "What To Expect Video List - Possible Complications(Part 4)", video_url: "http://www.tpstracker.com/demo/consent/video/whattoexpect1/part2_a_c/video", js_key: "s16"},
# 		{title: "What To Expect Video List - Possible Complications(Part 1)", video_url: "http://www.tpstracker.com/demo/consent/video/whattoexpect1/part2_a/video", js_key: "s17"},
# 		{title: "What To Expect Video List - Possible Complications(Part 5)", video_url: "http://www.tpstracker.com/demo/consent/video/whattoexpect1/part2_d/video", js_key: "s18"},
# 		{title: "What To Expect Video List - Possible Complications(Part 6)", video_url: "http://www.tpstracker.com/demo/consent/video/whattoexpect1/part2_e/video", js_key: "s19"},
# 		{title: "What To Expect Video List - Possible Complications(Part 7)", video_url: "http://www.tpstracker.com/demo/consent/video/whattoexpect1/part2_f/video", js_key: "s20"}
# 	])
# end

# def procedure
# 	yield
# 	Procedure.create([
# 		{procedure_name: "Preliminary Segments for Dental Implants Specialty", procedure_description: "", speciality_id: 1}
# 	])
# end

# def procedure_type
# 	yield
# 	ProcedureType.create([
# 		{ procedure_id: 1, type_name: "Implant Surgery Healing and Restoration", parent_type_id: 0, video_file_link: "http://www.tpsdemo.in/demo/consent/surgeryhealing/video", js_key: nil },
# 		{ procedure_id: 1, type_name: "Maxillary Anterior", parent_type_id: 0, video_file_link: "http://www.tpsdemo.in/demo/consent/bonegraft/video", js_key: nil },
# 		{ procedure_id: 1, type_name: "Maxillary Posterior", parent_type_id: 0, video_file_link: "http://www.tpsdemo.in/demo/consent/bonegraft/video", js_key: nil },
# 		{ procedure_id: 1, type_name: "Extraction ", parent_type_id: 0, video_file_link: "http://www.tpsdemo.in/demo/bonegraft/video", js_key: nil },
# 		{ procedure_id: 1, type_name: "Nitrous ", parent_type_id: 0, video_file_link: "http://www.tpsdemo.in/demo/consent/nitrous/video", js_key: nil },
# 		{ procedure_id: 1, type_name: "Oral Sedation", parent_type_id: 0, video_file_link: "http://www.tpsdemo.in/demo/consent/oralsedation/video", js_key: nil },
# 		{ procedure_id: 1, type_name: "Intravenous Sedation", parent_type_id: 0, video_file_link: "http://www.tpsdemo.in/demo/consent/bonegraft/video", js_key: nil },
# 		{ procedure_id: 1, type_name: "​What to expect , what is expected Patient Complications​", parent_type_id: 0, video_file_link: "http://www.tpsdemo.in/demo/consent/bonegraft/video", js_key: nil },
# 		{ procedure_id: 1, type_name: "EXTRACTION", parent_type_id: nil, video_file_link: "", js_key: nil },
# 		{ procedure_id: 1, type_name: "Maxillary Anterior", parent_type_id: 9, video_file_link: "http://www.tpsdemo.in/demo/consent/standalone/video", js_key: "c_1_1" },
# 		{ procedure_id: 1, type_name: "Maxillary Posterior", parent_type_id: 9, video_file_link: "http://www.tpsdemo.in/demo/consent/standalone/video", js_key: "c_1_2" },
# 		{ procedure_id: 1, type_name: "Mandibular Anterior", parent_type_id: 9, video_file_link: "http://www.tpsdemo.in/demo/consent/standalone/video", js_key: "c_1_3" },
# 		{ procedure_id: 1, type_name: "Mandibular Posterior", parent_type_id: 9, video_file_link: "http://www.tpsdemo.in/demo/consent/standalone/video", js_key: "c_1_4" },
# 		{ procedure_id: 1, type_name: "Socket Graft", parent_type_id: 9, video_file_link: "http://www.tpsdemo.in/demo/consent/standalone/video", js_key: "c_1_5" },
# 		{ procedure_id: 1, type_name: "IMPLANT", parent_type_id: nil, video_file_link: "", js_key: nil },
# 		{ procedure_id: 1, type_name: "Maxillary Anterior", parent_type_id: 15, video_file_link: "http://www.tpsdemo.in/demo/consent/surgeryhealing/video", js_key: "c_2_1" },
# 		{ procedure_id: 1, type_name: "Maxillary Posterior", parent_type_id: 15, video_file_link: "http://www.tpsdemo.in/demo/consent/surgeryhealing/video", js_key: "c_2_2" },
# 		{ procedure_id: 1, type_name: "Mandibular Anterior", parent_type_id: 15, video_file_link: "http://www.tpsdemo.in/demo/consent/surgeryhealing/video", js_key: "c_2_3" },
# 		{ procedure_id: 1, type_name: "Mandibular Posterior", parent_type_id: 15, video_file_link: "http://www.tpsdemo.in/demo/consent/surgeryhealing/video", js_key: "c_2_4" },
# 		{ procedure_id: 1, type_name: "BONE GRAFT", parent_type_id: nil, video_file_link: "", js_key: nil },
# 		{ procedure_id: 1, type_name: "Autogenous & Allograft", parent_type_id: 20, video_file_link: "", js_key: "c_3_1" },
# 		{ procedure_id: 1, type_name: "Maxillary Anterior", parent_type_id: 20, video_file_link: "", js_key: "c_3_3" },
# 		{ procedure_id: 1, type_name: "Maxillary Posterior", parent_type_id: 20, video_file_link: "", js_key: "c_3_4" },
# 		{ procedure_id: 1, type_name: "Mandibular Anterior", parent_type_id: 20, video_file_link: "", js_key: "c_3_5" },
# 		{ procedure_id: 1, type_name: "Mandibular Posterior", parent_type_id: 20, video_file_link: "", js_key: "c_3_6" },
# 		{ procedure_id: 1, type_name: "Sinus Augmentation", parent_type_id: 20, video_file_link: "", js_key: "c_3_7" },
# 		{ procedure_id: 1, type_name: "SEDATION", parent_type_id: nil, video_file_link: "", js_key: nil },
# 		{ procedure_id: 1, type_name: "Oral Sedation", parent_type_id: 27, video_file_link: "http://www.tpsdemo.in/demo/consent/oralsedation/video", js_key: "c_4_1" },
# 		{ procedure_id: 1, type_name: "IV Sedation", parent_type_id: 27, video_file_link: "http://www.tpsdemo.in/demo/consent/intravenous/video", js_key: "c_4_2" },
# 		{ procedure_id: 1, type_name: "Nitrous", parent_type_id: 27, video_file_link: "http://www.tpsdemo.in/demo/consent/nitrous/video", js_key: "c_4_3" }
# 	])
# end

# def doctor
# 	yield
# 	Doctor.create([
# 		{ email: "jignesh.tps@gmail.com", password: "hello123",doctor_name: "jignesh", practise_name: "dentice", address: "maninagar", phone: "1234567891", city: "ahmedabad", state: "Gujarat", zip: 382443, code: "+1", sex: "Male", speciality_id: 1, is_accept: 1 }
# 	])
# end

# # Call fuctions
# puts ">>>>>>>>>> Adding data for use <<<<<<<<<<"
# speciality { puts "Adding specialities..." }
# presentation { puts "Creating presentations..." }
# procedure { puts "Creating procedures..." }
# procedure_type { puts "Adding procedure types..." }
# puts "Now your Database is ready to use! :)"