class SubscriptionController < ApplicationController
	# include AuthorizeNet
	# layout 'authorize_net'
  helper :authorize_net

  def doctor_plan
    @doctor = current_doctor
    session[:subcribe_doc] = @doctor
    if ["Free", "Unlimited"].include?(@doctor.subscription_plan)
	    return redirect_to root_path unless session[:subcribe_doc].present?
	    @one_year_amount = 495
	    @one_year_sim_transaction = SetAuthorizeObject.new(@one_year_amount, payments_thank_you_url(:only_path => false)).set_sim_transaction
			@two_years_amount = 895
	    @two_years_sim_transaction = SetAuthorizeObject.new(@two_years_amount, payments_thank_you_url(:only_path => false)).set_sim_transaction
	  end
  end
end
