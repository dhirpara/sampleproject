require 'spec_helper'

describe Report do
  before do
    @report = Report.new(doctor_id: "1",patient_id: '1')
  end
  subject { @report }

  it { should respond_to(:doctor) }
  it { should respond_to(:patient) }
end

